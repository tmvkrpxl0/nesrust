use std::collections::VecDeque;
use std::mem::transmute;
use std::ops::ShrAssign;

use eframe::epaint::textures::TextureOptions;
use eframe::epaint::{pos2, Color32, ColorImage, Rect, TextureHandle, Vec2};
use egui::{ComboBox, Context, ScrollArea, Sense, TextEdit, TextStyle, Ui};
use egui_plot::{Line, Plot, PlotPoints};

use crate::system::debug::DevConsole;
use crate::system::devices::bus::ppu::{Object, PPU};
use crate::system::devices::bus::{Bus, PPUBus};
use crate::system::devices::cpu::debug::DevCPU;
use crate::system::devices::cpu::StatusFlags::*;
use crate::system::NES;
use crate::val_text::ValText;

static ENTIRE_PALETTE_SIZE: [usize; 2] = [16, 4];
static PALETTE_MEMORY_CONTENTS_SIZE: [usize; 2] = [4, 1];

pub struct SideMenu {
    texts: TextStorage,
    pattern_tables: [TextureHandle; 2],
    ppu_palette: TextureHandle,
    selected_palette: (u8, TextureHandle),
    pub played_audio_data: VecDeque<f32>,
    dump_target: DumpTarget,
}

impl SideMenu {
    pub fn new(egui_context: &Context) -> Self {
        let pattern_tables = [
            egui_context.load_texture(
                "Pattern Table 1",
                ColorImage::new([16 * 8, 16 * 16], Color32::BLACK),
                TextureOptions::NEAREST,
            ),
            egui_context.load_texture(
                "Pattern Table 2",
                ColorImage::new([16 * 8, 16 * 16], Color32::BLACK),
                TextureOptions::NEAREST,
            ),
        ];
        let palette_texture = egui_context.load_texture(
            "Palette Texture",
            ColorImage::new(ENTIRE_PALETTE_SIZE, Color32::BLACK),
            TextureOptions::NEAREST,
        );
        let selected_palette_texture = egui_context.load_texture(
            "Quad palette Texture",
            ColorImage::new(PALETTE_MEMORY_CONTENTS_SIZE, Color32::BLACK),
            TextureOptions::NEAREST,
        );

        Self {
            texts: TextStorage::new(),
            pattern_tables,
            ppu_palette: palette_texture,
            selected_palette: (0, selected_palette_texture),
            played_audio_data: VecDeque::with_capacity(44100),
            dump_target: DumpTarget::Cpu,
        }
    }

    pub fn update_debug_textures(&mut self, console: &NES) -> Result<(), String> {
        let ppu_rc = console.ppu();
        let ppu = ppu_rc.borrow_mut();
        let palette_rc = console.palette();
        let pattern_rc = console.pattern();

        fn dump_pattern_table(
            colors: &[(u8, u8, u8); 64],
            ppu_bus: &PPUBus,
            is_second: bool,
            palette_id: u8,
        ) -> Result<[u8; 3 * 16 * 8 * 16 * 8], String> {
            let mut to_fill = [[(0u8, 0u8, 0u8); 16 * 8]; 16 * 8];
            let table_offset: u16 = if is_second { 0x1000 } else { 0 };

            for y in 0..16u16 {
                for x in 0..16u16 {
                    let offset = 16 * (y * 16 + x);
                    for row in 0..8u16 {
                        let mut lsb = ppu_bus.read(table_offset + offset + row + 0);
                        let mut msb = ppu_bus.read(table_offset + offset + row + 8);

                        for column in 0..8u16 {
                            let value = ((lsb & 0x01) << 1) | (msb & 0x01);
                            assert!((0..=3).contains(&value));

                            lsb.shr_assign(1);
                            msb.shr_assign(1);
                            let pixel = get_color(colors, ppu_bus, palette_id, value);

                            to_fill[(y * 8 + row) as usize][(x * 8 + (7 - column)) as usize] =
                                pixel;
                        }
                    }
                }
            }

            Ok(unsafe { transmute(to_fill) })
        }

        let update_palette = palette_rc.borrow_mut().needs_update();
        let update_pattern = pattern_rc.borrow_mut().needs_update();
        let colors = PPU::colors(ppu.ppu_type)?;
        if update_pattern || update_palette {
            self.pattern_tables[0].set(
                ColorImage::from_rgb(
                    [16 * 8, 16 * 8],
                    &dump_pattern_table(
                        colors,
                        console.ppu_bus(),
                        false,
                        self.selected_palette.0,
                    )?,
                ),
                Default::default(),
            );
            self.pattern_tables[1].set(
                ColorImage::from_rgb(
                    [16 * 8, 16 * 8],
                    &dump_pattern_table(
                        colors,
                        console.ppu_bus(),
                        true,
                        self.selected_palette.0,
                    )?,
                ),
                Default::default(),
            );
        }

        if update_palette {
            let mut current_palette = [(0u8, 0u8, 0u8); 4];

            let entire_palette: &[u8; 3 * 16 * 4] = unsafe { transmute(colors) };
            for i in 0..4 {
                let color = get_color(
                    colors,
                    console.ppu_bus(),
                    self.selected_palette.0,
                    i as u8,
                );
                current_palette[i] = color;
            }
            let current_palette: [u8; 3 * 4] = unsafe { transmute(current_palette) };

            self.ppu_palette.set(
                ColorImage::from_rgb(ENTIRE_PALETTE_SIZE, entire_palette),
                TextureOptions::NEAREST,
            );
            self.selected_palette.1.set(
                ColorImage::from_rgb(PALETTE_MEMORY_CONTENTS_SIZE, &current_palette),
                TextureOptions::NEAREST,
            );
        };
        Ok(())
    }

    pub fn draw_audio(&mut self, console: &NES, ui: &mut Ui) {
        ui.collapsing("Audio Data", |ui| {
            let cpu = console.cpu();
            let apu = cpu.borrow().apu();
            let mut access = apu.borrow_mut();
            ui.horizontal(|ui| {
                ui.checkbox(&mut access.config[0], "PS1");
                ui.checkbox(&mut access.config[1], "PS2");
                ui.checkbox(&mut access.config[2], "TRI");
                ui.checkbox(&mut access.config[3], "NOI");
                ui.checkbox(&mut access.config[4], "DMC");
            });
            let data = (0..44100).into_iter().map(|index| {
                [index as f64, *self.played_audio_data.get(index).unwrap_or(&0.0) as f64]
            }).collect();
            let line = Line::new(PlotPoints::new(data));
            let max = ui.max_rect();
            let max = max.height().min(max.width());
            Plot::new("Audio Data")
                .show_x(false).width(max).height(max).show(ui, |ui| {
                ui.line(line);
            });
        });
    }

    // Returns if user has changed current palette
    pub fn draw_palettes(&mut self, ui: &mut Ui) -> bool {
        let mut should_update = false;
        ui.collapsing("현재 팔레트", |ui| {
            ComboBox::from_label("팔레트 ID")
                .selected_text(self.selected_palette.0.to_string())
                .show_ui(ui, |ui| {
                    for i in 0..8 {
                        should_update |= ui
                            .selectable_value(&mut self.selected_palette.0, i, i.to_string())
                            .changed();
                    }
                });

            {
                // draws selected_palette
                let (response, painter) =
                    ui.allocate_painter(Vec2::new(64.0, 16.0), Sense::focusable_noninteractive());

                let uv = Rect::from_min_max(pos2(0.0, 0.0), pos2(1.0, 1.0));
                painter.image(
                    self.selected_palette.1.id(),
                    response.rect,
                    uv,
                    Color32::WHITE,
                );
            }

            {
                // draws ppu_palette
                let (response, painter) =
                    ui.allocate_painter(Vec2::new(256.0, 64.0), Sense::focusable_noninteractive());

                let uv = Rect::from_min_max(pos2(0.0, 0.0), pos2(1.0, 1.0));
                painter.image(self.ppu_palette.id(), response.rect, uv, Color32::WHITE)
            }
        });

        should_update
    }

    pub fn draw_pattern_table(&mut self, ui: &mut Ui) {
        let pattern_tables = &self.pattern_tables;

        ui.collapsing("패턴 테이블 1", |ui| {
            let (response, painter) =
                ui.allocate_painter(Vec2::new(128.0, 128.0), Sense::focusable_noninteractive());

            let uv = Rect::from_min_max(pos2(0.0, 0.0), pos2(1.0, 1.0));
            painter.image(pattern_tables[0].id(), response.rect, uv, Color32::WHITE)
        });
        ui.collapsing("패턴 테이블 2", |ui| {
            let (response, painter) =
                ui.allocate_painter(Vec2::new(128.0, 128.0), Sense::focusable_noninteractive());

            let uv = Rect::from_min_max(pos2(0.0, 0.0), pos2(1.0, 1.0));
            painter.image(pattern_tables[1].id(), response.rect, uv, Color32::WHITE)
        });
    }

    pub fn draw_oam(&mut self, oam: &[Object; 64], ui: &mut Ui) {
        ui.collapsing("OAM", |ui| {
            ScrollArea::both().auto_shrink([false, false]).show(ui, |ui| {
                oam.iter().for_each(|object| { ui.label(object.to_string()); });
            });
        });
    }

    pub fn draw(&mut self, ui: &mut Ui, console: &mut NES) {
        ui.horizontal(|ui| {
            if ui.button("tick").clicked() {
                let _ = console.tick();
            }
            if ui.button("step").clicked() {
                console.step();
            }

            if ui.button("frame").clicked() {
                console.step_frame();
            }
        });

        ui.horizontal(|ui| {
            ui.text_edit_singleline(&mut self.texts.run_until);

            if ui.button("until").clicked() {
                let rc = console.cpu();
                while rc.borrow().program_counter() < *self.texts.run_until.get_val() {
                    console.step();
                }
            }
        });

        if ui.button("리셋").clicked() {
            console.press_reset()
        }

        ui.collapsing("cpu", |ui| {
            let rc = console.cpu();
            let cpu = rc.borrow();
            ui.horizontal_wrapped(|ui| {
                let green = Color32::from_rgb(0, 255, 0);
                let red = Color32::from_rgb(255, 0, 0);
                let dump = cpu.flags();
                let all_flags = [
                    Carry,
                    Zero,
                    NoInterrupt,
                    Decimal,
                    Break,
                    Unused,
                    Overflow,
                    Negative,
                ];
                for (index, flag) in all_flags.into_iter().enumerate() {
                    ui.colored_label(
                        if dump[index] { green } else { red },
                        flag.as_char().to_string(),
                    );
                }
            });
            ui.label(format!("PC: ${:04X}", cpu.program_counter()));
            ui.label(format!("A: ${:02X}", cpu.accumulator()));
            ui.label(format!("X: ${:02X}", cpu.x_register()));
            ui.label(format!("Y: ${:02X}", cpu.y_register()));
            ui.label(format!("Stack: ${:02X}", cpu.stack_pointer()));
        });

        let first_color = if self.texts.address_begin.is_valid() {
            Color32::from_rgb(255, 255, 255)
        } else {
            Color32::from_rgb(255, 0, 0)
        };
        let second_color = if self.texts.address_end.is_valid() {
            Color32::from_rgb(255, 255, 255)
        } else {
            Color32::from_rgb(255, 0, 0)
        };

        ui.horizontal_wrapped(|ui| {
            TextEdit::singleline(&mut self.texts.address_begin)
                .desired_width(64.0)
                .clip_text(true)
                .hint_text("0x0000")
                .cursor_at_end(true)
                .text_color(first_color)
                .show(ui);
            ui.label("~");
            TextEdit::singleline(&mut self.texts.address_end)
                .desired_width(64.0)
                .clip_text(true)
                .hint_text("0xFFFF")
                .cursor_at_end(true)
                .text_color(second_color)
                .show(ui);
        });

        ui.horizontal(|ui| {
            if ui.button("메모리 해독").clicked() {
                let start = self.texts.address_begin.get_val();
                let end = self.texts.address_end.get_val();
                self.texts.dump_decoded = console.dump_disassembly(*start..=*end);
            }

            if ui.button("메모리 덤프").clicked() {
                let start = self.texts.address_begin.get_val();
                let end = self.texts.address_end.get_val();
                self.texts.dump_raw = console.dump_bus(*start..=*end, self.dump_target);
            }

            ComboBox::from_label("대상")
                .selected_text(self.dump_target.to_string())
                .show_ui(ui, |ui| {
                    ui.selectable_value(&mut self.dump_target, DumpTarget::Cpu, "CPU");
                    ui.selectable_value(&mut self.dump_target, DumpTarget::Ppu, "PPU");
                });
        });

        if !self.texts.dump_decoded.is_empty() {
            ui.collapsing("메모리 해독 결과", |ui| {
                let to_print = &self.texts.dump_decoded;
                let height = ui.text_style_height(&TextStyle::Body);
                let pc = console.cpu().borrow().program_counter();

                ScrollArea::vertical()
                    .max_width(256.0)
                    .max_height(256.0)
                    .show_rows(ui, height, to_print.len(), |ui, row_range| {
                        to_print
                            .iter()
                            .enumerate()
                            .filter(|(index, _)| row_range.contains(index))
                            .for_each(|(_, (address, contents))| {
                                if matches!(self.dump_target, DumpTarget::Cpu) && *address == pc {
                                    ui.colored_label(Color32::from_rgb(100, 100, 255), contents);
                                } else {
                                    ui.label(contents);
                                }
                            });
                    });
            });
        };

        if !self.texts.dump_raw.is_empty() {
            ui.collapsing("메모리 덤프", |ui| {
                let to_print = &self.texts.dump_raw;
                let height = ui.text_style_height(&TextStyle::Body);

                ScrollArea::vertical()
                    .max_width(256.0)
                    .max_height(256.0)
                    .show_rows(ui, height, to_print.len(), |ui, row_range| {
                        to_print
                            .iter()
                            .enumerate()
                            .filter(|(index, _)| row_range.contains(index))
                            .for_each(|(_, (address, contents))| {
                                ui.label(format!("${:04X}: {:02X}", address, contents));
                            });
                    });
            });
        };

        if let Some(game) = &console.cartridge_port.borrow().cartridge {
            ui.collapsing("Name Table", |ui| {
                let nametable = &game.nametable;
                for n in 0..2 {
                    ui.push_id(n, |ui| {
                        ui.collapsing(format!("Table {}", n + 1), |ui| {
                            ScrollArea::both()
                                .max_height(1024.0)
                                .max_width(1024.0)
                                .show(ui, |ui| {
                                    for row in 0..32 {
                                        ui.horizontal(|ui| {
                                            for column in 0..32 {
                                                let idx = row * 32 + column;
                                                ui.label(hex::encode_upper(
                                                    &nametable.tables[n][idx..=idx],
                                                ));
                                            }
                                        });
                                    }
                                });
                        });
                    });
                }
            });
        }
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum DumpTarget {
    Cpu,
    Ppu,
}

impl ToString for DumpTarget {
    fn to_string(&self) -> String {
        match self {
            DumpTarget::Cpu => String::from("CPU"),
            DumpTarget::Ppu => String::from("PPU"),
        }
    }
}

struct TextStorage {
    address_begin: ValText<u16>,
    address_end: ValText<u16>,
    dump_decoded: Vec<(u16, String)>,
    dump_raw: Vec<(u16, u8)>,
    run_until: ValText<u16>,
}

impl TextStorage {
    fn new() -> Self {
        let convertor = |text: &str| {
            if text.is_empty() {
                return Some(0);
            }
            if !text.starts_with("0x") {
                return None;
            }
            let Ok(result) = hex::decode(&text[2..]) else {
                return None;
            };
            if result.len() != 2 {
                return None;
            }
            Some(((result[0] as u16) << 8) | result[1] as u16)
        };
        let address_begin = ValText::with_validator(convertor);
        let address_end = ValText::with_validator(convertor);
        let run_until = ValText::with_validator(convertor);

        TextStorage {
            address_begin,
            address_end,
            dump_decoded: vec![],
            dump_raw: vec![],
            run_until,
        }
    }
}

fn get_color(
    colors: &[(u8, u8, u8); 64],
    ppu_bus: &PPUBus,
    palette_id: u8,
    pixel_id: u8,
) -> (u8, u8, u8) {
    let index = ppu_bus.read(0x3F00u16 + ((palette_id << 2) + pixel_id) as u16) & 0x3F;
    colors[index as usize]
}
