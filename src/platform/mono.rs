use std::time::Duration;
use crossbeam::channel::Receiver;

pub struct MonoSource {
    pub receiver: Receiver<f32>,
    last_received: f32,
}

impl MonoSource {
    pub fn new(receiver: Receiver<f32>) -> MonoSource {
        Self {
            receiver,
            last_received: 0.0,
        }
    }
}

impl Iterator for MonoSource {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.receiver.is_empty() {
            Some(self.last_received)
        } else {
            let value = self.receiver.recv().ok();
            if let Some(value) = value {
                self.last_received = value;
            }

            Some(self.last_received)
        }
    }
}

impl rodio::Source for MonoSource {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn sample_rate(&self) -> u32 {
        44100
    }

    fn total_duration(&self) -> Option<Duration> {
        None
    }
}