use std::iter::once;
use std::ops::DerefMut;
use std::sync::Arc;

use bytemuck::cast_slice;
use cfg_if::cfg_if;
use crossbeam::channel::{bounded, Receiver, Sender};
use eframe::egui::{Context, FontData};
use eframe::emath::{Align2, pos2, Rect, Vec2};
use eframe::epaint::{ColorImage, FontFamily, ImageData, TextureHandle};
use eframe::epaint::textures::TextureOptions;
use egui::{Color32, ComboBox, menu, Sense, WidgetText};
use egui_wgpu::Renderer;
use egui_wgpu::ScreenDescriptor;
use egui_winit::egui::FontDefinitions;
use image::ImageFormat;
use rodio::{OutputStream, OutputStreamHandle, Sink};
use strum::IntoEnumIterator;
use wgpu::*;
use winit::{event::*, event_loop::EventLoop, keyboard, window::WindowBuilder};
use winit::dpi::PhysicalSize;
use winit::event_loop::EventLoopWindowTarget;
use winit::keyboard::KeyCode;
use winit::window::{Icon, Window};

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

use crate::platform::mono::MonoSource;
use crate::platform::side_menu::SideMenu;
use crate::system::{ConsoleHealth, LivingRoom, NES, Sharable, Timing, Tv, TvContents};
use crate::system::debug::DevConsole;
use crate::system::devices::bus::expansions::{ExpansionDevice, ExpansionDeviceKinds};
use crate::system::devices::bus::expansions::standard_controller::StandardNesControllers;
use crate::system::devices::bus::expansions::zapper::Zapper;
use crate::system::devices::game_cartridge::{Cartridge, ConsoleType};
use crate::utils::pcg_hash;

pub mod side_menu;
mod mono;

static SAMPLE_LENGTH: f32 = 1.0 / 44100.0;

struct WgpuState {
    surface: Surface<'static>,
    device: Device,
    screen: ScreenDescriptor,
    queue: Queue,
    config: SurfaceConfiguration,
    window: Arc<Window>,
    egui_state: egui_winit::State,
    egui_context: Context,
    egui_renderer: Renderer,
    frame_count: usize,
}

enum PlaybackSpeed {
    Double,
    Normal,
    Half,
}

impl PlaybackSpeed {
    pub fn double(&self) -> Self {
        match self {
            PlaybackSpeed::Double => PlaybackSpeed::Double,
            PlaybackSpeed::Normal => PlaybackSpeed::Double,
            PlaybackSpeed::Half => PlaybackSpeed::Normal
        }
    }

    pub fn half(&self) -> Self {
        match self {
            PlaybackSpeed::Double => PlaybackSpeed::Normal,
            PlaybackSpeed::Normal => PlaybackSpeed::Half,
            PlaybackSpeed::Half => PlaybackSpeed::Half
        }
    }

    pub fn numeric(&self) -> f32 {
        match self {
            PlaybackSpeed::Double => 2.0,
            PlaybackSpeed::Normal => 1.0,
            PlaybackSpeed::Half => 0.5,
        }
    }
}

struct NesRust {
    room: LivingRoom,
    screen: TextureHandle,
    audio_time: f32,
    channel: (Sender<f32>, Receiver<f32>),
    audio: (OutputStream, OutputStreamHandle, Sink),
    gpu: WgpuState,
    side_menu: SideMenu,
    speed: PlaybackSpeed,
}

impl NesRust {
    // Creating some of the wgpu types requires async code
    async fn new(window: Window) -> NesRust {
        let window: Arc<_> = window.into();
        let size = window.inner_size();

        let instance = Instance::new(InstanceDescriptor {
            backends: Backends::all(),
            flags: InstanceFlags::VALIDATION,
            ..Default::default()
        });

        let surface = instance.create_surface(window.clone()).unwrap();
        let adapter = instance
            .request_adapter(&RequestAdapterOptions {
                power_preference: Default::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();

        let device_descriptor = DeviceDescriptor {
            label: None,
            required_features: Default::default(),
            required_limits: if cfg!(target_arch = "wasm32") {
                Limits::downlevel_webgl2_defaults()
            } else {
                Limits::default()
            },
        };
        let (device, queue) = adapter
            .request_device(&device_descriptor, None)
            .await
            .unwrap();

        let surface_caps = surface.get_capabilities(&adapter);
        // Shader code in this tutorial assumes an sRGB surface texture. Using a different
        // one will result all the colors coming out darker. If you want to support non
        // sRGB surfaces, you'll need to account for that when drawing to the frame.
        let surface_format = surface_caps
            .formats
            .iter()
            .copied()
            .filter(TextureFormat::is_srgb)
            .next()
            .unwrap_or(surface_caps.formats[0]);
        let config = SurfaceConfiguration {
            usage: TextureUsages::RENDER_ATTACHMENT,
            format: surface_format,
            width: size.width,
            height: size.height,
            present_mode: PresentMode::Fifo,
            desired_maximum_frame_latency: 2,
            alpha_mode: surface_caps.alpha_modes[0],
            view_formats: vec![],
        };
        surface.configure(&device, &config);

        let egui_context = Context::default();
        let egui_state = egui_winit::State::new(
            egui_context.clone(),
            egui_context.viewport_id(),
            &window,
            egui_context.native_pixels_per_point().into(),
            None
        );

        let fonts = {
            let mut f = FontDefinitions {
                font_data: Default::default(),
                families: Default::default(),
            };

            f.font_data.insert(
                "Nanum Gothic".to_owned(),
                FontData::from_static(include_bytes!("NanumGothic.ttf")),
            );
            f.font_data.insert(
                "Nanum Gothic Coding".to_owned(),
                FontData::from_static(include_bytes!("NanumGothicCoding.ttf")),
            );

            f.families
                .insert(FontFamily::Proportional, vec!["Nanum Gothic".to_owned()]);
            f.families.insert(
                FontFamily::Monospace,
                vec!["Nanum Gothic Coding".to_owned()],
            );

            f
        };
        egui_context.set_fonts(fonts);

        let screen = ScreenDescriptor {
            size_in_pixels: [config.width, config.height],
            pixels_per_point: egui_context.pixels_per_point(),
        };
        let egui_renderer = Renderer::new(&device, surface_format, None, 1);
        let side_menu = SideMenu::new(&egui_context);

        let gpu = WgpuState {
            surface,
            device,
            screen,
            queue,
            config,
            window,
            egui_state,
            egui_context,
            egui_renderer,
            frame_count: 0,
        };

        let (sender, receiver) = bounded(44100);
        let mono_source = MonoSource::new(receiver.clone());
        let (stream, handle) = OutputStream::try_default().unwrap();
        let sink = Sink::try_new(&handle).unwrap();
        sink.append(mono_source);

        let tv = Sharable::new(Tv::new(Timing::NTSC));
        let peripheral = Sharable::new((
            Box::new(StandardNesControllers::new()) as Box<dyn ExpansionDevice>, Box::new(StandardNesControllers::new()) as Box<dyn ExpansionDevice>
        ));
        let console = NES::new(ConsoleType::NesFcDendy {
            timing: Timing::NTSC,
        }, tv.wire(), peripheral.wire());

        let room = LivingRoom {
            tv,
            save: None,
            console,
            peripheral,
        };

        let screen = gpu.egui_context.load_texture(
            "screen",
            ColorImage::new([256, 240], Color32::BLACK),
            TextureOptions::NEAREST,
        );

        Self {
            room,
            screen,
            audio_time: 0.0,
            channel: (sender, receiver),
            audio: (stream, handle, sink),
            gpu,
            side_menu,
            speed: PlaybackSpeed::Normal,
        }
    }

    pub fn window(&self) -> &Window {
        &self.gpu.window
    }

    fn resize(&mut self, new_size: PhysicalSize<u32>) {
        if new_size.width == 0 || new_size.height == 0 {
            return;
        }

        self.gpu.config.width = new_size.width;
        self.gpu.config.height = new_size.height;
        self.gpu
            .surface
            .configure(&self.gpu.device, &self.gpu.config);

        self.gpu.screen.pixels_per_point = self.gpu.egui_context.pixels_per_point();
        self.gpu.screen.size_in_pixels = [new_size.width, new_size.height];
    }

    fn process_screen(&mut self, tv: &mut Tv) {
        match &mut tv.screen {
            TvContents::Error(_) => {
                self.screen.set(
                    ColorImage::new([256, 240], Color32::BLACK),
                    TextureOptions::NEAREST,
                );
            }
            TvContents::Contents(contents) => {
                if self.room.console.cartridge_port.borrow().cartridge.is_none() {
                    let contents = Arc::get_mut(contents).unwrap();
                    for i in 0..(256 * 240) {
                        let brightness = ((pcg_hash(self.gpu.frame_count.wrapping_mul(i) as u32)) % 256) as u8;
                        contents[i][0] = brightness;
                        contents[i][1] = brightness;
                        contents[i][2] = brightness;
                    }
                    self.screen.set(
                        ImageData::Color(ColorImage::from_rgb([256, 240], cast_slice(contents.as_slice())).into()),
                        TextureOptions::NEAREST,
                    )
                } else {
                    self.screen.set(
                        ImageData::Color(ColorImage::from_rgb(
                            [256, 240],
                            cast_slice(contents.as_slice()),
                        ).into()),
                        TextureOptions::NEAREST,
                    )
                }
            },
        }
    }

    fn render(&mut self) -> Result<(), SurfaceError> {
        if !self.room.console.has_paused() {
            let timing = self.room.console.console_type.ppu_type().timing();
            let tick_count = (((timing.ppu_clock_speed()) / timing.framerate() as f32) * self.speed.numeric()) as u32;
            self.audio.2.set_speed(self.speed.numeric());
            for _ in 0..tick_count {
                if !self.room.console.tick() {
                    break;
                }
                self.audio_time += 1.0 / timing.ppu_clock_speed();
                if self.audio_time >= SAMPLE_LENGTH {
                    self.audio_time -= SAMPLE_LENGTH;
                    if self.channel.0.is_full() {
                        self.channel.1.recv().unwrap();
                    }
                    let audio = self.room.tv.borrow().audio;
                    if self.side_menu.played_audio_data.len() == self.side_menu.played_audio_data.capacity() {
                        self.side_menu.played_audio_data.pop_front();
                    }
                    self.side_menu.played_audio_data.push_back(audio);
                    self.channel.0.send(audio).unwrap();
                }
            }
        }

        let frame = self.gpu.surface.get_current_texture()?;
        let view = frame.texture.create_view(&Default::default());

        let mut encoder = self
            .gpu
            .device
            .create_command_encoder(&CommandEncoderDescriptor {
                label: Some("nes encoder"),
            });

        let mut input = self.gpu.egui_state.take_egui_input(&self.gpu.window);
        if let Some(path) = input.dropped_files.pop() {
            match Cartridge::load_from_file(path.path.unwrap()) {
                Ok(rom) => {
                    self.room.setup(&rom);
                    self.room.console.insert_cartridge(rom);
                    self.room.console.press_reset();
                },
                Err(e) => {
                    self.room.console.panic(&mut self.room.tv.borrow_mut(), e.to_string());
                }
            }
        }
        let output = {
            let context = self.gpu.egui_context.clone();
            context.run(input, |ctx| {
                self.draw_gui(ctx);
            })
        };
        self.gpu.egui_state.handle_platform_output(
            &self.gpu.window,
            output.platform_output,
        );

        let primitives = self.gpu.egui_context.tessellate(output.shapes, self.gpu.egui_context.pixels_per_point());
        for (texture_id, image_delta) in output.textures_delta.set {
            self.gpu.egui_renderer.update_texture(
                &self.gpu.device,
                &self.gpu.queue,
                texture_id,
                &image_delta,
            );
        }

        self.gpu.egui_renderer.update_buffers(
            &self.gpu.device,
            &self.gpu.queue,
            &mut encoder,
            primitives.as_slice(),
            &self.gpu.screen,
        );

        {
            let mut render_pass = encoder.begin_render_pass(&RenderPassDescriptor {
                label: Some("nes main render pass"),
                color_attachments: &[Some(RenderPassColorAttachment {
                    view: &view,
                    // The resolve_target is the texture that will receive the resolved output.
                    // This will be the same as view unless multisampling is enabled.
                    // We don't need to specify this, so we leave it as None.
                    resolve_target: None,
                    // Operation is a pair of:
                    // Load(for specifying what to do with previous colors
                    // Store(for specifying whether or not to save result of Load operation into the view)
                    ops: Operations {
                        load: LoadOp::Clear(Color {
                            r: 0.0,
                            g: 0.0,
                            b: 0.0,
                            a: 1.0,
                        }),
                        store: StoreOp::Store,
                    },
                })],
                depth_stencil_attachment: None,
                timestamp_writes: None,
                occlusion_query_set: None,
            });

            self.gpu
                .egui_renderer
                .render(&mut render_pass, primitives.as_slice(), &self.gpu.screen)
        }
        self.gpu.queue.submit(once(encoder.finish()));

        frame.present();
        self.gpu.frame_count += 1;

        Ok(())
    }

    fn draw_gui(&mut self, ctx: &Context) {
        let panel = egui::CentralPanel::default();
        let menu = egui::SidePanel::right("설정");
        panel.show(ctx, |ui| {
            menu::bar(ui, |ui| {
                ui.menu_button("File", |ui| {
                    if ui.button("Open").clicked() {
                        /*if let Some(path) = FileDialog::new().pick_file() {
                            match Cartridge::load_from_file(path) {
                                Ok(rom) => {
                                    self.room.setup(&rom);
                                    self.room.console.insert_cartridge(rom);
                                    self.room.console.press_reset();
                                }
                                Err(error) => {
                                    self.room.tv.borrow_mut().screen =
                                        TvContents::Error(error.to_string())
                                }
                            }
                        }*/
                    }
                });
            });

            let menu_width = menu
                .show_animated_inside(ui, true, |ui| {
                    let console = &mut self.room.console;

                    self.side_menu.draw(ui, console);
                    if let Err(message) = self.side_menu.update_debug_textures(console) {
                        console.panic(&mut self.room.tv.borrow_mut(), message)
                    };
                    self.side_menu.draw_audio(console, ui);
                    let palette = console.palette();
                    let has_palette_changed = self.side_menu.draw_palettes(ui);
                    palette.borrow_mut().or(has_palette_changed);
                    self.side_menu.draw_pattern_table(ui);

                    let ppu_wire = console.ppu();
                    let mut ppu = ppu_wire.borrow_mut();
                    self.side_menu.draw_oam(ppu.oam(), ui);
                })
                .unwrap()
                .response
                .rect
                .width();

            let tv_rc = self.room.tv.wire();
            let mut tv = tv_rc.borrow_mut();
            #[allow(unused_variables)]
            {
                // Draw screen
                self.process_screen(tv.deref_mut());

                let (x, y) = {
                    let max_x = ui.max_rect().width() - menu_width;
                    let max_y = ui.max_rect().height() - 100.0;
                    let ratio = 15.0 / 16.0;
                    if max_x > max_y {
                        (max_y / ratio, max_y)
                    } else {
                        (max_x, max_x * ratio)
                    }
                };
                let size = Vec2::new(x, y);
                let (response, painter) =
                    ui.allocate_painter(size, Sense::click());

                let uv = Rect::from_min_max(pos2(0.0, 0.0), pos2(1.0, 1.0));
                painter.image(self.screen.id(), response.rect, uv, Color32::WHITE);

                let mut devices = self.room.peripheral.borrow_mut();
                let p1 = &mut devices.0;
                let p2 = &mut devices.1;
                if matches!(p2.kind(), ExpansionDeviceKinds::Zapper4017) {
                    let zapper: &mut Zapper = p2.as_any_mut().downcast_mut().unwrap();
                    if let Some(mut hover) = response.hover_pos() {
                        hover.x -= response.rect.left_top().x;
                        hover.y -= response.rect.left_top().y;

                        hover.x /= response.rect.size().x;
                        hover.y /= response.rect.size().y;

                        hover.x = hover.x.clamp(0.0, 1.0);
                        hover.y = hover.y.clamp(0.0, 1.0);

                        zapper.aiming = ((256.0 * hover.x).floor() as u8, (240.0 * hover.y).floor() as u8);
                    }
                    zapper.is_pulling = response.clicked();
                }

                if let TvContents::Error(message) = &tv.screen {
                    painter.debug_text(
                        response.rect.left_top(),
                        Align2::LEFT_TOP,
                        Color32::RED,
                        message,
                    );
                };
            };

            drop(tv);
            ui.horizontal(|ui| {
                let console = &mut self.room.console;
                let led_color = match &console.health_led {
                    ConsoleHealth::Green => Color32::LIGHT_GREEN,
                    ConsoleHealth::Yellow => Color32::YELLOW,
                    ConsoleHealth::Red => Color32::RED
                };
                if ui.button(WidgetText::from("재시작").color(Color32::RED)).clicked() {
                    *console = NES::new(console.console_type, self.room.tv.wire(), self.room.peripheral.wire());
                    let mut tv = self.room.tv.borrow_mut();

                    match &mut tv.screen {
                        TvContents::Error(_) => {
                            tv.screen = TvContents::Contents(Tv::new_screen().into())
                        }
                        TvContents::Contents(contents) => {
                            Arc::get_mut(contents).unwrap().fill([0u8; 3])
                        }
                    }
                }
                if ui.button(WidgetText::from("Reset").color(led_color)).clicked() {
                    console.press_reset();
                }
                let timings = [Timing::NTSC, Timing::PAL, Timing::Dendy];
                let mut tv = tv_rc.borrow_mut();
                timings.iter().for_each(|timing| {
                    let timing_color = if tv.timing.eq(timing) {
                        Color32::WHITE
                    } else {
                        Color32::GRAY
                    };
                    if ui.button(WidgetText::from(timing.to_string()).color(timing_color)).clicked() {
                        tv.timing = *timing;
                    }
                });

                {
                    let text = if console.has_paused() { "일시정지" } else { "재생" };
                    if ui.button(text).clicked() {
                        if console.has_paused() {
                            console.resume()
                        } else {
                            console.pause()
                        }
                    }
                }
            });
                let mut devices = self.room.peripheral.borrow_mut();
                ui.push_id("컨트롤러 1", |ui| {
                    let mut current_kind = devices.0.kind();
                    let copy = current_kind;
                    ComboBox::from_label("컨트롤러 선택")
                        .selected_text(current_kind.to_string())
                        .show_ui(ui, |ui| {
                            ExpansionDeviceKinds::iter().filter(|kind| kind.is_supported()).for_each(|kind| {
                                ui.selectable_value(
                                    &mut current_kind, kind, kind.to_string()
                                );
                            });
                        });
                    if copy != current_kind {
                        devices.0 = current_kind.create(true);
                    }
                });

                ui.push_id("컨트롤러 2", |ui| {
                    let mut current_kind = devices.1.kind();
                    let copy = current_kind;
                    ComboBox::from_label("컨트롤러 선택")
                        .selected_text(current_kind.to_string())
                        .show_ui(ui, |ui| {
                            ExpansionDeviceKinds::iter().filter(|kind| kind.is_supported()).for_each(|kind| {
                                ui.selectable_value(
                                    &mut current_kind, kind, kind.to_string()
                                );
                            });
                        });
                    if copy != current_kind {
                        devices.1 = current_kind.create(false);
                    }
                });
        });
    }
}

pub async fn run(icon_path: Option<String>) {
    cfg_if! {
        if #[cfg(target_arch = "wasm32")] {
            std::panic::set_hook(Box::new(console_error_panic_hook::hook));
            console_log::init_with_level(log::Level::Warn).expect("로거 초기화 실패!");
        } else {
            env_logger::init();
        }
    }
    let icon = {
        icon_path
            .and_then(|path| {
                image::open(path).ok().or_else(|| {
                    image::load_from_memory_with_format(
                        include_bytes!("./tmvkrpxl0.png"),
                        ImageFormat::Png,
                    )
                    .ok()
                })
            })
            .and_then(|image| {
                let rgba = image.into_rgba8();
                let width = rgba.width();
                let height = rgba.height();
                Icon::from_rgba(rgba.into_raw(), width, height).ok()
            })
    };

    let event_loop = EventLoop::new().unwrap();
    let window = WindowBuilder::new()
        .with_window_icon(icon)
        .build(&event_loop)
        .unwrap();

    #[cfg(target_arch = "wasm32")]
    {
        // Winit prevents sizing with CSS, so we have to set
        // the size manually when on web.
        use winit::dpi::PhysicalSize;
        window.set_inner_size(PhysicalSize::new(450, 400));

        use winit::platform::web::WindowExtWebSys;
        web_sys::window()
            .and_then(|win| win.document())
            .and_then(|doc| {
                let dst = doc.get_element_by_id("wasm-example")?;
                let canvas = web_sys::Element::from(window.canvas());
                dst.append_child(&canvas).ok()?;
                Some(())
            })
            .expect("Couldn't append canvas to document body.");
    }

    let mut state = NesRust::new(window).await;

    event_loop.run(move |event, target| on_event(&mut state, event, target)).unwrap();
}

fn on_event(state: &mut NesRust, event: Event<()>, target: &EventLoopWindowTarget<()>) {
    match event {
        Event::WindowEvent {
            ref event,
            window_id,
        } if window_id == state.window().id() => {
            let response = state
                .gpu
                .egui_state
                .on_window_event(&state.gpu.window, event);
            if !response.consumed {
                match event {
                    WindowEvent::RedrawRequested => {
                        let Err(error) = state.render() else {
                            return;
                        };
                        match error {
                            SurfaceError::Lost => {
                                let width = state.gpu.config.width;
                                let height = state.gpu.config.height;
                                let size = PhysicalSize::from((width, height));
                                state.resize(size)
                            }
                            SurfaceError::OutOfMemory => target.exit(),
                            _ => eprintln!("{:?}", error),
                        }
                    }
                    WindowEvent::Resized(new_size) => {
                        state.resize(*new_size);
                    }
                    WindowEvent::ScaleFactorChanged { .. } => {
                        state.resize(PhysicalSize::new(state.gpu.config.width, state.gpu.config.height));
                    }
                    WindowEvent::CloseRequested => target.exit(),
                    WindowEvent::KeyboardInput {
                        event: KeyEvent {
                            state: pressed,
                            physical_key: keyboard::PhysicalKey::Code(key),
                            ..
                        },
                        ..
                    } => {
                        let is_press = matches!(pressed, ElementState::Pressed);
                        match key {
                            KeyCode::KeyF if is_press => {
                                state.room.console.step_frame()
                            }
                            KeyCode::Space if is_press => {
                                if state.room.console.has_paused() {
                                    state.room.console.resume()
                                } else {
                                    state.room.console.pause()
                                }
                            }
                            KeyCode::KeyL => {
                                if is_press {
                                    state.speed = state.speed.double()
                                } else {
                                    state.speed = state.speed.half()
                                }
                            }
                            KeyCode::KeyK => {
                                if is_press {
                                    state.speed = state.speed.half();
                                } else {
                                    state.speed = state.speed.double();
                                }
                            }
                            KeyCode::KeyR if is_press => {
                                state.room.console.press_reset();
                            }
                            KeyCode::F5 if is_press => {
                                let peripheral = state.room.peripheral.borrow();
                                let cloned = Sharable::new((peripheral.0.clone_box(),
                                    peripheral.1.clone_box()
                                ));
                                state.room.save.replace((state.room.console.take_snapshot(cloned.wire()), cloned));
                            }
                            KeyCode::F7 if is_press => {
                                if let Some(save) = state.room.save.as_ref() {
                                    let devices = &save.1.borrow();
                                    state.room.peripheral = Sharable::new((devices.0.clone_box(), devices.1.clone_box()));
                                    state.room.console = save.0.take_snapshot(state.room.peripheral.wire());
                                }
                            }
                            _ => {
                                let mut devices = state.room.peripheral.borrow_mut();
                                match devices.0.kind() {
                                    ExpansionDeviceKinds::StandardNesControllers => {
                                        let nes_controllers: &mut StandardNesControllers = devices.0.as_any_mut().downcast_mut().unwrap();
                                        let keyboard_input = &mut nes_controllers.keyboard_input[0];
                                        match key {
                                            KeyCode::ArrowRight => keyboard_input.set_right(is_press),
                                            KeyCode::ArrowLeft => keyboard_input.set_left(is_press),
                                            KeyCode::ArrowDown => keyboard_input.set_down(is_press),
                                            KeyCode::ArrowUp => keyboard_input.set_up(is_press),
                                            KeyCode::KeyS => keyboard_input.set_s(is_press),
                                            KeyCode::KeyA => keyboard_input.set_a(is_press),
                                            KeyCode::KeyZ => keyboard_input.set_z(is_press),
                                            KeyCode::KeyX => keyboard_input.set_x(is_press),
                                            _ => {}
                                        }
                                    }
                                    _ => {}
                                }
                            },
                        }
                    }
                    _ => {}
                }
            }
            if response.repaint {
                state.gpu.window.request_redraw();
            }
        }

        Event::AboutToWait => {
            state.window().request_redraw();
        }
        _ => {}
    }
}
