extern crate core;

use pollster::block_on;

use crate::platform::run;

mod platform;
mod system;
mod utils;
mod val_text;

fn main() {
    block_on(run(None));
}
