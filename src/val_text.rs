use std::str::FromStr;

use eframe::egui::TextBuffer;

/// A mutable TextBuffer that will validate it's contents when changed.
///
/// The default validator will simply attempt to parse the text as `T`,
/// but a custom validator function can be provided.
pub struct ValText<T: Default> {
    text: String,
    val: T,
    #[allow(clippy::type_complexity)]
    validator: Box<dyn Fn(&str) -> Option<T>>,
    is_valid: bool,
}

impl<T: Default + FromStr> ValText<T> {
    #[allow(dead_code)]
    pub fn new() -> ValText<T> {
        Self {
            text: Default::default(),
            val: Default::default(),
            validator: Box::new(|t| FromStr::from_str(t).ok()),
            is_valid: true,
        }
    }
}

impl<T: Default> ValText<T> {
    pub fn with_validator(validator: impl Fn(&str) -> Option<T> + 'static) -> Self {
        Self {
            text: Default::default(),
            val: Default::default(),
            validator: Box::new(validator),
            is_valid: true,
        }
    }

    pub fn get_val(&self) -> &T {
        &self.val
    }

    pub fn is_valid(&self) -> bool {
        self.is_valid.clone()
    }
}

impl<T: Default> TextBuffer for ValText<T> {
    fn is_mutable(&self) -> bool {
        true
    }

    fn as_str(&self) -> &str {
        self.text.as_str()
    }

    fn insert_text(&mut self, text: &str, char_index: usize) -> usize {
        let inserted = self.text.insert_text(text, char_index);
        let result: Option<T> = (self.validator)(&self.text);
        match result {
            None => {
                self.is_valid = false;
            }
            Some(new) => {
                self.is_valid = true;
                self.val = new;
            }
        }
        inserted
    }

    fn delete_char_range(&mut self, char_range: std::ops::Range<usize>) {
        self.text.delete_char_range(char_range);
        let result: Option<T> = (self.validator)(&self.text);
        match result {
            None => {
                self.is_valid = false;
            }
            Some(new) => {
                self.val = new;
                self.is_valid = true;
            }
        }
    }
}
