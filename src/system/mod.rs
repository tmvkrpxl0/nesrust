use std::cell::{RefCell, RefMut};
use std::ops::{Deref, DerefMut, RangeInclusive};
use std::rc::Rc;
use std::sync::Arc;

use strum_macros::Display;

use crate::platform::side_menu::DumpTarget;
use crate::system::devices::bus::{Bus, PPUBus, SystemBus};
use crate::system::devices::bus::cartridge_connector::CartridgeConnector;
use crate::system::devices::bus::expansions::{ExpansionDevice, ExpansionDeviceKinds};
use crate::system::devices::bus::expansions::zapper::Zapper;
use crate::system::devices::bus::memory::Memory;
use crate::system::devices::bus::ppu::{PPU, PPUType};
use crate::system::devices::bus::ppu::debug::DevPPU;
use crate::system::devices::bus::ppu::palette_memory::PaletteMemory;
use crate::system::devices::bus::ppu::pattern_memory::PatternMemory;
use crate::system::devices::cpu::CPU6502;
use crate::system::devices::cpu::debug::DevCPU;
use crate::system::devices::game_cartridge::{Cartridge, ConsoleType};

pub mod debug;
pub mod devices;

pub struct LivingRoom {
    pub tv: Sharable<Tv>,
    pub save: Option<(NES, Sharable<(Box<dyn ExpansionDevice>, Box<dyn ExpansionDevice>)>)>,
    pub console: NES,
    pub peripheral: Sharable<(Box<dyn ExpansionDevice>, Box<dyn ExpansionDevice>)>,
}

pub struct Sharable<T> {
    internal: Rc<RefCell<T>>
}

pub type Wire<T> = Rc<RefCell<T>>;

impl<T> Sharable<T> {
    pub fn new(value: T) -> Self {
        Self {
            internal: Rc::new(RefCell::new(value)),
        }
    }

    pub fn wire(&self) -> Wire<T> {
        self.internal.clone()
    }
}

impl<T> Deref for Sharable<T> {
    type Target = RefCell<T>;

    fn deref(&self) -> &Self::Target {
        self.internal.deref()
    }
}

impl LivingRoom {
    pub fn setup(&mut self, rom: &Cartridge) {
        let nes = NES::new(
            rom.made_for,
            self.tv.wire(),
            self.peripheral.wire(),
        );
        let timing = nes.console_type.ppu_type().timing();
        self.console = nes;
        self.tv.borrow_mut().timing = timing;
        let mut peripheral = self.peripheral.borrow_mut();
        peripheral.0 = rom.needs.create(true);
        peripheral.1 = rom.needs.create(false);
    }
}

pub struct Tv {
    pub screen: TvContents,
    pub audio: f32,
    pub timing: Timing,
}

pub enum TvContents {
    Error(String),
    Contents(Arc<[[u8; 3]; 256 * 240]>),
}

impl Tv {
    pub fn new(timings: Timing) -> Tv {
        Tv {
            screen: TvContents::Contents(Self::new_screen().into()),
            audio: 0.0,
            timing: timings,
        }
    }

    pub fn new_screen() -> Box<[[u8; 3]; 256 * 240]> {
        let vec = vec![[0u8; 3]; 256 * 240].into_boxed_slice();
        let ptr = Box::into_raw(vec) as *mut [[u8; 3]; 256 * 240];
        unsafe { Box::from_raw(ptr) }
    }

    pub fn clear(&mut self) {
        match &mut self.screen {
            TvContents::Error(_) => {
                self.screen = TvContents::Contents(Tv::new_screen().into());
            }
            TvContents::Contents(contents) => {
                let data = Arc::get_mut(contents).unwrap();
                data.fill([0u8; 3]);
            }
        }
    }
}

#[derive(Copy, Clone)]
pub enum ConsoleHealth {
    Green,
    Yellow,
    Red,
}

type RcMain<T> = Rc<T>;

pub struct NES {
    pub console_type: ConsoleType,
    pub cartridge_port: Sharable<CartridgeConnector>,
    pub tv: Wire<Tv>,
    pub health_led: ConsoleHealth,

    cpu: Sharable<CPU6502>,
    main_bus: RcMain<SystemBus>,
    system_ram: Sharable<Memory<0x0000, 0x1FFF>>,

    peripheral: Wire<(Box<dyn ExpansionDevice>, Box<dyn ExpansionDevice>)>,

    ppu: Sharable<PPU>,
    ppu_bus: RcMain<PPUBus>,
    pattern: Sharable<PatternMemory>,
    palette: Sharable<PaletteMemory>,

    has_paused: bool,
    uptime_clocks: u32,
}

#[derive(Default, Copy, Clone, PartialEq, Eq, Display)]
pub enum Timing {
    #[default]
    NTSC,
    PAL,
    MultiRegion,
    Dendy,
}

#[allow(dead_code)]
impl Timing {
    const NTSC_NOISES: [u16; 16] = [4, 8, 16, 32, 64, 96, 128, 160, 202, 254, 380, 508, 762, 1016, 2034, 4068];
    const PAL_NOISES: [u16; 16] = [4, 8, 14, 30, 60, 88, 118, 148, 188, 236, 354, 472, 708,  944, 1890, 3778];

    pub fn supports(&self, other: &Self) -> bool {
        if self == other {
            return true;
        }
        if matches!(self, Timing::MultiRegion) {
            return true;
        }
        false
    }

    pub fn speed(&self) -> u64 {
        match self {
            Timing::NTSC => 21477272,
            Timing::PAL => 26601712,
            Timing::MultiRegion => 21477272, // Randomly picked NTSC
            Timing::Dendy => 26601712
        }
    }

    pub fn ppu_clock_speed(&self) -> f32 {
        match self {
            Timing::NTSC => 21477272.0 / 4.0,
            Timing::PAL => 26601712.0 / 5.0,
            Timing::MultiRegion => 21477272.0 / 4.0, // Randomly picked NTSC
            Timing::Dendy => 26601712.0 / 5.0
        }
    }

    pub fn cpu_clock_speed(&self) -> u32 {
        match self {
            Timing::NTSC => 1789773,
            Timing::PAL => 1662607,
            Timing::MultiRegion => 1789773, // Randomly picked NTSC
            Timing::Dendy => 1773448,
        }
    }

    pub fn framerate(&self) -> u32 {
        match self {
            Timing::NTSC => 60,
            Timing::PAL => 50,
            Timing::MultiRegion => 60,
            Timing::Dendy => 50
        }
    }

    pub fn scanlines(&self) -> u16 {
        match self {
            Timing::NTSC
            | Timing::MultiRegion => 262,
            Timing::PAL
            | Timing::Dendy => 312,
        }
    }

    pub fn noise_period(&self) -> &[u16; 16] {
        match self {
            Timing::NTSC
            | Timing::MultiRegion => &Self::NTSC_NOISES,
            Timing::PAL
            | Timing::Dendy => &Self::PAL_NOISES
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum VsType {
    UniSystemNormal,
    UniSystemRbi,
    UniSystemTko,
    UniSystemSuperX,
    UniSystemIceClimber,
    DualSystemNormal,
    DualSystemRaid,
}

impl NES {
    pub fn new(console_type: ConsoleType, tv: Wire<Tv>, peripheral: Wire<(Box<dyn ExpansionDevice>, Box<dyn ExpansionDevice>)>) -> Self {
        let ppu_type = match &console_type {
            ConsoleType::VS {
                vs_type: _vs_type,
                ppu_type,
            } => *ppu_type,
            _ => PPUType::RP2C02,
        };

        let palette = Sharable::new(PaletteMemory::new());
        let cartridge_port = Sharable::new(CartridgeConnector { cartridge: None });
        let pattern = Sharable::new(match console_type {
            ConsoleType::VS { .. }
            | ConsoleType::PlayChoice
            | ConsoleType::FamicomNetworkSystem => PatternMemory::builtin(),
            _ => PatternMemory::cartridge(cartridge_port.wire()),
        });
        let system_ram = Sharable::new(Memory::new("RAM", 4));
        let cpu = Sharable::new(CPU6502::new(ppu_type));
        let ppu_bus = Rc::new(PPUBus::new(
            pattern.wire(),
            palette.wire(),
            cartridge_port.wire()
        ));
        let ppu = Sharable::new(PPU::new(ppu_type, ppu_bus.clone(), cpu.wire()));
        cartridge_port.borrow().connect_side_channel(ppu.borrow_mut().deref_mut());
        let main_bus = RcMain::new(SystemBus::new(
            system_ram.wire(),
            cartridge_port.wire(),
            ppu.wire(),
            cpu.borrow().apu(),
            peripheral.clone(),
        ));
        cpu.borrow_mut().connect_bus(main_bus.clone());

        let console = NES {
            cpu,
            main_bus,
            system_ram,
            peripheral,
            ppu,
            ppu_bus,
            pattern,
            palette,
            has_paused: true,
            uptime_clocks: 0,
            console_type,
            cartridge_port,
            tv,
            health_led: ConsoleHealth::Yellow,
        };

        console
    }

    pub fn has_paused(&self) -> bool {
        self.has_paused
    }

    pub fn press_reset(&mut self) {
        if self.cartridge_port.borrow().cartridge.is_none() {
            return;
        }

        self.health_led = ConsoleHealth::Green;
        self.cpu.borrow_mut().reset();
        self.main_bus.reset();

        let mut tv = self.tv.borrow_mut();
        tv.clear();
    }

    pub fn insert_cartridge(&mut self, cartridge: Cartridge) {
        if !cartridge.needs.is_supported() {
            let mut tv = self.tv.borrow_mut();
            tv.screen = TvContents::Error(format!("{} is unsupported!", cartridge.needs.to_string()));
            return;
        }

        if let Some(trainer) = &cartridge.trainer {
            self.main_bus.load_trainer(&trainer);
        }

        self.cartridge_port
            .borrow_mut()
            .cartridge
            .replace(cartridge);
    }

    pub fn step_frame(&mut self) {
        loop {
            if !self.tick() || !self.ppu.borrow().is_drawing()  {
                break
            }
        }
    }

    pub fn step(&mut self) {
        let rc = self.cpu.wire();
        let initial_pc = rc.borrow().program_counter();
        while rc.borrow().program_counter() == initial_pc {
            if !self.tick() {
                break
            }
        }
    }

    #[must_use]
    pub fn tick(&mut self) -> bool {
        if !matches!(self.health_led, ConsoleHealth::Green) {
            return false;
        }
        if self.cartridge_port.borrow().cartridge.is_none() {
            return false;
        }

        let tv_rc = self.tv.clone();
        let mut tv = tv_rc.borrow_mut();
        let ppu_rc = self.ppu.wire();
        let mut ppu = ppu_rc.borrow_mut();
        let ppu_type = ppu.ppu_type;
        let apu_rc = self.cpu.borrow().apu();

        let timing = tv.timing;

        if !ppu_type.timing().supports(&timing) {
            self.panic(&mut tv, format!(
                "Timing is unsupported in this Tv!. Console: {}, Tv: {}",
                ppu.ppu_type.to_string(),
                timing.to_string()
            ));
            return false;
        }
        let result = match ppu.tick() {
            Ok(o) => o,
            Err(message) => {
                self.panic(&mut tv, message);
                return false;
            }
        };

        if let Some(output) = result {
            if let TvContents::Contents(screen) = &mut tv.screen {
                let color = output.color;
                let (x, y) = output.screen_pos;

                let tv_index = 256usize * (y as usize) + (x as usize);
                Arc::get_mut(screen).unwrap()[tv_index] = color.into();

                let mut peripheral = self.peripheral.borrow_mut();
                if matches!(peripheral.1.kind(), ExpansionDeviceKinds::Zapper4017) {
                    let zapper: &mut Zapper = peripheral.1.as_any_mut().downcast_mut().unwrap();
                    zapper.scanline(y);
                }
            }
        }

        let cpu_rc = self.cpu.wire();
        let mut cpu = cpu_rc.borrow_mut();
        if self.uptime_clocks % 3 == 0 {
            let is_dma = self.main_bus.dma(ppu.oam_bytes(), self.uptime_clocks);
            drop(ppu);

            if !is_dma {
                apu_rc.borrow_mut().tick();
                if let Err(message) = cpu.tick() {
                    self.panic(&mut tv, message);
                    return false;
                }
            }
        }

        if self.main_bus.has_irq() {
            cpu.irq();
        }
        cpu.handle_interrupts();
        self.uptime_clocks += 1;
        tv.audio = apu_rc.borrow_mut().get_final_sample();

        true
    }

    pub fn dump_disassembly(&mut self, range: RangeInclusive<u16>) -> Vec<(u16, String)> {
        self.cpu.borrow().disassemble(range)
    }

    pub fn dump_bus(
        &mut self,
        range: RangeInclusive<u16>,
        dump_target: DumpTarget,
    ) -> Vec<(u16, u8)> {
        let mut to_return = Vec::with_capacity(range.len());

        match dump_target {
            DumpTarget::Cpu => {
                range.for_each(|address| {
                    to_return.push((address, self.main_bus.read(address)));
                })
            }
            DumpTarget::Ppu => {
                range.for_each(|address| {
                    to_return.push((address, self.ppu_bus.read(address)));
                })
            }
        }
        to_return
    }

    pub fn take_snapshot(&self, peripheral: Wire<(Box<dyn ExpansionDevice>, Box<dyn ExpansionDevice>)>) -> Self {
        let cartridge_port = Sharable::new(self.cartridge_port.borrow().take_snapshot());
        let new_pattern_memory = Sharable::new(self.pattern.borrow().take_snapshot(cartridge_port.wire()));
        let new_palette = Sharable::new(self.palette.borrow().clone());
        let new_ppu_bus = RcMain::new(PPUBus::new(
            new_pattern_memory.wire(),
            new_palette.wire(),
            cartridge_port.wire()
        ));
        let new_cpu = Sharable::new(self.cpu.borrow().take_snapshot_oldbus());
        let new_ppu = Sharable::new(self.ppu.borrow().take_snapshot(new_ppu_bus.clone(), new_cpu.wire()));
        cartridge_port.borrow().connect_side_channel(new_ppu.borrow_mut().deref_mut());
        let new_ram = Sharable::new(self.system_ram.borrow().clone());
        let main_bus = RcMain::new(self.main_bus.take_snapshot(new_ram.wire(), cartridge_port.wire(), new_ppu.wire(), new_cpu.borrow().apu(), peripheral.clone()));
        new_cpu.borrow_mut().connect_bus(main_bus.clone());
        Self {
            console_type: self.console_type,
            cartridge_port,
            tv: self.tv.clone(),
            health_led: self.health_led,
            cpu: new_cpu,
            main_bus,
            system_ram: new_ram,
            peripheral,
            ppu: new_ppu,
            ppu_bus: new_ppu_bus,
            pattern: new_pattern_memory,
            palette: new_palette,
            has_paused: self.has_paused,
            uptime_clocks: self.uptime_clocks,
        }
    }

    pub fn panic(&mut self, tv: &mut RefMut<Tv>, message: String) {
        tv.screen = TvContents::Error(message);
        self.health_led = ConsoleHealth::Red;
    }
}
