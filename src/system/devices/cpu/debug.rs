use std::ops::BitAnd;
use crate::system::devices::cpu::apu::APU;

use crate::system::devices::cpu::CPU6502;
use crate::system::Wire;

pub trait DevCPU {
    fn apu(&self) -> Wire<APU>;
    fn flags(&self) -> [bool; 8];
    fn program_counter(&self) -> u16;
    fn accumulator(&self) -> u8;
    fn x_register(&self) -> u8;
    fn y_register(&self) -> u8;
    fn status_register(&self) -> u8;
    fn stack_pointer(&self) -> u8;
    fn age(&self) -> u32;
    fn wait_duration(&self) -> u8;
}

impl DevCPU for CPU6502 {
    fn apu(&self) -> Wire<APU> {
        self.apu.wire()
    }

    fn flags(&self) -> [bool; 8] {
        let mut to_return = [false; 8];
        for i in 0..8 {
            to_return[i] = self.status_register.bitand(1 << i) != 0;
        }
        to_return
    }

    fn program_counter(&self) -> u16 {
        self.program_counter.0
    }

    fn accumulator(&self) -> u8 {
        self.accumulator.0
    }
    fn x_register(&self) -> u8 {
        self.x_register.0
    }
    fn y_register(&self) -> u8 {
        self.y_register.0
    }
    fn status_register(&self) -> u8 {
        self.status_register.0
    }
    fn stack_pointer(&self) -> u8 {
        self.stack_pointer.0
    }
    fn age(&self) -> u32 {
        self.age
    }

    fn wait_duration(&self) -> u8 {
        self.wait_duration
    }
}
