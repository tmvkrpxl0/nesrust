use std::ops::{BitAnd, BitOrAssign, BitXor, Not};
use strum_macros::Display;

use crate::system::devices::bus::{Bus, SystemBus};
use crate::system::devices::cpu::StatusFlags::*;
use crate::system::devices::cpu::CPU6502;
use crate::utils::{append, msb, MultiByte};

#[derive(Eq, PartialEq, Display)]
pub enum OpCode {
    ADC,
    AND,
    ASL,
    BCC,
    BCS,
    BEQ,
    BIT,
    BMI,
    BNE,
    BPL,
    BRK,
    BVC,
    BVS,
    CLC,
    CLD,
    CLI,
    CLV,
    CMP,
    CPX,
    CPY,
    DEC,
    DEX,
    DEY,
    EOR,
    INC,
    INX,
    INY,
    JMP,
    JSR,
    LDA,
    LDX,
    LDY,
    LSR,
    NOP,
    ORA,
    PHA,
    PHP,
    PLA,
    PLP,
    ROL,
    ROR,
    RTI,
    RTS,
    SBC,
    SEC,
    SED,
    SEI,
    STA,
    STX,
    STY,
    TAX,
    TAY,
    TSX,
    TXA,
    TXS,
    TYA,

    // I capture all "unofficial" opcodes with this function. It is
    // functionally identical to a NOP
    XXX,
}

impl Instruction<'_> {
    fn safe_read(bus: &SystemBus, address: Option<u16>, cpu: &mut CPU6502) -> u8 {
        if let Some(address) = address {
            cpu.last_read = bus.read(address);
        }
        cpu.last_read
    }

    pub fn can_force_extend(&self) -> bool {
        matches!(
            self.opcode,
            OpCode::BCC
                | OpCode::BCS
                | OpCode::BEQ
                | OpCode::BMI
                | OpCode::BNE
                | OpCode::BPL
                | OpCode::BVC
                | OpCode::BVS
        )
    }

    pub fn execute(
        &self,
        cpu: &mut CPU6502,
        bus: &SystemBus,
        address: Option<u16>,
        opcode: u8,
    ) -> Result<u8, String> {
        Ok(match self.opcode {
            OpCode::ADC => {
                let fetched = Instruction::safe_read(bus, address, cpu);
                let mut added: u16 = (cpu.accumulator.0 as u16) + (fetched as u16);
                if cpu.get_flag(Carry) {
                    added += 1;
                }
                cpu.set_flag(Carry, added > 255);
                cpu.set_flag(Zero, added.bitand(0x00FF) == 0);
                cpu.set_flag(Negative, msb(added.bitand(0x00FF) as u8));

                let xor: u8 = cpu
                    .accumulator
                    .bitxor(fetched)
                    .not()
                    .bitand(cpu.accumulator.bitxor(added.bitand(0x00FF) as u8));
                cpu.set_flag(Overflow, msb(xor));
                cpu.accumulator.0 = added.bitand(0x00FF) as u8;
                1
            }
            OpCode::AND => {
                let fetched = Instruction::safe_read(bus, address, cpu);
                cpu.accumulator &= fetched;
                cpu.set_flag(Zero, cpu.accumulator == 0);
                cpu.set_flag(Negative, msb(cpu.accumulator.0));
                1u8
            }
            OpCode::ASL => {
                let fetched = Instruction::safe_read(bus, address, cpu);
                let shifted = fetched << 1;

                cpu.set_flag(Carry, msb(fetched));
                cpu.set_flag(Zero, shifted == 0);
                cpu.set_flag(Negative, msb(shifted));

                if let Some(address) = address {
                    bus.write(address, shifted);
                } else {
                    cpu.accumulator.0 = shifted
                }
                0
            }
            OpCode::BCC => {
                let mut extra_duraion: u8 = 0;
                if !cpu.get_flag(Carry) {
                    extra_duraion += 1;
                    let new_pc = address.unwrap();
                    if new_pc.bitand(0xFF00) != cpu.program_counter.bitand(0xFF00) {
                        extra_duraion += 1;
                    }

                    cpu.program_counter.0 = new_pc;
                }
                extra_duraion
            }
            OpCode::BCS => {
                let mut extra_duraion: u8 = 0;
                if cpu.get_flag(Carry) {
                    extra_duraion += 1;
                    let new_pc = address.unwrap();
                    if new_pc.bitand(0xFF00) != cpu.program_counter.bitand(0xFF00) {
                        extra_duraion += 1;
                    }

                    cpu.program_counter.0 = new_pc;
                }
                extra_duraion
            }
            OpCode::BEQ => {
                let mut extra_duration: u8 = 0;
                if cpu.get_flag(Zero) {
                    extra_duration += 1;
                    let new_pc = address.unwrap();
                    if new_pc.bitand(0xFF00) != cpu.program_counter.bitand(0xFF00) {
                        extra_duration += 1;
                    }

                    cpu.program_counter.0 = new_pc;
                }
                extra_duration
            }
            OpCode::BIT => {
                let fetched = Instruction::safe_read(bus, address, cpu);
                let result = cpu.accumulator.bitand(fetched);

                cpu.set_flag(Zero, result == 0);
                cpu.set_flag(Negative, msb(fetched));
                cpu.set_flag(Overflow, fetched.bitand(0b01000000) != 0);

                0
            }
            OpCode::BMI => {
                let mut extra_duraion: u8 = 0;
                if cpu.get_flag(Negative) {
                    extra_duraion += 1;
                    let new_pc = address.unwrap();
                    if new_pc.bitand(0xFF00) != cpu.program_counter.bitand(0xFF00) {
                        extra_duraion += 1;
                    }

                    cpu.program_counter.0 = new_pc;
                }
                extra_duraion
            }
            OpCode::BNE => {
                let mut extra_duraion: u8 = 0;
                if !cpu.get_flag(Zero) {
                    extra_duraion += 1;
                    let new_pc = address.unwrap();
                    if new_pc.bitand(0xFF00) != cpu.program_counter.bitand(0xFF00) {
                        extra_duraion += 1;
                    }

                    cpu.program_counter.0 = new_pc;
                }
                extra_duraion
            }
            OpCode::BPL => {
                let mut extra_duraion: u8 = 0;
                if !cpu.get_flag(Negative) {
                    extra_duraion += 1;
                    let new_pc = address.unwrap();
                    if new_pc.bitand(0xFF00) != cpu.program_counter.bitand(0xFF00) {
                        extra_duraion += 1;
                    }

                    cpu.program_counter.0 = new_pc;
                }
                extra_duraion
            }
            OpCode::BRK => {
                cpu.program_counter += 1;

                cpu.set_flag(NoInterrupt, true);
                bus.write(
                    (cpu.stack_pointer.0 as u16) + 0x0100,
                    cpu.program_counter.higher(),
                );
                cpu.stack_pointer -= 1;
                bus.write(
                    (cpu.stack_pointer.0 as u16) + 0x0100,
                    cpu.program_counter.lower(),
                );
                cpu.stack_pointer -= 1;

                cpu.set_flag(Break, true);
                bus.write((cpu.stack_pointer.0 as u16) + 0x0100, cpu.status_register.0);
                cpu.stack_pointer -= 1;
                cpu.set_flag(Break, false);

                cpu.program_counter.0 = append(bus.read(0xFFFF), bus.read(0xFFFE));
                0
            }
            OpCode::BVC => {
                let mut extra_duraion: u8 = 0;
                if !cpu.get_flag(Overflow) {
                    extra_duraion += 1;
                    let new_pc = address.unwrap();
                    if new_pc.bitand(0xFF00) != cpu.program_counter.bitand(0xFF00) {
                        extra_duraion += 1;
                    }

                    cpu.program_counter.0 = new_pc;
                }
                extra_duraion
            }
            OpCode::BVS => {
                let mut extra_duraion: u8 = 0;
                if cpu.get_flag(Overflow) {
                    extra_duraion += 1;
                    let new_pc = address.unwrap();
                    if new_pc.bitand(0xFF00) != cpu.program_counter.bitand(0xFF00) {
                        extra_duraion += 1;
                    }

                    cpu.program_counter.0 = new_pc;
                }
                extra_duraion
            }
            OpCode::CLC => {
                cpu.set_flag(Carry, false);
                0u8
            }
            OpCode::CLD => {
                cpu.set_flag(Decimal, false);
                0u8
            }
            OpCode::CLI => {
                cpu.set_flag(NoInterrupt, false);
                0u8
            }
            OpCode::CLV => {
                cpu.set_flag(Overflow, false);
                0u8
            }
            OpCode::CMP => {
                let value = Instruction::safe_read(bus, address, cpu);

                cpu.set_flag(Carry, cpu.accumulator >= value);
                cpu.set_flag(Zero, cpu.accumulator == value);
                cpu.set_flag(Negative, msb(cpu.accumulator - value));
                1
            }
            OpCode::CPX => {
                let value = Instruction::safe_read(bus, address, cpu);

                cpu.set_flag(Carry, cpu.x_register >= value);
                cpu.set_flag(Zero, cpu.x_register == value);
                cpu.set_flag(Negative, msb(cpu.x_register - value));
                0
            }
            OpCode::CPY => {
                let value = Instruction::safe_read(bus, address, cpu);

                cpu.set_flag(Carry, cpu.y_register >= value);
                cpu.set_flag(Zero, cpu.y_register == value);
                cpu.set_flag(Negative, msb(cpu.y_register - value));
                0
            }
            OpCode::DEC => {
                let value = Instruction::safe_read(bus, address, cpu).wrapping_sub(1);

                bus.write(address.unwrap(), value);
                cpu.set_flag(Zero, value == 0);
                cpu.set_flag(Negative, msb(value));
                0
            }
            OpCode::DEX => {
                cpu.x_register -= 1;
                cpu.set_flag(Zero, cpu.x_register == 0);
                cpu.set_flag(Negative, msb(cpu.x_register.0));
                0
            }
            OpCode::DEY => {
                cpu.y_register -= 1;
                cpu.set_flag(Zero, cpu.y_register == 0);
                cpu.set_flag(Negative, msb(cpu.y_register.0));
                0
            }
            OpCode::EOR => {
                let value = Instruction::safe_read(bus, address, cpu);

                cpu.accumulator ^= value;
                cpu.set_flag(Zero, cpu.accumulator == 0);
                cpu.set_flag(Negative, msb(cpu.accumulator.0));
                1
            }
            OpCode::INC => {
                let value = Instruction::safe_read(bus, address, cpu).wrapping_add(1);

                bus.write(address.unwrap(), value);
                cpu.set_flag(Zero, value == 0);
                cpu.set_flag(Negative, msb(value));
                0
            }
            OpCode::INX => {
                cpu.x_register += 1;
                cpu.set_flag(Zero, cpu.x_register == 0);
                cpu.set_flag(Negative, msb(cpu.x_register.0));
                0
            }
            OpCode::INY => {
                cpu.y_register += 1;
                cpu.set_flag(Zero, cpu.y_register == 0);
                cpu.set_flag(Negative, msb(cpu.y_register.0));
                0
            }
            OpCode::JMP => {
                cpu.program_counter.0 = address.unwrap();
                0
            }
            OpCode::JSR => {
                cpu.program_counter -= 1;

                bus.write(
                    (cpu.stack_pointer.0 as u16) + 0x0100,
                    cpu.program_counter.higher(),
                );
                cpu.stack_pointer -= 1;
                bus.write(
                    (cpu.stack_pointer.0 as u16) + 0x0100,
                    cpu.program_counter.lower(),
                );
                cpu.stack_pointer -= 1;

                cpu.program_counter.0 = address.unwrap();
                0
            }
            OpCode::LDA => {
                let fetched = Instruction::safe_read(bus, address, cpu);
                cpu.accumulator.0 = fetched;

                cpu.set_flag(Zero, fetched == 0);
                cpu.set_flag(Negative, msb(fetched));

                1
            }
            OpCode::LDX => {
                let fetched = Instruction::safe_read(bus, address, cpu);
                cpu.x_register.0 = fetched;

                cpu.set_flag(Zero, fetched == 0);
                cpu.set_flag(Negative, msb(fetched));

                1
            }
            OpCode::LDY => {
                let fetched = Instruction::safe_read(bus, address, cpu);
                cpu.y_register.0 = fetched;

                cpu.set_flag(Zero, fetched == 0);
                cpu.set_flag(Negative, msb(fetched));

                1
            }
            OpCode::LSR => {
                let fetched = Instruction::safe_read(bus, address, cpu);

                cpu.set_flag(Carry, fetched & 0x1 != 0);
                let shifted = fetched >> 1;
                cpu.set_flag(Zero, shifted == 0);
                cpu.set_flag(Negative, msb(shifted));

                if let Some(address) = address {
                    bus.write(address, shifted);
                } else {
                    cpu.accumulator.0 = shifted;
                }
                0
            }
            OpCode::NOP => match opcode {
                0x1C | 0x3C | 0x5C | 0x7C | 0xDC | 0xFC => 1,
                _ => 0,
            },
            OpCode::ORA => {
                let fetched = Instruction::safe_read(bus, address, cpu);
                cpu.accumulator.bitor_assign(fetched);

                cpu.set_flag(Zero, cpu.accumulator == 0);
                cpu.set_flag(Negative, msb(cpu.accumulator.0));
                1
            }
            OpCode::PHA => {
                bus.write((cpu.stack_pointer.0 as u16) + 0x0100, cpu.accumulator.0);
                cpu.stack_pointer -= 1;
                0
            }
            OpCode::PHP => {
                bus.write(
                    (cpu.stack_pointer.0 as u16) + 0x0100,
                    cpu.status_register | Break.as_number() | Unused.as_number(),
                );
                cpu.set_flag(Break, false);
                cpu.set_flag(Unused, false);
                cpu.stack_pointer -= 1;
                0
            }
            OpCode::PLA => {
                cpu.stack_pointer += 1;
                cpu.accumulator.0 = bus.read((cpu.stack_pointer.0 as u16) + 0x0100);
                cpu.set_flag(Zero, cpu.accumulator == 0);
                cpu.set_flag(Negative, msb(cpu.accumulator.0));
                0
            }
            OpCode::PLP => {
                cpu.stack_pointer += 1;
                cpu.status_register.0 = bus.read((cpu.stack_pointer.0 as u16) + 0x0100);
                cpu.set_flag(Unused, true);
                0
            }
            OpCode::ROL => {
                let fetched = Instruction::safe_read(bus, address, cpu);

                let temp = ((fetched as u16) << 1 | cpu.get_flag(Carry) as u16).lower();

                cpu.set_flag(Carry, msb(fetched));
                cpu.set_flag(Zero, temp == 0);
                cpu.set_flag(Negative, msb(temp));


                if let Some(address) = address {
                    bus.write(address, temp)
                } else {
                    cpu.accumulator.0 = temp
                };
                0
            }
            OpCode::ROR => {
                let fetched = Instruction::safe_read(bus, address, cpu);

                let temp = ((cpu.get_flag(Carry) as u16) << 7 | (fetched >> 1) as u16).lower();

                cpu.set_flag(Carry, fetched & 0x1 != 0);
                cpu.set_flag(Zero, temp == 0);
                cpu.set_flag(Negative, msb(temp));

                if let Some(address) = address {
                    bus.write(address, temp)
                } else {
                    cpu.accumulator.0 = temp
                };
                0
            }
            OpCode::RTI => {
                cpu.stack_pointer += 1;

                let mut previous_state = bus.read((cpu.stack_pointer.0 as u16) + 0x0100);
                previous_state &= !Break.as_number();
                previous_state &= !Unused.as_number();

                let previous_pc = {
                    cpu.stack_pointer += 1;
                    let low = bus.read((cpu.stack_pointer.0 as u16) + 0x0100);
                    cpu.stack_pointer += 1;
                    let high = bus.read((cpu.stack_pointer.0 as u16) + 0x0100);

                    append(high, low)
                };

                cpu.program_counter.0 = previous_pc;
                cpu.status_register.0 = previous_state;
                0
            }
            OpCode::RTS => {
                let new_pc = {
                    cpu.stack_pointer += 1;
                    let low = bus.read((cpu.stack_pointer.0 as u16) + 0x0100);
                    cpu.stack_pointer += 1;
                    let high = bus.read((cpu.stack_pointer.0 as u16) + 0x0100);

                    append(high, low) + 1
                };

                cpu.program_counter.0 = new_pc;
                0
            }
            OpCode::SBC => {
                let fetched = Instruction::safe_read(bus, address, cpu).not();
                let mut added: u16 = (cpu.accumulator.0 as u16) + (fetched as u16);
                if cpu.get_flag(Carry) {
                    added += 1;
                }
                cpu.set_flag(Carry, added > 255);
                cpu.set_flag(Zero, added.bitand(0x00FF) == 0);
                cpu.set_flag(Negative, msb(added.bitand(0x00FF) as u8));

                let xor: u8 = cpu
                    .accumulator
                    .bitxor(fetched)
                    .not()
                    .bitand(cpu.accumulator.bitxor(added as u8));
                cpu.set_flag(Overflow, msb(xor.bitand(0x00FF) as u8));
                cpu.accumulator.0 = (added & 0x00FF).lower();
                1
            }
            OpCode::SEC => {
                cpu.set_flag(Carry, true);
                0
            }
            OpCode::SED => {
                cpu.set_flag(Decimal, true);
                0
            }
            OpCode::SEI => {
                cpu.set_flag(NoInterrupt, true);
                0
            }
            OpCode::STA => {
                bus.write(address.unwrap(), cpu.accumulator.0);
                0
            }
            OpCode::STX => {
                bus.write(address.unwrap(), cpu.x_register.0);
                0
            }
            OpCode::STY => {
                bus.write(address.unwrap(), cpu.y_register.0);
                0
            }
            OpCode::TAX => {
                cpu.x_register = cpu.accumulator;
                cpu.set_flag(Zero, cpu.x_register == 0);
                cpu.set_flag(Negative, msb(cpu.x_register.0));
                0
            }
            OpCode::TAY => {
                cpu.y_register = cpu.accumulator;
                cpu.set_flag(Zero, cpu.y_register == 0);
                cpu.set_flag(Negative, msb(cpu.y_register.0));
                0
            }
            OpCode::TSX => {
                cpu.x_register = cpu.stack_pointer;
                cpu.set_flag(Zero, cpu.x_register == 0);
                cpu.set_flag(Negative, msb(cpu.x_register.0));
                0
            }
            OpCode::TXA => {
                cpu.accumulator = cpu.x_register;
                cpu.set_flag(Zero, cpu.accumulator == 0);
                cpu.set_flag(Negative, msb(cpu.accumulator.0));
                0
            }
            OpCode::TXS => {
                cpu.stack_pointer = cpu.x_register;
                0
            }
            OpCode::TYA => {
                cpu.accumulator = cpu.y_register;
                cpu.set_flag(Zero, cpu.accumulator == 0);
                cpu.set_flag(Negative, msb(cpu.accumulator.0));
                0
            }
            OpCode::XXX => {
                let address = address.map(|a| format!("{}", a)).unwrap_or(String::from("{Implied}"));
                return Err(format!(
                    "Error: Unknown function detected! Work address: {}",
                    address
                ));
            }
        })
    }
}

#[derive(Eq, PartialEq, Display)]
pub enum AddressingMode {
    IMP,
    ACC, // Same as implied in practice. It's only to signify that instruction targets accumulator
    IMM,
    ZP0,
    ZPX,
    ZPY,
    REL,
    ABS,
    ABX,
    ABY,
    IND,
    IZX,
    IZY,
}

#[derive(Eq, PartialEq)]
pub struct Instruction<'a> {
    pub opcode: &'a OpCode,
    pub addressing_mode: &'a AddressingMode,
    pub duration: u8,
}

macro_rules! op {
    ($opcode:ident, $addr:ident, $duration:expr) => {
        Instruction {
            opcode: &OpCode::$opcode,
            addressing_mode: &AddressingMode::$addr,
            duration: $duration
        }
    };
}

pub const INSTRUCTIONS: [Instruction; 256] = [
    op!(BRK, IMM, 7), op!(ORA, IZX, 6), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(NOP, ZP0, 3), op!(ORA, ZP0, 3), op!(ASL, ZP0, 5), op!(XXX, IMP, 5), op!(PHP, IMP, 3), op!(ORA, IMM, 2), op!(ASL, ACC, 2), op!(XXX, IMP, 2), op!(NOP, ABS, 4), op!(ORA, ABS, 4), op!(ASL, ABS, 6), op!(XXX, IMP, 6), op!(BPL, REL, 2), op!(ORA, IZY, 5), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(NOP, ZPX, 4), op!(ORA, ZPX, 4), op!(ASL, ZPX, 6), op!(XXX, IMP, 6), op!(CLC, IMP, 2), op!(ORA, ABY, 4), op!(NOP, IMP, 2), op!(XXX, IMP, 7), op!(NOP, ABX, 4), op!(ORA, ABX, 4), op!(ASL, ABX, 7), op!(XXX, IMP, 7),
    op!(JSR, ABS, 6), op!(AND, IZX, 6), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(BIT, ZP0, 3), op!(AND, ZP0, 3), op!(ROL, ZP0, 5), op!(XXX, IMP, 5), op!(PLP, IMP, 4), op!(AND, IMM, 2), op!(ROL, ACC, 2), op!(XXX, IMP, 2), op!(BIT, ABS, 4), op!(AND, ABS, 4), op!(ROL, ABS, 6), op!(XXX, IMP, 6), op!(BMI, REL, 2), op!(AND, IZY, 5), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(NOP, ZPX, 4), op!(AND, ZPX, 4), op!(ROL, ZPX, 6), op!(XXX, IMP, 6), op!(SEC, IMP, 2), op!(AND, ABY, 4), op!(NOP, IMP, 2), op!(XXX, IMP, 7), op!(NOP, ABX, 4), op!(AND, ABX, 4), op!(ROL, ABX, 7), op!(XXX, IMP, 7),
    op!(RTI, IMP, 6), op!(EOR, IZX, 6), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(NOP, ZP0, 3), op!(EOR, ZP0, 3), op!(LSR, ZP0, 5), op!(XXX, IMP, 5), op!(PHA, IMP, 3), op!(EOR, IMM, 2), op!(LSR, ACC, 2), op!(XXX, IMP, 2), op!(JMP, ABS, 3), op!(EOR, ABS, 4), op!(LSR, ABS, 6), op!(XXX, IMP, 6), op!(BVC, REL, 2), op!(EOR, IZY, 5), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(NOP, ZPX, 4), op!(EOR, ZPX, 4), op!(LSR, ZPX, 6), op!(XXX, IMP, 6), op!(CLI, IMP, 2), op!(EOR, ABY, 4), op!(NOP, IMP, 2), op!(XXX, IMP, 7), op!(NOP, ABX, 4), op!(EOR, ABX, 4), op!(LSR, ABX, 7), op!(XXX, IMP, 7),
    op!(RTS, IMP, 6), op!(ADC, IZX, 6), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(NOP, ZP0, 3), op!(ADC, ZP0, 3), op!(ROR, ZP0, 5), op!(XXX, IMP, 5), op!(PLA, IMP, 4), op!(ADC, IMM, 2), op!(ROR, ACC, 2), op!(XXX, IMP, 2), op!(JMP, IND, 5), op!(ADC, ABS, 4), op!(ROR, ABS, 6), op!(XXX, IMP, 6), op!(BVS, REL, 2), op!(ADC, IZY, 5), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(NOP, ZPX, 4), op!(ADC, ZPX, 4), op!(ROR, ZPX, 6), op!(XXX, IMP, 6), op!(SEI, IMP, 2), op!(ADC, ABY, 4), op!(NOP, IMP, 2), op!(XXX, IMP, 7), op!(NOP, ABX, 4), op!(ADC, ABX, 4), op!(ROR, ABX, 7), op!(XXX, IMP, 7),
    op!(NOP, IMM, 2), op!(STA, IZX, 6), op!(NOP, IMM, 2), op!(XXX, IMP, 6), op!(STY, ZP0, 3), op!(STA, ZP0, 3), op!(STX, ZP0, 3), op!(XXX, IMP, 3), op!(DEY, IMP, 2), op!(NOP, IMM, 2), op!(TXA, IMP, 2), op!(XXX, IMP, 2), op!(STY, ABS, 4), op!(STA, ABS, 4), op!(STX, ABS, 4), op!(XXX, IMP, 4), op!(BCC, REL, 2), op!(STA, IZY, 6), op!(XXX, IMP, 2), op!(XXX, IMP, 6), op!(STY, ZPX, 4), op!(STA, ZPX, 4), op!(STX, ZPY, 4), op!(XXX, IMP, 4), op!(TYA, IMP, 2), op!(STA, ABY, 5), op!(TXS, IMP, 2), op!(XXX, IMP, 5), op!(NOP, ABX, 5), op!(STA, ABX, 5), op!(XXX, IMP, 5), op!(XXX, IMP, 5),
    op!(LDY, IMM, 2), op!(LDA, IZX, 6), op!(LDX, IMM, 2), op!(XXX, IMP, 6), op!(LDY, ZP0, 3), op!(LDA, ZP0, 3), op!(LDX, ZP0, 3), op!(XXX, IMP, 3), op!(TAY, IMP, 2), op!(LDA, IMM, 2), op!(TAX, IMP, 2), op!(XXX, IMP, 2), op!(LDY, ABS, 4), op!(LDA, ABS, 4), op!(LDX, ABS, 4), op!(XXX, IMP, 4), op!(BCS, REL, 2), op!(LDA, IZY, 5), op!(XXX, IMP, 2), op!(XXX, IMP, 5), op!(LDY, ZPX, 4), op!(LDA, ZPX, 4), op!(LDX, ZPY, 4), op!(XXX, IMP, 4), op!(CLV, IMP, 2), op!(LDA, ABY, 4), op!(TSX, IMP, 2), op!(XXX, IMP, 4), op!(LDY, ABX, 4), op!(LDA, ABX, 4), op!(LDX, ABY, 4), op!(XXX, IMP, 4),
    op!(CPY, IMM, 2), op!(CMP, IZX, 6), op!(NOP, IMM, 2), op!(XXX, IMP, 8), op!(CPY, ZP0, 3), op!(CMP, ZP0, 3), op!(DEC, ZP0, 5), op!(XXX, IMP, 5), op!(INY, IMP, 2), op!(CMP, IMM, 2), op!(DEX, IMP, 2), op!(XXX, IMP, 2), op!(CPY, ABS, 4), op!(CMP, ABS, 4), op!(DEC, ABS, 6), op!(XXX, IMP, 6), op!(BNE, REL, 2), op!(CMP, IZY, 5), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(NOP, ZPX, 4), op!(CMP, ZPX, 4), op!(DEC, ZPX, 6), op!(XXX, IMP, 6), op!(CLD, IMP, 2), op!(CMP, ABY, 4), op!(NOP, IMP, 2), op!(XXX, IMP, 7), op!(NOP, ABX, 4), op!(CMP, ABX, 4), op!(DEC, ABX, 7), op!(XXX, IMP, 7),
    op!(CPX, IMM, 2), op!(SBC, IZX, 6), op!(NOP, IMM, 2), op!(XXX, IMP, 8), op!(CPX, ZP0, 3), op!(SBC, ZP0, 3), op!(INC, ZP0, 5), op!(XXX, IMP, 5), op!(INX, IMP, 2), op!(SBC, IMM, 2), op!(NOP, IMP, 2), op!(SBC, IMM, 2), op!(CPX, ABS, 4), op!(SBC, ABS, 4), op!(INC, ABS, 6), op!(XXX, IMP, 6), op!(BEQ, REL, 2), op!(SBC, IZY, 5), op!(XXX, IMP, 2), op!(XXX, IMP, 8), op!(NOP, ZPX, 4), op!(SBC, ZPX, 4), op!(INC, ZPX, 6), op!(XXX, IMP, 6), op!(SED, IMP, 2), op!(SBC, ABY, 4), op!(NOP, IMP, 2), op!(XXX, IMP, 7), op!(NOP, ABX, 4), op!(SBC, ABX, 4), op!(INC, ABX, 7), op!(XXX, IMP, 7),
];
