use std::fs::{File, remove_file};
use std::io::Write;
use std::ops::{BitAnd, BitAndAssign, BitOrAssign, Not, RangeInclusive};
use std::rc::Rc;

use crate::system::devices::bus::{Bus, SystemBus};
use crate::system::devices::bus::ppu::PPUType;
use crate::system::devices::cpu::apu::APU;
use crate::system::devices::cpu::instructions::{AddressingMode, INSTRUCTIONS};
use crate::system::devices::cpu::instructions::AddressingMode::*;
use crate::system::devices::cpu::StatusFlags::{
    Break, Carry, Decimal, Negative, NoInterrupt, Overflow, Unused, Zero,
};
use crate::system::Sharable;
use crate::utils::{append, MultiByte, Wrapping};

pub mod debug;
mod instructions;
pub mod apu;

static LOG_CPU: bool = false;

pub struct CPU6502 {
    apu: Sharable<APU>,
    bus: Option<Rc<SystemBus>>,
    accumulator: Wrapping<u8>,
    x_register: Wrapping<u8>,
    y_register: Wrapping<u8>,
    stack_pointer: Wrapping<u8>,
    program_counter: Wrapping<u16>,
    status_register: Wrapping<u8>,
    wait_duration: u8,
    irq: bool,
    nmi: bool,
    last_read: u8,
    pub(crate) log_file: File,
    age: u32,
}

pub enum StatusFlags {
    Carry,
    Zero,
    NoInterrupt,
    Decimal, // Unused
    Break,
    Unused,
    Overflow,
    Negative,
}

impl StatusFlags {
    fn as_number(&self) -> u8 {
        match self {
            Carry => 0b00000001,
            Zero => 0b00000010,
            NoInterrupt => 0b00000100,
            Decimal => 0b00001000,
            Break => 0b00010000,
            Unused => 0b00100000,
            Overflow => 0b01000000,
            Negative => 0b10000000,
        }
    }

    pub fn as_char(&self) -> char {
        match self {
            Carry => 'C',
            Zero => 'Z',
            NoInterrupt => 'I',
            Decimal => 'D',
            Break => 'B',
            Unused => '-',
            Overflow => 'V',
            Negative => 'N',
        }
    }
}

impl CPU6502 {
    pub fn new(ppu_type: PPUType) -> CPU6502 {
        let _ = remove_file("log.txt");
        Self {
            apu: Sharable::new(APU::new(ppu_type)),
            bus: None,
            accumulator: Wrapping(0),
            x_register: Wrapping(0),
            y_register: Wrapping(0),
            stack_pointer: Wrapping(0),
            program_counter: Wrapping(0),
            status_register: Wrapping(0),
            wait_duration: 0,
            irq: false,
            nmi: false,
            last_read: 0,
            log_file: File::options()
                .create(true)
                .read(true)
                .write(true)
                .open("log.txt")
                .unwrap(),
            age: 0,
        }
    }

    pub fn connect_bus(&mut self, bus: Rc<SystemBus>) {
        self.bus.replace(bus);
    }

    pub fn tick(&mut self) -> Result<(), String> {
        let bus = self.bus.as_ref().unwrap().clone();

        if self.wait_duration == 0 {
            self.set_flag(Unused, true);
            let log_pc = self.program_counter.0;
            let mut log_cache = Vec::new();
            let mut reference = if LOG_CPU { Some(&mut log_cache)} else { None };
            let mut pc = self.program_counter.0;
            let opcode =  self.sequential_read(&bus, &mut pc, &mut reference);
            self.program_counter.0 = pc;
            let instruction = &INSTRUCTIONS[opcode as usize];

            let (address, has_extended) = self.resolve_address(&bus, instruction.addressing_mode, &mut reference);
            let execution_result = instruction.execute(self, &bus, address, opcode)?;

            let extra_duration = if has_extended || instruction.can_force_extend() {
                execution_result
            } else {
                0
            };

            self.wait_duration = instruction.duration + extra_duration;
            self.set_flag(Unused, true);

            if LOG_CPU {
                let mut file = File::options()
                    .create(true)
                    .read(true)
                    .write(true)
                    .append(true)
                    .open("log.txt")
                    .unwrap();
                let contents = format!(
                    "{:10}:{:02} PC:{:04X} {} A:{:02X} X:{:02X} Y:{:02X} {}{}{}{}{}{}{}{} STKP:{:02X}\n",
                    self.age,
                    self.wait_duration,
                    log_pc,
                    instruction.opcode.to_string(),
                    self.accumulator,
                    self.x_register,
                    self.y_register,
                    if self.get_flag(Negative) { "N" } else { "." },
                    if self.get_flag(Overflow) { "V" } else { "." },
                    if self.get_flag(Unused) { "U" } else { "." },
                    if self.get_flag(Break) { "B" } else { "." },
                    if self.get_flag(Decimal) { "D" } else { "." },
                    if self.get_flag(NoInterrupt) { "I" } else { "." },
                    if self.get_flag(Zero) { "Z" } else { "." },
                    if self.get_flag(Carry) { "C" } else { "." },
                    self.stack_pointer
                );
                file.write_all(contents.as_bytes()).unwrap();
            };
        }

        self.age += 1;
        self.wait_duration -= 1;

        Ok(())
    }

    pub fn handle_interrupts(&mut self) {
        if self.nmi {
            self.nmi = false;
            self.force_interrupt(&self.bus.as_ref().unwrap().clone());
        }

        if self.irq {
            self.irq = false;
            self.maskable_interrupt(&self.bus.as_ref().unwrap().clone())
        }
    }

    pub fn reset(&mut self) {
        let bus = self.bus.as_ref().unwrap();

        self.program_counter.0 = {
            let low = bus.read(0xFFFC);
            let high = bus.read(0xFFFD);

            append(high, low)
        };

        self.accumulator.0 = 0;
        self.x_register.0 = 0;
        self.y_register.0 = 0;
        self.stack_pointer.0 = 0xFD;
        self.status_register.0 = Unused.as_number();

        self.wait_duration = 8;

        self.apu.borrow_mut().reset();
    }

    pub fn irq(&mut self) {
        self.irq = true;
    }

    pub fn nmi(&mut self) {
        self.nmi = true;
    }

    fn maskable_interrupt(&mut self, bus: &SystemBus) {
        if self.get_flag(NoInterrupt) {
            return;
        }

        bus.write(
            (self.stack_pointer.0 as u16) + 0x0100,
            self.program_counter.higher(),
        );
        self.stack_pointer -= 1;
        bus.write(
            (self.stack_pointer.0 as u16) + 0x0100,
            self.program_counter.lower(),
        );
        self.stack_pointer -= 1;

        self.set_flag(Break, false);
        self.set_flag(Unused, true);
        self.set_flag(NoInterrupt, true);

        bus.write(
            (self.stack_pointer.0 as u16) + 0x0100,
            self.status_register.0,
        );
        self.stack_pointer -= 1;

        let new_pc = {
            let low = bus.read(0xFFFE);
            let high = bus.read(0xFFFF);

            append(high, low)
        };

        self.program_counter.0 = new_pc;
        self.wait_duration = 7;
    }

    fn force_interrupt(&mut self, bus: &SystemBus) {
        bus.write(
            (self.stack_pointer.0 as u16) + 0x0100,
            self.program_counter.higher(),
        );
        self.stack_pointer -= 1;
        bus.write(
            (self.stack_pointer.0 as u16) + 0x0100,
            self.program_counter.lower(),
        );
        self.stack_pointer -= 1;

        self.set_flag(Break, false);
        self.set_flag(Unused, true);
        self.set_flag(NoInterrupt, true);

        bus.write(
            (self.stack_pointer.0 as u16) + 0x0100,
            self.status_register.0,
        );
        self.stack_pointer -= 1;

        let new_pc = {
            let low = bus.read(0xFFFA);
            let high = bus.read(0xFFFB);

            append(high, low)
        };

        self.program_counter.0 = new_pc;
        self.wait_duration = 8;
    }

    fn sequential_read(&self, bus: &SystemBus, address: &mut u16, log_cache: &mut Option<&mut Vec<u8>>) -> u8 {
        let result = bus.read(*address);
        if let Some(log_cache) = log_cache {
            log_cache.push(result)
        }
        *address = address.wrapping_add(1);
        result
    }

    fn resolve_address_readonly(
        &self,
        bus: &SystemBus,
        mode: &AddressingMode,
        current_address: &mut u16,
        log_cache: &mut Option<&mut Vec<u8>>
    )  -> (Option<u16>, bool) {
        match mode {
            IMP | ACC => { (None, false) },
            IMM => {
                let to_return = (Some(*current_address), false);
                self.sequential_read(bus, current_address, log_cache);
                to_return
            }
            ZP0 => {
                let value =  self.sequential_read(bus, current_address, log_cache);
                (Some(value as u16), false)
            }
            ZPX => {
                let value = (self.sequential_read(bus, current_address, log_cache) as u16
                    + self.x_register.0 as u16)
                    & 0x00FF;
                (Some(value), false)
            }
            ZPY => {
                let value = (self.sequential_read(bus, current_address, log_cache) as u16
                    + self.y_register.0 as u16)
                    & 0x00FF;
                (Some(value), false)
            }
            REL => {
                let relative_offset: i8 =  self.sequential_read(bus, current_address, log_cache) as i8;
                let absolute_address = ((*current_address as i16) + relative_offset as i16) as u16;
                (Some(absolute_address), false)
            }
            ABS => {
                let low =  self.sequential_read(bus, current_address, log_cache);
                let high =  self.sequential_read(bus, current_address, log_cache);
                let address = append(high, low);
                (Some(address), false)
            }
            ABX => {
                let low =  self.sequential_read(bus, current_address, log_cache);
                let high =  self.sequential_read(bus, current_address, log_cache);
                let combined = append(high, low);

                let address = combined.overflowing_add(self.x_register.0 as u16).0;

                let is_next_page = (address & 0xFF00) != (combined & 0xFF00);
                (Some(address), is_next_page)
            }
            ABY => {
                let low =  self.sequential_read(bus, current_address, log_cache);
                let high =  self.sequential_read(bus, current_address, log_cache);
                let address = append(high, low).wrapping_add(self.y_register.0 as u16);

                let is_next_page = low.overflowing_add(self.y_register.0).1;
                (Some(address), is_next_page)
            }
            IND => {
                let low =  self.sequential_read(bus, current_address, log_cache);
                let high =  self.sequential_read(bus, current_address, log_cache);
                let pointer = append(high, low);
                let address = if low == 0xFF { // Emulate Hardware bug
                    append(bus.read(pointer & 0xFF00), bus.read(pointer))
                } else {
                    append(bus.read(pointer + 1), bus.read(pointer))
                };
                (Some(address), false)
            }

            // This is the confusing bit
            // IZX: reads an offset from pc, and reads from (offset + x) to get final address
            // IZY: reads an offset from pc, and reads from (offset) and then add y to what it just read = final address
            // ?????
            // Also these 2 can only specify address that is in zero page.
            IZX => {
                let mut offset =  self.sequential_read(bus, current_address, log_cache).wrapping_add(self.x_register.0) as u16;

                let address: u16 = {
                    let low =  self.sequential_read(bus, &mut offset, log_cache);
                    offset &= 0xFF;
                    let high =  self.sequential_read(bus, &mut offset, log_cache);

                    append(high, low)
                };

                (Some(address), false)
            }
            IZY => {
                let mut offset =  self.sequential_read(bus, current_address, log_cache) as u16;

                let low =  self.sequential_read(bus, &mut offset, log_cache);
                offset &= 0xFF;
                let high =  self.sequential_read(bus, &mut offset, log_cache);

                let combined: u16 = append(high, low).wrapping_add(self.y_register.0 as u16);
                (Some(combined), low.overflowing_add(self.y_register.0).1)
            }
        }
    }

    fn resolve_address(
        &mut self,
        bus: &SystemBus,
        mode: &AddressingMode,
        log_cache: &mut Option<&mut Vec<u8>>
    ) -> (Option<u16>, bool) {
        let mut pc = self.program_counter.0;
        let to_return = self.resolve_address_readonly(bus, mode, &mut pc, log_cache);
        self.program_counter.0 = pc;
        if matches!(mode, AddressingMode::IMP | AddressingMode::ACC) {
            self.last_read = self.accumulator.0;
        };
        to_return
    }

    fn get_flag(&self, flag: StatusFlags) -> bool {
        self.status_register.bitand(flag.as_number()) != 0
    }

    fn set_flag(&mut self, flag: StatusFlags, new_status: bool) {
        if new_status {
            self.status_register.bitor_assign(flag.as_number())
        } else {
            self.status_register.bitand_assign(flag.as_number().not())
        }
    }

    pub fn disassemble(&self, range: RangeInclusive<u16>) -> Vec<(u16, String)> {
        let bus = self.bus.as_ref().unwrap();
        let mut current_address = *range.start();
        let mut lines = Vec::new();
        let mut line_address: u16;

        while current_address <= *range.end() {
            line_address = current_address;

            // Prefix line with instruction address
            let mut line = String::with_capacity(6);

            line += format!("${:04X}: ", current_address).as_str();

            // Read instruction, and get its readable name
            let opcode = bus.read(current_address);
            current_address += 1;
            let instruction = &INSTRUCTIONS[opcode as usize];
            let decoded = {
                let absolute = self.resolve_address_readonly(
                    bus,
                    instruction.addressing_mode,
                    &mut current_address,
                    &mut None
                );
                match absolute.0 {
                    None => String::from("None"),
                    Some(address) => format!("{:04X}", address),
                }
            };
            line += format!(
                "{} at {}({})",
                instruction.opcode.to_string(),
                decoded,
                instruction.addressing_mode.to_string()
            )
            .as_str();

            lines.push((line_address, line));
        }

        lines
    }

    pub fn take_snapshot_oldbus(&self) -> Self {
        Self {
            apu: Sharable::new(self.apu.borrow().clone()),
            bus: self.bus.clone(),
            accumulator: self.accumulator,
            x_register: self.x_register,
            y_register: self.y_register,
            stack_pointer: self.stack_pointer,
            program_counter: self.program_counter,
            status_register: self.status_register,
            wait_duration: self.wait_duration,
            irq: self.irq,
            nmi: self.nmi,
            last_read: self.last_read,
            log_file: self.log_file.try_clone().unwrap(),
            age: self.age,
        }
    }
}
