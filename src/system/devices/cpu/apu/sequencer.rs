use crate::utils::Wrapping;

#[derive(Clone)]
pub struct Sequencer<I: Iterator> {
    counter: Wrapping<u16>,
    pub target: u16, // period
    pub iterator: I,
}

impl<I: Iterator> Sequencer<I> {
    pub fn new(iterator: I) -> Sequencer<I> {
        Self {
            counter: Wrapping(0),
            target: 0,
            iterator,
        }
    }

    pub fn clock(&mut self) -> (bool, Option<I::Item>) {
        self.counter += 1;
        if self.counter == self.target {
            self.counter.0 = 0;
            (true, self.iterator.next())
        } else {
            (false, None)
        }
    }

    pub fn reset(&mut self) {
        self.counter.0 = 0;
    }
}
