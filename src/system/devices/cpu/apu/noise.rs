use crate::system::devices::cpu::apu::divider::Divider;
use crate::system::devices::cpu::apu::envelop::Envelop;
use crate::system::devices::cpu::apu::lc::LengthCounter;
use crate::system::Timing;
use crate::utils::msb;

#[derive(Clone)]
pub struct NoiseGenerator {
    pub is_enabled: bool,
    timer: Divider,
    pub envelop: Envelop,
    pub lc: LengthCounter,
    lfsr: LinearFeedback,
    mode: bool,
    timing: Timing,
}

impl NoiseGenerator {
    pub fn new(timing: Timing) -> Self {
        let mut timer = Divider::new();
        timer.set_period(1);
        timer.counter = 1;
        Self {
            is_enabled: false,
            timer,
            envelop: Envelop::new(),
            lc: LengthCounter::new(),
            lfsr: LinearFeedback::new(),
            mode: false,
            timing,
        }
    }

    pub fn clock(&mut self) {
        if self.timer.clock() {
            self.lfsr.clock(self.mode);
        }
    }

    pub fn is_muted(&self) -> bool {
        !self.is_enabled || self.lc.should_mute()
    }

    pub fn output(&self) -> f32 {
        if self.lfsr.register & 0b1 != 0 || self.is_muted() {
            return 0.0
        }
        (self.envelop.output() as f32) / 16.0
    }

    pub fn on_write(&mut self, address: u16, data: u8) {
        match address {
            0x400C => {
                self.envelop.configure(data);
                self.lc.configure(data);
            }
            0x400E => {
                let period = data & 0b00001111;
                self.timer.period = self.timing.noise_period()[period as usize];
                self.lc.is_halt = msb(data);
            }
            0x400F => {
                self.lc.configure(data);
            }
            _ => {}
        }
    }
}

#[derive(Clone)]
struct LinearFeedback {
    register: u16,
}

impl LinearFeedback {
    pub fn new() -> Self {
        Self {
            register: 0xFFFF,
        }
    }
    pub fn clock(&mut self, mode: bool) {
        let feedback = if mode {
            let bit6 = self.register & 0b1000000 != 0;
            let bit0 = self.register & 0b0000001 != 0;
            bit6 ^ bit0
        } else {
            let bit1 = self.register & 0b10 != 0;
            let bit0 = self.register & 0b01 != 0;
            bit1 ^ bit0
        };
        self.register >>= 1;
        if feedback {
            self.register |= 0b100000000000000;
        }
    }
}