use crate::system::devices::cpu::apu::divider::Divider;
use crate::system::devices::cpu::apu::lc::LengthCounter;
use crate::system::devices::cpu::apu::envelop::Envelop;
use crate::system::devices::cpu::apu::RAW_SQUARE;
use crate::system::devices::cpu::apu::sequencer::Sequencer;
use crate::system::devices::cpu::apu::sweeper::Sweeper;
use crate::system::Timing;
use crate::utils::square_wave;

#[derive(Clone)]
pub struct PulseGenerator {
    pub is_enabled: bool,
    pub timer: Divider,
    pub sequencer: Sequencer<PulseSequencer>,
    pub duty_cycle: f32,
    pub lc: LengthCounter,
    pub sweeper: Sweeper,
    pub envelop: Envelop,
    pub output: f32,
    timing: Timing,
}

impl PulseGenerator {
    pub fn new(timing: Timing, is_pulse2: bool) -> Self {
        let mut timer = Divider::new();
        timer.counter = 1;
        timer.set_period(1);
        Self {
            is_enabled: false,
            timer,
            sequencer: Sequencer::new(PulseSequencer { sequence: 0 }),
            duty_cycle: 0.0,
            lc: LengthCounter::new(),
            sweeper: Sweeper::new(is_pulse2),
            envelop: Envelop::new(),
            output: 0.0,
            timing,
        }
    }

    pub fn is_muted(&self) -> bool {
        !self.is_enabled || self.lc.should_mute() || self.sweeper.is_muted(&self.sequencer.target)
    }

    pub fn clock(&mut self, time: f64) {
        if self.timer.clock() {
            let sequence = self.sequencer.clock();
            let volume = self.envelop.output();
            if RAW_SQUARE {
                if let (_, Some(output)) = sequence {
                    if self.is_muted() || !output {
                        self.output = 0.0;
                    } else {
                        self.output = (volume as f32) / 16.0
                    }
                }
            } else {
                self.output = square_wave(time, self.frequency(self.timing.cpu_clock_speed() as f32).into(), self.duty_cycle.into(), ((volume + 1) as f64) / 16.0) as f32;
            }
        }
    }
    fn frequency(&self, cpu_clock_speed: f32) -> f32 {
        cpu_clock_speed / (16 * (self.sequencer.target + 1)) as f32
    }

    pub fn on_write(&mut self, address: u16, data: u8) {
        match address {
            0x4000 | 0x4004 => {
                self.envelop.configure(data);
                let bit5 = data & 0b00100000 != 0;
                self.lc.is_halt = bit5;
                match (data & 0xC0) >> 6 {
                    0 => {
                        self.duty_cycle = 0.125;
                        self.sequencer.iterator.sequence = 0b01000000;
                    }
                    1 => {
                        self.duty_cycle = 0.250;
                        self.sequencer.iterator.sequence = 0b01100000;
                    }
                    2 => {
                        self.duty_cycle = 0.5;
                        self.sequencer.iterator.sequence = 0b01111000;
                    }
                    3 => {
                        self.duty_cycle = 0.75;
                        self.sequencer.iterator.sequence = 0b10011111;
                    }
                    _ => unreachable!()
                };
            },
            0x4001 | 0x4005 => {
                self.sweeper.configure(data);
            }
            0x4002 | 0x4006 => {
                self.sequencer.target = (self.sequencer.target & 0xFF00) | data as u16;
            }
            0x4003 | 0x4007 => {
                self.sequencer.target = (((data & 0x07) as u16) << 8) | (self.sequencer.target & 0x00FF);
                self.sequencer.reset();
                self.envelop.is_start = true;
                self.lc.configure(data);
            }
            _ => {}
        }
    }
}

#[derive(Clone)]
pub struct PulseSequencer {
    sequence: u8,
}

impl Iterator for PulseSequencer {
    type Item = bool;

    fn next(&mut self) -> Option<Self::Item> {
        let lsb = self.sequence & 0b01;
        self.sequence = (self.sequence & 0xFE) >> 1;
        self.sequence |= lsb << 7;
        Some(lsb != 0)
    }
}
