use crate::system::devices::cpu::apu::divider::Divider;

#[derive(Clone)]
pub struct Envelop {
    pub is_start: bool,
    pub decay: u16,
    pub divider: Divider,
    pub is_const: bool,
    pub is_loop: bool,
}

impl Envelop {
    pub fn new() -> Self {
        Self {
            is_start: true,
            decay: 0,
            divider: Divider::new(),
            is_const: false,
            is_loop: false,
        }
    }

    pub fn clock(&mut self) {
        if !self.is_start {
            if self.divider.clock() {
                if self.decay == 0 {
                    if self.is_loop {
                        self.decay = 15;
                    }
                } else {
                    self.decay -= 1;
                }
            }
        } else {
            self.is_start = false;
            self.decay = 15;
        }
    }

    pub fn output(&self) -> u16 {
        if self.is_const {
            self.divider.period
        } else {
            self.decay
        }
    }

    pub fn configure(&mut self, data: u8) {
        self.is_loop = data & 0b00100000 != 0;
        self.is_const = data & 0b00010000 != 0;
        self.divider.period = (data & 0b00001111) as u16;
        self.is_start = true;
    }
}
