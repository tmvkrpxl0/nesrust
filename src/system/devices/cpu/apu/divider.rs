#[derive(Clone)]
pub struct Divider {
    pub counter: u16,
    pub period: u16,
}

impl Divider {
    pub fn new() -> Self {
        Divider {
            counter: 0,
            period: 0,
        }
    }

    pub fn clock(&mut self) -> bool {
        if self.counter == 0 {
            self.counter = self.period;
            true
        } else {
            self.counter -= 1;
            false
        }
    }

    pub fn reload(&mut self) {
        self.counter = self.period;
    }

    pub fn set_period(&mut self, period: u16) {
        self.period = period
    }
}