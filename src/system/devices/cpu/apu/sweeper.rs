use crate::system::devices::cpu::apu::divider::Divider;
use crate::utils::msb;

#[derive(Clone)]
pub struct Sweeper {
    is_enabled: bool,
    divider: Divider,
    negate: bool,
    shift: u8,
    reload: bool,
    is_pulse2: bool,
}

impl Sweeper {
    pub fn new(is_pulse2: bool) -> Self {
        Self {
            is_enabled: false,
            divider: Divider::new(),
            negate: false,
            shift: 0,
            reload: false,
            is_pulse2,
        }
    }
    fn target(&self, pulse_period: &u16) -> u16 {
        let pulse_period = *pulse_period as i32;
        let mut change = pulse_period.wrapping_shr(self.shift as u32);
        if self.negate {
            change = -change;
            if !self.is_pulse2 {
                change -= 1;
            }
        };

        ((pulse_period + change).max(0)) as u16
    }

    pub fn is_muted(&self, pulse_period: &u16) -> bool{
        if *pulse_period < 8 {
            return true
        }
        if self.target(pulse_period) > 0x07FF {
            return true
        }
        false
    }

    fn is_disabled(&self) -> bool {
        !self.is_enabled || self.shift == 0
    }

    pub fn clock(&mut self, pulse_period: &mut u16) {
        let divider_signal = self.divider.clock();
        if divider_signal && !self.is_disabled() && !self.is_muted(pulse_period) {
            *pulse_period = self.target(pulse_period);
        }
        if divider_signal || self.reload {
            self.divider.reload();
            self.reload = false;
        }
    }

    pub fn configure(&mut self, data: u8) {
        self.is_enabled = msb(data);
        self.divider.set_period(((data & 0b01110000) >> 4) as u16);
        self.negate = data & 0b00001000 != 0;
        self.shift = data & 0b00000111;
        self.reload = true;
    }
}
