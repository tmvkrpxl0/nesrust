use crate::system::devices::cpu::apu::divider::Divider;
use crate::system::devices::cpu::apu::frame_counter::StepMode::{Five, Four};
use crate::system::devices::cpu::apu::sequencer::Sequencer;
use crate::utils::msb;

// TODO Support PAL as well
#[derive(Clone)]
pub struct FrameCounter {
    divider: Divider,
    sequencer: Sequencer<FrameIter>,
    disable_interrupt: bool,
    pub has_irq: bool,
    signal_cache: Option<FrameSignal>,
    side_effect: Option<(bool, u8)>,
}

impl FrameCounter {
    pub fn new() -> Self {
        let mut divider = Divider::new();
        divider.counter = 1;
        divider.set_period(1);

        Self {
            divider,
            sequencer: Sequencer::new(FrameIter { step_mode: Four(0), last_signal: FrameSignal::Half }),
            disable_interrupt: false,
            has_irq: false,
            signal_cache: None,
            side_effect: None,
        }
    }

    pub fn clock(&mut self) -> Option<FrameSignal> {
        if self.divider.clock() {
            let (has_generated, value) = self.sequencer.clock();
            if has_generated {
                match &mut self.sequencer.iterator.step_mode {
                    Four(step) => {
                        if *step == 1 {
                            self.sequencer.target = 3729
                        }
                        if *step == 3 {
                            self.sequencer.target = 3728
                        }
                        self.signal_cache = value;
                        *step += 1;
                        *step %= 4;
                    }
                    Five(step) => {
                        if *step == 1 {
                            self.sequencer.target = 3729
                        }
                        if *step == 3 {
                            self.sequencer.target = 3726
                        }
                        if *step == 4 {
                            self.sequencer.target = 3728
                        }
                        self.signal_cache = value;
                        *step += 1;
                        *step %= 5;
                    }
                }
            }
            None
        } else {
            let mut to_return = self.signal_cache.take();
            let mut clear_side_effect = false;
            if let Some((send_half, count)) = &mut self.side_effect {
                *count -= 1;
                if *count == 0 {
                    self.sequencer.reset();
                    clear_side_effect = true;
                    if *send_half {
                        to_return = Some(FrameSignal::Half);
                    }
                }
            }
            if clear_side_effect {
                self.side_effect = None;
            }
            to_return
        }
    }

    pub fn configure(&mut self, data: u8) {
        self.sequencer.iterator.step_mode = if msb(data) {
            Five(0)
        } else {
            Four(0)
        };
        self.sequencer.iterator.last_signal = FrameSignal::Half;
        self.disable_interrupt = data & 0b01000000 != 0;
        if self.disable_interrupt {
            self.has_irq = false;
        }
        self.side_effect = Some((msb(data), 2))
    }
}

#[derive(Clone)]
enum StepMode {
    Four(u8),
    Five(u8)
}

#[derive(Copy, Clone)]
pub enum FrameSignal {
    Quarter,
    Half,
}

#[derive(Clone)]
struct FrameIter {
    step_mode: StepMode,
    last_signal: FrameSignal,
}

impl Iterator for FrameIter {
    type Item = FrameSignal;

    fn next(&mut self) -> Option<Self::Item> {
        if let Five(step) = &mut self.step_mode {
            if *step == 3 {
                return None
            }
        }
        self.last_signal = match &self.last_signal {
            FrameSignal::Quarter => FrameSignal::Half,
            FrameSignal::Half => FrameSignal::Quarter
        };
        Some(self.last_signal)
    }
}
