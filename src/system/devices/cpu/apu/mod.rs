use pulse::PulseGenerator;
use crate::system::devices::bus::ppu::PPUType;
use crate::system::devices::bus::SystemBus;
use crate::system::devices::BusDevice;
use crate::system::devices::cpu::apu::frame_counter::{FrameCounter, FrameSignal};
use crate::system::devices::cpu::apu::noise::NoiseGenerator;
use crate::system::devices::cpu::apu::triangle::TriangleGenerator;
use crate::system::Timing;

static RAW_SQUARE: bool = false;

mod sequencer;
mod pulse;
mod envelop;
mod sweeper;
mod noise;
mod lc;
mod divider;
mod frame_counter;
mod triangle;

#[derive(Clone)]
pub struct APU {
    pub timing: Timing,
    frame_counter: FrameCounter,
    pulse: [PulseGenerator; 2],
    noise: NoiseGenerator,
    triangle: TriangleGenerator,

    pub config: [bool; 5],
    time_elapsed : f64,
    age: u32,
}

impl APU {
    pub fn new(ppu_type: PPUType) -> Self {
        let timing = ppu_type.timing();
        Self {
            timing,
            frame_counter: FrameCounter::new(),
            pulse: [PulseGenerator::new(timing, false), PulseGenerator::new(timing, true)],
            config: [true; 5],
            noise: NoiseGenerator::new(timing),
            time_elapsed: 0.0,
            age: 0,
            triangle: TriangleGenerator::new(timing),
        }
    }

    pub fn has_irq(&self) -> bool {
        self.frame_counter.has_irq
    }

    pub fn tick(&mut self) {
        let time_step = 1.0 / self.timing.cpu_clock_speed() as f64;
        self.time_elapsed += time_step;

        if let Some(signal) = self.frame_counter.clock() {
            let at_quarter = true;
            let at_half = matches!(signal, FrameSignal::Half);

            if at_quarter { // adjust volume
                self.pulse[0].envelop.clock();
                self.pulse[1].envelop.clock();
                self.noise.envelop.clock();
                self.triangle.linear.clock();
            }

            if at_half { // adjust length & frequency
                self.pulse[0].lc.clock();
                self.pulse[1].lc.clock();
                self.noise.lc.clock();
                self.triangle.lc.clock();

                self.pulse[0].sweeper.clock(&mut self.pulse[0].sequencer.target);
                self.pulse[1].sweeper.clock(&mut self.pulse[1].sequencer.target);
            }
        }
        self.pulse[0].clock(self.time_elapsed);
        self.pulse[1].clock(self.time_elapsed);
        self.noise.clock();
        self.triangle.clock(self.time_elapsed);

        self.age += 1;
    }

    pub fn reset(&mut self) {
        self.time_elapsed = 0.0;
    }

    pub fn get_final_sample(&mut self) -> f32 {
        let pulse_out = {
            let pulse1 = if !self.config[0] { 0.0 } else { self.pulse[0].output };
            let pulse2 = if !self.config[1] { 0.0 } else { self.pulse[1].output };
            let sum = pulse1 + pulse2;
            if sum == 0.0 {
                0.0
            } else {
                95.88 / ((8128.0 / sum) + 100.0)
            }
        };
        let tnd_out = {
            let triangle = if !self.config[2] { 0.0 } else { self.triangle.output() };
            let noise = if !self.config[3] { 0.0 } else { self.noise.output() };
            let dmc = 0.0;
            let sum = (triangle / 8227.0) + (noise / 12241.0) + (dmc / 22638.0);
            if sum == 0.0 {
                0.0
            } else {
                159.79 / ((1.0 / sum) + 100.0)
            }
        };
        pulse_out + tnd_out
    }
}

impl ToString for APU {
    fn to_string(&self) -> String {
        format!("APU[{}]", self.timing.to_string())
    }
}

impl BusDevice<SystemBus> for APU {
    const MIN_ADDRESS: u16 = 0x4000;
    const MAX_ADDRESS: u16 = 0x4017;

    fn on_read(&mut self, _: &SystemBus, address: u16) -> Option<u8> {
        if address == 0x4015 {
            let pulse1 = !self.pulse[0].lc.has_stopped() as u8;
            let pulse2 = !self.pulse[1].lc.has_stopped() as u8;
            let triangle = !self.triangle.lc.has_stopped() as u8;
            let noise = self.noise.lc.has_stopped() as u8;
            let dmc = 0;
            // open bus
            let irq = self.frame_counter.has_irq as u8;
            let dmc_irq = 0;

            self.frame_counter.has_irq = false;

            return Some((dmc_irq << 7) | (irq << 6) | (dmc << 4) | (noise << 3) | (triangle << 2) | (pulse2 << 1) | pulse1)
        }
        None
    }

    fn on_write(&mut self, _: &SystemBus, address: u16, data: u8) {
        let register = (address & 0b11100) >> 2;
        match register {
            0 => self.pulse[0].on_write(address, data),
            1 => self.pulse[1].on_write(address, data),
            2 => self.triangle.on_write(address, data),
            3 => self.noise.on_write(address, data),
            4 => {} // dmc
            _ => {
                if address == 0x4015 {
                    let pulse1 = data & 0b00000001 != 0;
                    let pulse2 = data & 0b00000010 != 0;
                    let triangle = data & 0b00000100 != 0;
                    let noise = data & 0b00001000 != 0;
                    #[allow(unused_variables)]
                    let dmc = data & 0b00010000;

                    self.pulse[0].is_enabled = pulse1;
                    self.pulse[0].lc.is_enabled = pulse1;
                    self.pulse[1].is_enabled = pulse2;
                    self.pulse[1].lc.is_enabled = pulse1;

                    self.triangle.is_enabled = triangle;

                    self.noise.is_enabled = noise;
                }
                if address == 0x4017 {
                    self.frame_counter.configure(data);
                }
            }
        }
    }
}
