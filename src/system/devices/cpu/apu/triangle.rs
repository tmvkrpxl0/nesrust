use std::iter::{Cloned, Cycle};
use std::slice::Iter;
use crate::system::devices::cpu::apu::divider::Divider;
use crate::system::devices::cpu::apu::lc::LengthCounter;
use crate::system::devices::cpu::apu::sequencer::Sequencer;
use crate::system::Timing;
use crate::utils::triangle_wave;

static TRIANGLE: [u8; 32] = [15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

static RAW_TRIANGLE: bool = false;

#[derive(Clone)]
pub struct TriangleGenerator {
    pub is_enabled: bool,
    pub timing: Timing,
    timer: Divider,
    pub lc: LengthCounter,
    sequencer: Sequencer<Cloned<Cycle<Iter<'static, u8>>>>,
    pub linear: LinearCounter,
    output: f32,
}

impl TriangleGenerator {
    pub fn new(timing: Timing) -> Self {
        let mut sequencer = Sequencer::new(TRIANGLE.iter().cycle().cloned());
        sequencer.target = 1;
        Self {
            is_enabled: false,
            timing,
            timer: Divider::new(),
            lc: LengthCounter::new(),
            sequencer,
            linear: LinearCounter::new(),
            output: 0.0,
        }
    }

    ///
    ///       Linear Counter   Length Counter
    ///             |                |
    ///             v                v
    /// Timer ---> Gate ----------> Gate ---> Sequencer ---> (to mixer)
    pub fn clock(&mut self, time: f64) {
        if self.timer.clock() {
            if self.linear.counter != 0 {
                if RAW_TRIANGLE {
                    let level = self.sequencer.clock().1.unwrap();
                    self.output = (level as f32) / 15.0
                } else {
                    self.output = triangle_wave(time, self.frequency()) as f32
                }
            }
        }
    }

    pub fn frequency(&self) -> f64 {
        self.timing.cpu_clock_speed() as f64 / (32 * (self.timer.period + 1)) as f64
    }

    pub fn output(&self) -> f32 {
        if !self.is_enabled && self.lc.should_mute() {
            0.0
        } else {
            self.output
        }
    }

    pub fn on_write(&mut self, address: u16, data: u8) {
        match address {
            0x4008 => {
                let bit7 = data & 0b10000000 != 0;
                let reload = data & 0b01111111;

                self.lc.is_halt = bit7;
                self.linear.control = bit7;
                self.linear.reload = reload;
            }
            0x400A => {
                self.timer.period = (self.timer.period & 0xFF00) | data as u16;
            }
            0x400B => {
                self.lc.configure(data);
                self.timer.period = (((data & 0x07) as u16) << 8) | (self.timer.period & 0x00FF);
                self.linear.should_reload = true;
            }
            _ => {}
        }
    }
}

#[derive(Clone)]
pub struct LinearCounter {
    counter: u8,
    reload: u8,
    control: bool,
    should_reload: bool,
}

impl LinearCounter {
    pub fn new() -> Self {
        Self {
            counter: 0,
            reload: 0,
            control: false,
            should_reload: false,
        }
    }

    pub fn clock(&mut self) {
        if self.should_reload {
            self.counter = self.reload;
        } else {
            if self.counter != 0 {
                self.counter -= 1;
            }
        }

        if !self.control {
            self.should_reload = false;
        }
    }
}
