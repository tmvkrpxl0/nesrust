static LENGTH_TABLE: [u8; 32] = [
    10, 254, 20, 2, 40, 4, 80, 6,
    160, 8, 60, 10, 14, 12, 26, 14,
    12, 16, 24, 18, 48, 20, 96, 22,
    192, 24, 72, 26, 16, 28, 32, 30
];


#[derive(Clone)]
pub struct LengthCounter {
    pub is_enabled: bool,
    pub is_halt: bool,
    pub counter: u8,
}

impl LengthCounter {
    pub fn new() -> Self {
        Self {
            is_enabled: false,
            is_halt: false,
            counter: 0,
        }
    }

    pub fn has_stopped(&self) -> bool {
        self.counter == 0 || self.is_halt
    }

    pub fn clock(&mut self) -> u8 {
        if !self.is_enabled {
            self.counter = 0;
        } else {
            if !self.has_stopped() {
                self.counter -= 1;
            }
        }

        self.counter
    }

    pub fn configure(&mut self, data: u8) {
        self.counter = LENGTH_TABLE[((data & 0b11111000) >> 3) as usize];
    }

    pub fn should_mute(&self) -> bool {
        self.counter == 0
    }
}
