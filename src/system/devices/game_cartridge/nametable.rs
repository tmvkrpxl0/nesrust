use crate::system::devices::game_cartridge::NameTableMirroring;

#[derive(Clone)]
pub struct NameTable {
    pub tables: [[u8; 0x400]; 2],
}

impl NameTable {
    pub fn new() -> Self {
        Self {
            tables: [[0u8; 1024]; 2],
        }
    }

    // TODO expand this for more types of mirroring
    fn pick(&self, mut address: u16, mirroring: NameTableMirroring) -> Option<(usize, usize)> {
        address &= 0x0FFF;

        // Here assumes there are only 2 possible mirroring: Vertical and Horizontal
        let is_vertical = if let NameTableMirroring::Vertical = mirroring {
            1
        } else {
            0
        };
        let is_horizontal = if let NameTableMirroring::Vertical = mirroring {
            0
        } else {
            1
        };
        if (0x0000..=0x03FF).contains(&address) {
            Some((0, (address & 0x03FF) as usize))
        } else if (0x0400..=0x07FF).contains(&address) {
            Some((is_vertical, (address & 0x03FF) as usize))
        } else if (0x0800..=0x0BFF).contains(&address) {
            Some((is_horizontal, (address & 0x03FF) as usize))
        } else if (0x0C00..=0x0FFF).contains(&address) {
            Some((1, (address & 0x03FF) as usize))
        } else {
            None
        }

        /*
        Original code for better readability
        if let NameTableMirroring::Vertical = &self.mirroring {
            if (0x0000..=0x03FF).contains(&address) {
                Some((0, (address & 0x03FF) as usize))
            } else if (0x0400..=0x07FF).contains(&address) {
                Some((1, (address & 0x03FF) as usize))
            } else if (0x0800..=0x0BFF).contains(&address) {
                Some((0, (address & 0x03FF) as usize))
            } else if (0x0C00..=0x0FFF).contains(&address) {
                Some((1, (address & 0x03FF) as usize))
            } else {
                None
            }
        } else if let NameTableMirroring::Horizontal = &self.mirroring {
            if (0x0000..=0x03FF).contains(&address) {
                Some((0, (address & 0x03FF) as usize))
            } else if (0x0400..=0x07FF).contains(&address) {
                Some((0, (address & 0x03FF) as usize))
            } else if (0x0800..=0x0BFF).contains(&address) {
                Some((1, (address & 0x03FF) as usize))
            } else if (0x0C00..=0x0FFF).contains(&address) {
                Some((1, (address & 0x03FF) as usize))
            } else {
                None
            }
        } else {
            None
        }*/
    }

    pub fn read(&self, address: u16, mirroring: NameTableMirroring) -> Option<u8> {
        self.pick(address, mirroring).map(|v| self.tables[v.0][v.1])
    }

    pub fn write(&mut self, address: u16, data: u8, mirroring: NameTableMirroring) {
        if let Some(v) = self.pick(address, mirroring) {
            self.tables[v.0][v.1] = data
        }
    }
}

impl ToString for NameTable {
    fn to_string(&self) -> String {
        String::from("NameTable")
    }
}
