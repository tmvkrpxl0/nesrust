use crate::system::devices::game_cartridge::Cartridge;
use crate::system::devices::game_cartridge::mappers::{BusConflict, Mapper};

#[derive(Clone)]
pub struct Mapper2 {
    first_bank_index: u8,
    bus_conflict: BusConflict,
}

impl Mapper2 {
    pub fn new(submapper_id: u8) -> Self {
        Self {
            first_bank_index: 0,
            bus_conflict: BusConflict::new_237(submapper_id),
        }
    }
}

impl Mapper for Mapper2 {
    fn read(&mut self, mut address: u16, cartridge: &Cartridge) -> Option<u8> {
        let is_ram = cartridge.character.len() == 0;

        if address <= 0x1FFF {
            if is_ram {
                &cartridge.character_ram
            } else {
                &cartridge.character
            }.get(address as usize).cloned()
        } else if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.read(address, cartridge.hard_mirroring)
        } else if 0x8000 <= address {
            let is_second = address & 0x4000 != 0;
            address &= 0x3FFF;
            if !is_second {
                cartridge.program.get(((self.first_bank_index as usize) << 14) | address as usize).copied()
            } else {
                let last_bank = (cartridge.program_banks - 1) as usize;
                Some(cartridge.program[(last_bank << 14) | address as usize])
            }
        } else {
            None
        }
    }

    fn write(&mut self, address: u16, mut data: u8, cartridge: &mut Cartridge) {
        let is_ram = cartridge.character.len() == 0;
        if address <= 0x1FFF && is_ram {
            cartridge.character_ram[address as usize] = data
        } else if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.write(address, data, cartridge.hard_mirroring);
        } else if 0x8000 <= address {
            if self.bus_conflict.can_conflict(true) {
                if let Some(own_output) = self.read(address, cartridge) {
                    data &= own_output;
                    if self.bus_conflict.should_log() {
                        eprintln!("Mapper 2: Bus conflict detected at address {:04X}", address);
                    }
                }
            }

            let mut new_index = 0;
            if cartridge.program_banks >= 2 {
                new_index |= data & 0b0001;
            }
            if cartridge.program_banks >= 4 {
                new_index |= data & 0b0010;
            }
            if cartridge.program_banks >= 8 {
                new_index |= data & 0b0100;
            }
            if cartridge.program_banks >= 16 {
                new_index |= data & 0b1000;
            }
            self.first_bank_index = new_index;
        }
    }

    fn reset(&mut self) {
        self.first_bank_index = 0;
    }

    fn clone_box(&self) -> Box<dyn Mapper> {
        Box::new(self.clone())
    }
}