use crate::system::devices::game_cartridge::{Cartridge, NameTableMirroring};
use crate::system::devices::game_cartridge::mappers::Mapper;
use crate::system::devices::game_cartridge::NameTableMirroring::{FourScreen, Horizontal, Vertical};

#[derive(Clone)]
pub struct MMC3 {
    registers: [u8; 8],

    targeted_register: usize,
    swap_first_prg: bool,
    quad_first_chr: bool,

    prg_sub_bank_count: u8,
    prg_banks: [u8; 4],
    chr_banks: [u8; 8],

    mirroring: NameTableMirroring,

    is_ram_readonly: bool,
    is_ram_enabled: bool,

    irq_reset_to: u8,
    irq_counter: u8,
    irq_enabled: bool,
    irq_active: bool,
}

impl MMC3 {
    pub fn new(program_banks: u8) -> Self {
        Self {
            registers: [0; 8],
            targeted_register: 0,
            swap_first_prg: false,
            quad_first_chr: false,
            prg_sub_bank_count: program_banks * 2,
            prg_banks: [0, 1, (program_banks * 2) - 2, (program_banks * 2) - 1],
            chr_banks: [0; 8],
            mirroring: Horizontal,
            is_ram_readonly: false,
            is_ram_enabled: false,
            irq_reset_to: 0,
            irq_counter: 0,
            irq_enabled: false,
            irq_active: false,
        }
    }
    fn is_ram_writable(&self) -> bool {
        self.is_ram_enabled && !self.is_ram_readonly
    }

    fn mirroring(&self, cartridge: &Cartridge) -> NameTableMirroring {
        if let FourScreen = cartridge.hard_mirroring {
            FourScreen
        } else {
            self.mirroring
        }
    }
}

impl Mapper for MMC3 {
    fn read(&mut self, address: u16, cartridge: &Cartridge) -> Option<u8> {
        if address <= 0x1FFF {
            let index = (address & 0x1C00) as usize >> 10;
            Some(cartridge.character[(self.chr_banks[index] as usize) << 10 | (address & 0x03FF) as usize])
        } else if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.read(address, self.mirroring(cartridge))
        } else if (0x6000..=0x7FFF).contains(&address) {
            if self.is_ram_enabled {
                Some(cartridge.program_ram[(address & 0x1FFF) as usize])
            } else {
                None
            }
        } else if 0x8000 <= address {
            let index = (address & 0x6000) as usize >> 13;
            Some(cartridge.program[(self.prg_banks[index] as usize) << 13 | (address & 0x1FFF) as usize])
        } else {
            None
        }
    }

    fn write(&mut self, address: u16, data: u8, cartridge: &mut Cartridge) {
        if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.write(address, data, self.mirroring(cartridge));
        } else if (0x6000..=0x7FFF).contains(&address) {
            if self.is_ram_writable() {
                cartridge.program_ram[(address & 0x1FFF) as usize] = data
            }
        } else if (0x8000..=0x9FFF).contains(&address) {
            if address % 2 == 0 {
                self.targeted_register = (data & 0b00000111) as usize;
                self.swap_first_prg = data & 0b01000000 != 0;
                self.quad_first_chr = data & 0b10000000 != 0;
            } else {
                self.registers[self.targeted_register] = data;

                if self.quad_first_chr {
                    self.chr_banks[0] = self.registers[2];
                    self.chr_banks[1] = self.registers[3];
                    self.chr_banks[2] = self.registers[4];
                    self.chr_banks[3] = self.registers[5];
                    self.chr_banks[4] = self.registers[0] & 0xFE;
                    self.chr_banks[5] = (self.registers[0] & 0xFE   ) + 1;
                    self.chr_banks[6] = self.registers[1] & 0xFE;
                    self.chr_banks[7] = (self.registers[1] & 0xFE) + 1;
                } else {
                    self.chr_banks[0] = self.registers[0] & 0xFE;
                    self.chr_banks[1] = (self.registers[0] & 0xFE) + 1;
                    self.chr_banks[2] = self.registers[1] & 0xFE;
                    self.chr_banks[3] = (self.registers[1] & 0xFE) + 1;
                    self.chr_banks[4] = self.registers[2];
                    self.chr_banks[5] = self.registers[3];
                    self.chr_banks[6] = self.registers[4];
                    self.chr_banks[7] = self.registers[5];
                }

                if self.swap_first_prg {
                    self.prg_banks[2] = self.registers[6] & 0x3F;
                    self.prg_banks[0] = self.prg_sub_bank_count - 2;
                } else {
                    self.prg_banks[0] = self.registers[6] & 0x3F;
                    self.prg_banks[2] = self.prg_sub_bank_count - 2;
                }
                self.prg_banks[1] = self.registers[7] & 0x3F;
                self.prg_banks[3] = self.prg_sub_bank_count - 1;
            }

        } else if (0xA000..=0xBFFF).contains(&address) {
            if address % 2 == 0 {
                self.mirroring = if data & 0b00000001 != 0 {
                    Horizontal
                } else {
                    Vertical
                };
            } else {
                self.is_ram_readonly = data & 0b01000000 != 0;
                self.is_ram_enabled = data & 0b10000000 != 0;
            }
        } else if (0xC000..=0xDFFF).contains(&address) {
            if address % 2 == 0 {
                self.irq_reset_to = data;
            } else {
                self.irq_counter = 0;
            }
        } else if (0xE000..=0xFFFF).contains(&address) {
            if address % 2 == 0 {
                self.irq_active = false;
                self.irq_enabled = false;
            } else {
                self.irq_enabled = true;
            }
        }
    }

    fn reset(&mut self) {
        self.registers.fill(0);
        self.targeted_register = 0;
        self.swap_first_prg = false;
        self.quad_first_chr = false;
        self.mirroring = Horizontal;

        self.irq_active = false;
        self.irq_enabled = false;
        self.irq_counter = 0;
        self.irq_reset_to = 0;

        self.prg_banks[0] = 0;
        self.prg_banks[1] = 1;
        self.prg_banks[2] = self.prg_sub_bank_count - 2;
        self.prg_banks[3] = self.prg_sub_bank_count - 1;
        self.chr_banks.fill(0);
    }

    fn clone_box(&self) -> Box<dyn Mapper> {
        Box::new(self.clone())
    }

    fn take_irq(&mut self) -> bool {
        let irq = self.irq_active;
        self.irq_active = false;
        irq
    }

    fn on_scanline(&mut self) {
        if self.irq_counter == 0 {
            self.irq_counter = self.irq_reset_to;
        } else {
            self.irq_counter -= 1;
        }

        if self.irq_counter == 0 && self.irq_enabled {
            self.irq_active = true;
        }
    }
}
