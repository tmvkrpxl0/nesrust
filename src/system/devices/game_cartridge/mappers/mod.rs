mod mapper0;
mod mapper2;
mod mapper3;
mod mapper66;
mod mapper1;
mod mapper4;

use crate::system::devices::bus::expansions::ExpansionDeviceKinds;
use crate::system::devices::game_cartridge::{Cartridge, ConsoleType};
use crate::system::Timing;
use crate::system::devices::game_cartridge::mappers::BusConflict::{No, Unspecified, Yes};
use crate::system::devices::game_cartridge::mappers::mapper0::Mapper0;
use crate::system::devices::game_cartridge::mappers::mapper1::MMC1;
use crate::system::devices::game_cartridge::mappers::mapper2::Mapper2;
use crate::system::devices::game_cartridge::mappers::mapper3::Mapper3;
use crate::system::devices::game_cartridge::mappers::mapper4::MMC3;
use crate::system::devices::game_cartridge::mappers::mapper66::Mapper66;

pub trait Mapper {
    fn read(&mut self, address: u16, cartridge: &Cartridge) -> Option<u8>;
    fn write(&mut self, address: u16, data: u8, cartridge: &mut Cartridge);
    fn reset(&mut self) {}
    fn clone_box(&self) -> Box<dyn Mapper>;
    fn take_irq(&mut self) -> bool {
        false
    }

    fn on_scanline(&mut self) {}
}

#[derive(Copy, Clone)]
enum BusConflict {
    Unspecified,
    No,
    Yes
}

impl BusConflict {
    fn new_237(submapper: u8) -> Self {
        match submapper {
            0 => Unspecified,
            1 => No,
            2 => Yes,
            _ => unreachable!()
        }
    }

    fn can_conflict(&self, is_mapper2_3: bool) -> bool {
        match self {
            Unspecified => is_mapper2_3,
            Yes => true,
            No => false
        }
    }

    fn should_log(&self) -> bool {
        match self {
            Unspecified => true,
            _ => false
        }
    }
}

#[allow(unused_variables)]
pub fn create_mapper(
    id: u16,
    sub_id: u8,
    program_banks: u16,
    character_banks: u16,
    made_for: &ConsoleType,
    timing: &Timing,
    needs: &ExpansionDeviceKinds,
) -> Result<Box<dyn Mapper>, String> {
    println!("Mapper {}/{} detected", id, sub_id);
    Ok(match id {
        0 => {
            let is_32k = program_banks > 1;
            Box::new(Mapper0::new(is_32k))
        }
        1 => {
            assert!(program_banks < 256);
            Box::new(MMC1::new(program_banks as u8))
        }
        2 => {
            Box::new(Mapper2::new(sub_id))
        }
        3 => {
            Box::new(Mapper3::new(sub_id))
        }
        4 => {
            assert!(program_banks < 256);
            Box::new(MMC3::new(program_banks as u8))
        }
        66 => {
            Box::new(Mapper66::new())
        }
        _ => return Err(format!("Unknown mapper! {}/{}", id, sub_id)),
    })
}
