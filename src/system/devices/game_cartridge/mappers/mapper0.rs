use crate::system::devices::game_cartridge::Cartridge;
use crate::system::devices::game_cartridge::mappers::Mapper;

#[derive(Clone)]
pub struct Mapper0 {
    rom_32k: bool,
}

impl Mapper0 {
    pub fn new(rom_32k: bool) -> Self {
        Self {
            rom_32k,
        }
    }
}

impl Mapper for Mapper0 {
    fn read(&mut self, address: u16, cartridge: &Cartridge) -> Option<u8> {
        let is_ram = cartridge.character.is_empty();
        if address <= 0x1FFF {
            Some(if !is_ram { cartridge.character[address as usize] } else { cartridge.character_ram[address as usize] })
        } else if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.read(address, cartridge.hard_mirroring)
        } else if (0x6000..=0x7FFF).contains(&address) {
            Some(cartridge.program_ram[(address & 0x1FFF) as usize])
        } else if (0x8000..=0xFFFF).contains(&address) {
            let index = if self.rom_32k {
                address & 0x7FFF
            } else {
                address & 0x3FFF
            } as usize;
            Some(cartridge.program[index])
        } else {
            None
        }
    }

    fn write(&mut self, address: u16, data: u8, cartridge: &mut Cartridge) {
        if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.write(address, data, cartridge.hard_mirroring);
        } else if (0x6000..=0x7FFF).contains(&address) {
            cartridge.program_ram[(address & 0x1FFF) as usize] = data;
        }
    }

    fn reset(&mut self) {}

    fn clone_box(&self) -> Box<dyn Mapper> {
        Box::new(self.clone())
    }
}