use crate::system::devices::game_cartridge::Cartridge;
use crate::system::devices::game_cartridge::mappers::Mapper;

#[derive(Clone)]
pub struct Mapper66 {
    prg_bank_index: u8,
    chr_bank_index: u8,
}

impl Mapper66 {
    pub fn new() -> Self {
        Self {
            prg_bank_index: 0,
            chr_bank_index: 0,
        }
    }
}

impl Mapper for Mapper66 {
    fn read(&mut self, address: u16, cartridge: &Cartridge) -> Option<u8> {
        if address <= 0x1FFF {
            let index = (self.chr_bank_index as usize * 0x2000) + address as usize;
            cartridge.character.get(index).cloned()
        } else if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.read(address, cartridge.hard_mirroring)
        } else if 0x8000 <= address {
            let index = (self.prg_bank_index as usize * 0x8000) + (address as usize - 0x8000);
            cartridge.program.get(index).cloned()
        } else {
            None
        }
    }

    fn write(&mut self, address: u16, data: u8, cartridge: &mut Cartridge) {
        if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.write(address, data, cartridge.hard_mirroring);
        } else if 0x8000 <= address {
            /*if let Some(conflict) = self.read(address, cartridge) {
                data &= conflict;
            }*/
            let chr_index = data & 0b00001111;
            let prg_index = (data & 0b11110000) >> 4;

            if cartridge.program_banks >= 2 {
                self.prg_bank_index |= prg_index & 0b0001;
            }
            if cartridge.program_banks >= 4 {
                self.prg_bank_index |= prg_index & 0b0010;
            }
            if cartridge.program_banks >= 8 {
                self.prg_bank_index |= prg_index & 0b0100;
            }
            if cartridge.program_banks >= 16 {
                self.prg_bank_index |= prg_index & 0b0100;
            }

            if cartridge.character_banks >= 2 {
                self.chr_bank_index |= chr_index & 0b0001;
            }
            if cartridge.character_banks >= 4 {
                self.chr_bank_index |= chr_index & 0b0010;
            }
            if cartridge.character_banks >= 8 {
                self.chr_bank_index |= chr_index & 0b0100;
            }
            if cartridge.character_banks >= 16 {
                self.chr_bank_index |= chr_index & 0b0100;
            }
        }
    }

    fn reset(&mut self) {
        self.chr_bank_index = 0;
        self.prg_bank_index = 0;
    }

    fn clone_box(&self) -> Box<dyn Mapper> {
        Box::new(self.clone())
    }
}
