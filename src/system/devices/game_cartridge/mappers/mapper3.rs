use crate::system::devices::game_cartridge::Cartridge;
use crate::system::devices::game_cartridge::mappers::{BusConflict, Mapper};

#[derive(Clone)]
pub struct Mapper3 {
    chr_bank_index: u8,
    bus_conflict: BusConflict,
}

impl Mapper3 {
    pub fn new(submapper: u8) -> Self {
        Self {
            chr_bank_index: 0,
            bus_conflict: BusConflict::new_237(submapper),
        }
    }
}

impl Mapper for Mapper3 {
    fn read(&mut self, address: u16, cartridge: &Cartridge) -> Option<u8> {
        if address <= 0x1FFF {
            let index = (self.chr_bank_index as usize) << 13 | address as usize;
            Some(cartridge.character[index])
        } else if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.read(address, cartridge.hard_mirroring)
        } else if 0x8000 <= address {
            let index = (address - 0x8000) as usize;
            cartridge.program.get(index).cloned()
        } else {
            None
        }
    }

    fn write(&mut self, address: u16, mut data: u8, cartridge: &mut Cartridge) {
        if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.write(address, data, cartridge.hard_mirroring);
        } else if 0x8000 <= address {
            if self.bus_conflict.can_conflict(true) {
                if let Some(own_output) = self.read(address, cartridge) {
                    data &= own_output;
                    if self.bus_conflict.should_log() {
                        eprintln!("Mapper 3: Bus conflict detected at address {:04X}", address);
                    }
                }
            }

            if cartridge.character_banks > 4 {
                self.chr_bank_index = data;
            } else {
                self.chr_bank_index = data & 0b11;
            }
        }
    }

    fn reset(&mut self) {
        self.chr_bank_index = 0;
    }

    fn clone_box(&self) -> Box<dyn Mapper> {
        Box::new(self.clone())
    }
}
