use crate::system::devices::game_cartridge::{Cartridge, NameTableMirroring};
use crate::system::devices::game_cartridge::mappers::Mapper;
use crate::system::devices::game_cartridge::mappers::mapper1::CharacterBankMode::Separated;
use crate::system::devices::game_cartridge::mappers::mapper1::ProgramBankMode::{FixFirst, FixSecond};
use crate::system::devices::game_cartridge::NameTableMirroring::{Horizontal, SingleScreenA, SingleScreenB, Vertical};
use crate::utils::msb;

// This lacks support for emulating consecutive write ignore
#[derive(Clone)]
pub struct MMC1 {
    custom_mirroring: NameTableMirroring,
    prg_banks: u8,
    prg_ram_bank: u8 /* upper 2 bits of address*/,
    shift_register: ShiftRegister,
    prg_first: u8, /* upper 5 bits of address*/
    prg_second: u8, /* upper 5 bits of address*/
    prg_combined: u8, /* upper 4 bits of address*/
    prg_bank_mode: ProgramBankMode,
    chr_first: u8, /* upper 5 bits of address*/
    chr_second: u8, /* upper 5 bits of address*/
    chr_combined: u8, /* upper 4 bits of address*/
    chr_bank_mode: CharacterBankMode,
}

#[derive(Clone)]
enum ProgramBankMode {
    FixFirst,
    FixSecond,
    Combined,
}

#[derive(Clone)]
enum CharacterBankMode {
    Separated,
    Combined,
}

impl MMC1 {
    pub fn new(prg_banks: u8,) -> Self {
        Self {
            custom_mirroring: Horizontal,
            prg_banks,
            prg_ram_bank: 0,
            shift_register: ShiftRegister::new(),
            prg_first: 0,
            prg_second: 0,
            prg_combined: 0,
            prg_bank_mode: ProgramBankMode::Combined,
            chr_first: 0,
            chr_second: 0,
            chr_combined: 0,
            chr_bank_mode: CharacterBankMode::Combined,
        }
    }

    fn remap_chr(&self, mut address: u16) -> usize {
        let last_bits = match &self.chr_bank_mode {
            Separated => {
                let is_second = address & 0x1000 != 0;
                address &= 0x0FFF;
                (if !is_second {
                    self.chr_first
                } else {
                    self.chr_second
                } as usize) << 12
            }
            CharacterBankMode::Combined => {
                (self.chr_combined as usize) << 13
            }
        };
        last_bits | address as usize
    }

    fn remap_prg(&self, mut address: u16) -> usize {
        let last_bits = match &self.prg_bank_mode {
            FixFirst => {
                let is_second = address & 0x4000 != 0;
                address &= 0x3FFF;
                (if !is_second {
                    0
                } else {
                    self.prg_second
                } as usize) << 14
            }
            FixSecond => {
                let is_second = address & 0x4000 != 0;
                address &= 0x3FFF;
                (if !is_second {
                    self.prg_first
                } else {
                    self.prg_banks - 1
                } as usize) << 14
            }
            ProgramBankMode::Combined => {
                address &= 0x7FFF;
                (self.prg_combined as usize) << 15
            }
        };
        last_bits | address as usize
    }

    fn remap_pram(&self, mut address: u16) -> usize {
        address &= 0x1FFF;
        ((self.prg_ram_bank as usize) << 13) | address as usize
    }
}

impl Mapper for MMC1 {
    fn read(&mut self, address: u16, cartridge: &Cartridge) -> Option<u8> {
        if address <= 0x1FFF {
            Some(if cartridge.character.is_empty() {
                &cartridge.character_ram
            } else {
                &cartridge.character
            }[self.remap_chr(address)])
        } else if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.read(address, self.custom_mirroring)
        } else if (0x6000..=0x7FFF).contains(&address) {
            if !cartridge.program_ram.is_empty() {
                Some(cartridge.program_ram[self.remap_pram(address)])
            } else {
                None
            }
        } else if 0x8000 <= address {
            Some(cartridge.program[self.remap_prg(address)])
        } else {
            None
        }
    }

    fn write(&mut self, address: u16, data: u8, cartridge: &mut Cartridge) {
        if address <= 0x1FFF {
            if !cartridge.character_ram.is_empty() {
                cartridge.character_ram[self.remap_chr(address)] = data;
            }
        } else if (0x2000..=0x3EFF).contains(&address) {
            cartridge.nametable.write(address, data, self.custom_mirroring);
        } else if (0x6000..=0x7FFF).contains(&address) {
            if !cartridge.program_ram.is_empty() {
                cartridge.program_ram[self.remap_pram(address)] = data;
            }
        } else if 0x8000 <= address {
            if msb(data) {
                self.prg_bank_mode = FixSecond;
                self.shift_register.clear();
                return;
            }

            self.shift_register.push(data & 0b00000001 != 0);
            if self.shift_register.count == 5 {
                let value = self.shift_register.clear();
                let register = (address & 0b0110000000000000) >> 13;

                match register {
                    0 => {
                        let mirroring = value & 0b11;
                        let prg_bank_mode = (value & 0b1100) >> 2;
                        let chr_bank_mode = (value & 0b10000) >> 4;

                        self.custom_mirroring = match mirroring {
                            0 => SingleScreenA,
                            1 => SingleScreenB,
                            2 => Vertical,
                            3 => Horizontal,
                            _ => unreachable!()
                        };
                        match prg_bank_mode {
                            0 | 1 => ProgramBankMode::Combined,
                            2 => FixFirst,
                            3 => FixSecond,
                            _ => unreachable!()
                        };
                        self.chr_bank_mode = match chr_bank_mode {
                            0 => CharacterBankMode::Combined,
                            1 => Separated,
                            _ => unreachable!()
                        };
                    }
                    1 | 2 => {
                        let mut chr_data = 0;
                        let mut prg_ram_data = 0;
                        let mut prg_rom_data = 0;

                        if !matches!(self.chr_bank_mode, CharacterBankMode::Combined) {
                            chr_data |= value & 0b00001;
                        }
                        if cartridge.character.len() >= 16384 {
                            chr_data |= value & 0b00010;
                        }
                        if cartridge.character.len() >= 32768 {
                            chr_data |= value & 0b00100;
                        }
                        if cartridge.character.len() >= 65536 {
                            chr_data |= value & 0b01000;
                        }
                        if cartridge.character.len() == 131072 {
                            chr_data |= value & 0b10000;
                        }

                        if cartridge.program_ram.len() == 32768 {
                            prg_ram_data |= (value & 0b01100) >> 2;

                        }
                        if cartridge.program_ram.len() == 16384 {
                            prg_ram_data |= (value & 0b01000) >> 3;
                        }
                        if cartridge.program.len() == 524288 {
                            prg_rom_data |= value & 0b1000;
                        }

                        if register == 1 {
                            // First CHR bank
                            match &mut self.chr_bank_mode {
                                Separated => self.chr_first = chr_data,
                                CharacterBankMode::Combined => self.chr_combined = chr_data,
                            }

                            match &mut self.prg_bank_mode {
                                FixSecond => self.prg_first = (self.prg_first & 0b01111) | prg_rom_data,
                                ProgramBankMode::Combined => self.prg_combined = (self.prg_combined & 0b01111) | prg_rom_data,
                                FixFirst => {}
                            }
                        } else {
                            // Second CHR Bank
                            match &mut self.chr_bank_mode {
                                Separated => self.chr_second = chr_data,
                                CharacterBankMode::Combined => {}
                            }

                            match &mut self.prg_bank_mode {
                                FixFirst => self.prg_second = (self.prg_second & 0b01111) | prg_rom_data,
                                _ => {}
                            }
                        }

                        self.prg_ram_bank = prg_ram_data;
                    }
                    3 => {
                        match &mut self.prg_bank_mode {
                            FixFirst => {
                                self.prg_first = 0;
                                self.prg_second = (self.prg_second & 0x10 ) | (value & 0b01111)
                            },
                            FixSecond => {
                                self.prg_first = (self.prg_first & 0x10) | (value & 0b01111);
                                self.prg_second = self.prg_banks - 1;
                            },
                            ProgramBankMode::Combined => self.prg_combined = (self.prg_combined & 0x10) | (value & 0b01110),
                        }
                    }
                    _ => unreachable!()
                }
            }
        }
    }

    fn reset(&mut self) {
        self.shift_register.clear();
        self.prg_bank_mode = FixSecond;
        self.chr_bank_mode = Separated;
        self.custom_mirroring = SingleScreenA;
        self.prg_ram_bank = 0;
        self.chr_first = 0;
        self.chr_second = 0;
        self.chr_combined = 0;
        self.prg_first = 0;
        self.prg_second = self.prg_banks - 1;
        self.prg_combined = 0;
    }

    fn clone_box(&self) -> Box<dyn Mapper> {
        Box::new(self.clone())
    }
}

#[derive(Copy, Clone)]
struct ShiftRegister {
    pub value: u8,
    pub count: u8
}

impl ShiftRegister {
    fn new() -> Self {
        Self {
            value: 0,
            count: 0,
        }
    }

    fn push(&mut self, lsb: bool) {
        self.value >>= 1;
        if lsb {
            self.value |= 0b10000;
        }

        if self.count < 5 {
            self.count += 1;
        };
    }

    pub fn clear(&mut self) -> u8 {
        let copy = self.value;
        self.value = 0;
        self.count = 0;
        copy
    }
}
