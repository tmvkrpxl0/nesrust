use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::fs;
use std::ops::{BitAnd, BitOr, Shl, Shr};
use std::path::Path;
use strum_macros::Display;

use crate::system::{Sharable, Timing, VsType};
use crate::system::devices::bus::expansions::ExpansionDeviceKinds;
use crate::system::devices::bus::expansions::ExpansionDeviceKinds::*;
use crate::system::devices::bus::ppu::PPUType;
use crate::system::devices::bus::ppu::PPUType::*;
use crate::system::devices::game_cartridge::ConsoleType::{
    FamicloneDecimal, FamicomNetworkSystem, NesFcDendy, NesFcEPSM, PlayChoice, UMC6578, VS, VT01,
    VT02, VT03, VT09, VT32, VT369,
};
use crate::system::devices::game_cartridge::mappers::{create_mapper, Mapper};
use crate::system::devices::game_cartridge::nametable::NameTable;
use crate::system::devices::game_cartridge::NameTableMirroring::{
    FourScreen, Horizontal, Vertical,
};
use crate::system::Timing::{Dendy, MultiRegion, NTSC, PAL};
use crate::system::VsType::*;
use crate::utils::msb;

pub mod mappers;
pub mod nametable;

pub struct Cartridge {
    pub mapper: Sharable<Box<dyn Mapper>>,
    pub made_for: ConsoleType,
    pub program: Vec<u8>,
    pub character: Vec<u8>,
    pub program_banks: u16,
    pub character_banks: u16,
    pub program_ram: Vec<u8>,
    pub character_ram: Vec<u8>,
    pub trainer: Option<Vec<u8>>,
    pub timing: Timing,
    pub needs: ExpansionDeviceKinds,
    pub hard_mirroring: NameTableMirroring,
    pub nametable: NameTable,
    pub misc: Vec<u8>,
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum ConsoleType {
    NesFcDendy {
        timing: Timing,
    },
    VS {
        vs_type: VsType,
        ppu_type: PPUType,
    },
    PlayChoice,
    FamicloneDecimal,
    NesFcEPSM,
    VT01,
    VT02,
    VT03,
    VT09,
    VT32,
    VT369,
    UMC6578,
    FamicomNetworkSystem,
    Unknown,
}

impl ConsoleType {
    pub fn ppu_type(&self) -> PPUType {
        match &self {
            NesFcDendy { timing } => {
                match timing {
                    NTSC => RP2C02,
                    PAL => RP2C07,
                    MultiRegion => RP2C02,
                    Dendy => unimplemented!()
                }
            }
            VS {
                vs_type: _vs_type,
                ppu_type,
            } => *ppu_type,
            _ => RP2C02,
        }
    }
}

#[derive(Copy, Clone, Display)]
pub enum NameTableMirroring {
    Horizontal,
    Vertical,
    SingleScreenA,
    SingleScreenB,
    FourScreen,
}

struct UnknownRomError {
    signature: String,
}

impl Debug for UnknownRomError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.signature.as_str())
    }
}

impl Display for UnknownRomError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.signature.as_str())
    }
}

impl Error for UnknownRomError {}

impl Cartridge {
    pub fn on_read(&mut self, address: u16) -> Option<u8> {
        let jailbreak = unsafe { (self as *mut Cartridge).as_mut().unwrap() };
        self.mapper.borrow_mut().read(address, jailbreak)
    }

    pub fn on_write(&mut self, address: u16, data: u8) {
        let jailbreak = unsafe { (self as *mut Cartridge).as_mut().unwrap() };
        self.mapper.borrow_mut().write(address, data, jailbreak);
    }

    pub fn reset(&mut self) {
        self.mapper.borrow_mut().reset();
    }

    pub fn load_from_file<P: AsRef<Path>>(path: P) -> Result<Cartridge, Box<dyn Error>> {
        let contents = fs::read(path)?;
        let mut iterator = contents.into_iter();

        let identifier = [
            iterator.next().unwrap() as char,
            iterator.next().unwrap() as char,
            iterator.next().unwrap() as char,
            iterator.next().unwrap() as char,
        ];

        if identifier != ['N', 'E', 'S', 0x1A as char] {
            return Err(Box::new(UnknownRomError {
                signature: String::from_iter(identifier),
            }));
        }

        let prg_rom_size = iterator.next().unwrap();
        let chr_rom_size = iterator.next().unwrap();
        let flag6 = iterator.next().unwrap();
        let flag7 = iterator.next().unwrap();

        let has_trainer = flag6.bitand(0b00000100) != 0;
        let revision = flag7.bitand(0b00001100).shr(2) as i32;

        if revision == 0b10 {
            let header = NES2Header {
                identifier,
                prg_rom_banks_lsb: prg_rom_size,
                chr_rom_banks_lsb: chr_rom_size,
                flag6,
                flag7,
                mapper_msb: iterator.next().unwrap(),
                rom_msb: iterator.next().unwrap(),
                program_ram_shift_counts: iterator.next().unwrap(),
                chr_ram_shift_counts: iterator.next().unwrap(),
                timing: iterator.next().unwrap(),
                type_info: iterator.next().unwrap(),
                misc_roms: iterator.next().unwrap(),
                default_expansion_device: iterator.next().unwrap(),
            };
            let trainer = if has_trainer {
                Some(iterator.by_ref().take(512).collect::<Vec<u8>>())
            } else {
                None
            };
            let program = iterator
                .by_ref()
                .take(header.program_rom_size() as usize)
                .collect::<Vec<u8>>();
            let character = iterator
                .by_ref()
                .take(header.character_rom_size() as usize)
                .collect::<Vec<u8>>();
            let program_banks = header.program_rom_banks();
            let character_banks = header.character_rom_banks();
            let mapper_id = header.mapper_id();
            let program_ram = vec![0u8; prg_ram_for_mapper(mapper_id).unwrap_or(header.program_ram_size())];
            let character_ram = vec![0u8; header.character_ram_size() as usize];
            let misc = iterator.collect::<Vec<u8>>();
            let made_for = header.console_type();
            let timing = header.timing();
            let needs = header.default_expansion_device();
            let sub_id = header.submapper_id();
            let hard_mirroring = header.mirroring_mode();
            let nametable = NameTable::new();
            let mapper = Sharable::new(create_mapper(
                mapper_id,
                sub_id,
                program_banks,
                character_banks,
                &made_for,
                &timing,
                &needs,
            )?);

            Ok(Cartridge {
                trainer,
                program,
                character,
                mapper,
                program_banks,
                character_banks,
                program_ram,
                made_for,
                timing,
                needs,
                hard_mirroring,
                nametable,
                character_ram,
                misc,
            })
        } else if revision == 0 {
            let header = INESHeader {
                identifier,
                prg_rom_banks: prg_rom_size,
                chr_rom_banks: chr_rom_size,
                flag6,
                flag7,
                prg_ram_size: iterator.next().unwrap(),
                system: iterator.next().unwrap(),
                system_ram_presence: iterator.next().unwrap(),
                extra: [
                    iterator.next().unwrap(),
                    iterator.next().unwrap(),
                    iterator.next().unwrap(),
                    iterator.next().unwrap(),
                    iterator.next().unwrap(),
                ],
            };
            let trainer = if has_trainer {
                Some(iterator.by_ref().take(512).collect::<Vec<u8>>())
            } else {
                None
            };
            let program = iterator
                .by_ref()
                .take(header.program_rom_size() as usize)
                .collect::<Vec<u8>>();
            let character = iterator
                .by_ref()
                .take(header.character_rom_size() as usize)
                .collect::<Vec<u8>>();
            let program_banks = header.program_rom_banks() as u16;
            let character_banks = header.character_rom_banks() as u16;
            let mapper_id = header.mapper_id() as u16;
            let program_ram = vec![0u8; prg_ram_for_mapper(mapper_id).unwrap_or(header.program_ram_size())];
            let character_ram = if character_banks == 0 { vec![0u8; 8192] } else { vec![] };
            let misc = iterator.collect::<Vec<u8>>();
            let timing = header.tv_system();
            let made_for = NesFcDendy {
                timing,
            };
            let hard_mirroring = header.mirroring_mode();
            let nametable = NameTable::new();
            let mapper = Sharable::new(create_mapper(
                mapper_id,
                0,
                program_banks,
                character_banks,
                &made_for,
                &timing,
                &Unspecified,
            )?);

            Ok(Cartridge {
                trainer,
                program,
                character,
                mapper,
                program_banks,
                character_banks,
                program_ram,
                character_ram,
                made_for,
                timing: header.tv_system(),
                needs: StandardNesControllers,
                hard_mirroring,
                nametable,
                misc,
            })
        } else {
            Err(Box::try_from(UnknownRomError {
                signature: "Unknown revision number!".to_string(),
            })
                .unwrap())
        }
    }

    pub fn take_snapshot(&self) -> Self {
        Self {
            mapper: Sharable::new(self.mapper.borrow().clone_box()),
            made_for: self.made_for,
            program: self.program.clone(),
            character: self.character.clone(),
            program_banks: self.program_banks,
            character_banks: self.character_banks,
            program_ram: self.program_ram.clone(),
            character_ram: self.character_ram.clone(),
            trainer: self.trainer.clone(),
            timing: self.timing.clone(),
            needs: self.needs.clone(),
            hard_mirroring: self.hard_mirroring,
            nametable: self.nametable.clone(),
            misc: self.misc.clone(),
        }
    }
    pub fn take_irq(&self) -> bool {
        self.mapper.borrow_mut().take_irq()
    }
}

struct INESHeader {
    identifier: [char; 4],
    prg_rom_banks: u8,
    chr_rom_banks: u8,
    flag6: u8,
    flag7: u8,
    prg_ram_size: u8,
    system: u8,
    system_ram_presence: u8,
    extra: [u8; 5],
}

struct NES2Header {
    identifier: [char; 4],
    prg_rom_banks_lsb: u8,
    chr_rom_banks_lsb: u8,
    flag6: u8,
    flag7: u8,
    mapper_msb: u8,
    rom_msb: u8,
    program_ram_shift_counts: u8,
    chr_ram_shift_counts: u8,
    timing: u8,
    type_info: u8,
    misc_roms: u8,
    default_expansion_device: u8,
}

#[allow(dead_code)]
impl INESHeader {
    pub fn identifier(&self) -> [char; 4] {
        self.identifier
    }

    pub fn should_mirror_vertically(&self) -> bool {
        self.flag6 & 0b00000001 != 0
    }

    pub fn has_battery(&self) -> bool {
        self.flag6 & 0b00000010 != 0
    }

    pub fn has_trainer(&self) -> bool {
        self.flag6 & 0b00000100 != 0
    }

    pub fn is_four_screen(&self) -> bool {
        self.flag6 & 0b00001000 != 0
    }

    pub fn mapper_id(&self) -> u8 {
        let low = (self.flag6 & 0b11110000) >> 4; // 3210
        let high = self.flag7 & 0b11110000; // 7654
        let combined = low | high;
        combined
    }

    pub fn program_rom_banks(&self) -> u8 {
        self.prg_rom_banks
    }

    pub fn program_rom_size(&self) -> u32 {
        16384 * self.prg_rom_banks as u32
    }

    pub fn character_rom_banks(&self) -> u8 {
        self.chr_rom_banks
    }

    pub fn character_rom_size(&self) -> u32 {
        8192u32 * self.character_rom_banks() as u32
    }

    pub fn program_ram_size(&self) -> usize {
        if self.system_ram_presence.bitand(0b01000000) == 0 {
            return 0;
        }

        if self.prg_ram_size == 0 {
            8192
        } else {
            8192 * self.prg_ram_size as usize
        }
    }

    pub fn tv_system(&self) -> Timing {
        let value = self.system.bitand(0b00000011);
        if value % 2 == 0 {
            MultiRegion
        } else if value == 0 {
            NTSC
        } else if value == 2 {
            PAL
        } else {
            unreachable!()
        }
    }

    pub fn has_conflicts(&self) -> bool {
        msb(self.system_ram_presence)
    }

    pub fn mirroring_mode(&self) -> NameTableMirroring {
        if self.is_four_screen() {
            FourScreen
        } else if self.should_mirror_vertically() {
            Vertical
        } else {
            Horizontal
        }
    }
}

#[allow(dead_code)]
impl NES2Header {
    pub fn identifier(&self) -> [char; 4] {
        self.identifier
    }

    pub fn should_mirror_vertically(&self) -> bool {
        self.flag6 & 0b00000001 != 0
    }

    pub fn has_battery(&self) -> bool {
        self.flag6 & 0b00000010 != 0
    }

    pub fn has_trainer(&self) -> bool {
        self.flag6 & 0b00000100 != 0
    }

    pub fn is_four_screen(&self) -> bool {
        self.flag6 & 0b00001000 != 0
    }

    pub fn mirroring_mode(&self) -> NameTableMirroring {
        if self.is_four_screen() {
            FourScreen
        } else if self.should_mirror_vertically() {
            Vertical
        } else {
            Horizontal
        }
    }

    pub fn mapper_id(&self) -> u16 {
        let low = ((self.flag6 & 0b11110000) as u16) >> 4; // 3210
        let middle = (self.flag7 & 0b11110000) as u16; // 7654
        let high = ((self.mapper_msb & 0b00001111) as u16) << 4; // 1089
        let combined = low | middle | high;
        combined
    }

    pub fn submapper_id(&self) -> u8 {
        self.mapper_msb.bitand(0b11110000).shr(4)
    }

    pub fn program_rom_banks(&self) -> u16 {
        let low = self.prg_rom_banks_lsb as u16;
        let high = (self.rom_msb.bitand(0b00001111) as u16).shl(8) as u16;
        high.bitor(low)
    }

    pub fn program_rom_size(&self) -> u32 {
        let low = self.prg_rom_banks_lsb as u16;
        let high = (self.rom_msb.bitand(0b00001111) as u16).shl(8) as u16;
        16384u32 * high.bitor(low) as u32
    }

    pub fn character_rom_banks(&self) -> u16 {
        let low = self.chr_rom_banks_lsb as u16;
        let high = (self.rom_msb.bitand(0b11110000) as u16).shl(4);
        let combined = high | low;
        combined
    }

    pub fn character_rom_size(&self) -> u32 {
        8192u32 * self.character_rom_banks() as u32
    }

    pub fn program_ram_size(&self) -> usize {
        let shift_count = self.program_ram_shift_counts.bitand(0b00001111);
        if shift_count == 0 {
            0
        } else {
            64usize << shift_count
        }
    }

    pub fn program_nvram_size(&self) -> u32 {
        let shift_count = self.program_ram_shift_counts.bitand(0b11110000).shr(4);
        if shift_count == 0u8 {
            0
        } else {
            64u32 << shift_count
        }
    }

    pub fn character_ram_size(&self) -> u32 {
        let shift_count = self.chr_ram_shift_counts.bitand(0b00001111);
        if shift_count == 0u8 {
            0
        } else {
            64 << shift_count
        }
    }

    pub fn character_nvram_size(&self) -> u32 {
        let shift_count = self.chr_ram_shift_counts.bitand(0b11110000).shr(4);
        if shift_count == 0u8 {
            0
        } else {
            64u32 << shift_count
        }
    }

    pub fn timing(&self) -> Timing {
        let value = self.timing.bitand(0b00000011);
        match value {
            0 => NTSC,
            1 => PAL,
            2 => MultiRegion,
            3 => Dendy,
            _ => unreachable!(),
        }
    }

    pub fn misc_roms(&self) -> u8 {
        self.misc_roms.bitand(0b00000011)
    }

    pub fn default_expansion_device(&self) -> ExpansionDeviceKinds {
        match self.default_expansion_device {
            0 | 1 => StandardNesControllers,
            2 => NesFourScoreControllers,
            3 => Famicom4PAdapter2ControllersSimple,
            4 => Vs1P4016,
            5 => Vs1P4017,
            6 => Reserved,
            7 => VsZapper,
            8 => Zapper4017,
            9 => TwoZappers,
            10 => BandaiLightgun,
            11 => PowerPadA,
            12 => PowerPadB,
            13 => FamilyTrainerA,
            14 => FamilyTrainerB,
            15 => VausNesController,
            16 => VausFamicomeController,
            17 => TwoVausControllersWithFamicomDataRecorder,
            18 => KonamiHyperShotController,
            19 => CoconutsPachinkoController,
            20 => BoxingPunchingBag,
            21 => JissenMahjongController,
            22 => PartyTap,
            23 => KidsTablet,
            24 => BarcodeBattler,
            25 => PianoKeyboard,
            26 => WhackAMoleMatAndMallet,
            27 => InflatableBicycle,
            28 => DoubleFisted,
            29 => Famicom3DSystem,
            30 => DoremikkoKeyboard,
            31 => RobGyroSet,
            32 => FamicomDataRecorder,
            33 => TurboFile,
            34 => IGSStorageBattleBox,
            35 => FamilyBASICKeyboardWithFamicomDataRecorder,
            36 => Dongda586Keyboard,
            37 => BitCorp79Keyboard,
            38 => SuborKeyboard,
            39 => SuborKeyboardWithMouse3x8,
            40 => SuborKeyboardWithMouse24,
            41 => SNESMouse4017,
            42 => Multicart,
            43 => TwoSNESControllers,
            44 => RacerMateBicycle,
            45 => UForce,
            46 => RobStackUp,
            47 => CityPatrolmanLightgun,
            48 => SharpC1CassetteInterface,
            49 => StandardControllerSwappedLayout,
            50 => ExcaliborSudokuPad,
            51 => AblPinball,
            52 => GoldenNuggetCasinoButtons,
            _ => Unspecified,
        }
    }

    pub fn console_type(&self) -> ConsoleType {
        let value = self.flag7.bitand(0b11);
        match value {
            0 => NesFcDendy {
                timing: self.timing(),
            },
            1 => {
                let vs_type = match self.type_info.bitand(0b11110000).shr(4u8) {
                    0 => UniSystemNormal,
                    1 => UniSystemRbi,
                    2 => UniSystemTko,
                    3 => UniSystemSuperX,
                    4 => UniSystemIceClimber,
                    5 => DualSystemNormal,
                    6 => DualSystemRaid,
                    _ => unreachable!(),
                };
                let ppu_type = match self.type_info.bitand(0b00001111) {
                    0 => RP2C03B,
                    1 => RP2C03G,
                    2 => RP2C04_0001,
                    3 => RP2C04_0002,
                    4 => RP2C04_0003,
                    5 => RP2C04_0004,
                    6 => RC2C03B,
                    7 => RC2C03C,
                    8 => RC2C05_01,
                    9 => RC2C05_02,
                    10 => RC2C05_03,
                    11 => RC2C05_04,
                    12 => RC2C05_05,
                    13..=15 => Unknown,
                    _ => unreachable!(),
                };
                VS { vs_type, ppu_type }
            }
            2 => PlayChoice,
            3 => {
                let extended = self.type_info.bitand(0b00001111);
                match extended {
                    3 => FamicloneDecimal,
                    4 => NesFcEPSM,
                    5 => VT01,
                    6 => VT02,
                    7 => VT03,
                    8 => VT09,
                    9 => VT32,
                    10 => VT369,
                    11 => UMC6578,
                    12 => FamicomNetworkSystem,
                    13..=15 => ConsoleType::Unknown,
                    _ => unreachable!(),
                }
            }
            _ => unreachable!("?"),
        }
    }
}

fn prg_ram_for_mapper(id: u16) -> Option<usize> {
    match id {
        0 => Some(8192),
        1 => Some(32768),
        4 => Some(8192),
        _ => None
    }
}
