use crate::system::devices::bus::Bus;

pub mod bus;
pub mod cpu;
pub mod game_cartridge;

pub trait BusDevice<T: Bus> {
    const MIN_ADDRESS: u16;
    const MAX_ADDRESS: u16;
    fn on_read(&mut self, bus: &T, address: u16) -> Option<u8>;
    fn on_write(&mut self, bus: &T, address: u16, data: u8);
}
