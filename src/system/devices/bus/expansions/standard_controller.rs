use std::any::Any;
use bitfield_struct::bitfield;
use crate::system::devices::bus::expansions::{ExpansionDevice, ExpansionDeviceKinds};
use crate::system::devices::bus::SystemBus;

use crate::utils::msb;

#[derive(Clone)]
pub struct StandardNesControllers {
    pub keyboard_input: [KeyboardInput; 2],
    cached: [u8; 2],
}

#[bitfield(u8)]
pub struct KeyboardInput {
    pub right: bool,
    pub left: bool,
    pub down: bool,
    pub up: bool,
    pub s: bool,
    pub a: bool,
    pub z: bool,
    pub x: bool,
}

impl ExpansionDevice for StandardNesControllers {
    fn kind(&self) -> ExpansionDeviceKinds {
        ExpansionDeviceKinds::StandardNesControllers
    }

    fn on_read(&mut self, _: &SystemBus, address: u16) -> Option<u8> {
        Some(self.shift(address % 2 != 0) as u8)
    }

    fn on_write(&mut self, _: &SystemBus, address: u16, _: u8) {
        self.store(address % 2 != 0)
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }

    fn clone_box(&self) -> Box<dyn ExpansionDevice> {
        Box::new(self.clone())
    }
}

impl StandardNesControllers {
    pub fn new() -> Self {
        StandardNesControllers {
            keyboard_input: [KeyboardInput(0), KeyboardInput(0)],
            cached: [0, 0],
        }
    }

    pub fn store(&mut self, is_second: bool) {
        if !is_second {
            self.cached[0] = self.keyboard_input[0].0;
        } else {
            self.cached[1] = self.keyboard_input[1].0;
        }
    }

    pub fn shift(&mut self, is_second: bool) -> bool {
        let cached = if !is_second { &mut self.cached[0] } else { &mut self.cached[1] };
        let msb = msb(*cached);

        *cached <<= 1;

        msb
    }
}
