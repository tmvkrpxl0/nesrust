use std::any::Any;
use crate::system::devices::bus::expansions::{ExpansionDevice, ExpansionDeviceKinds};
use crate::system::devices::bus::expansions::zapper::TriggerState::*;
use crate::system::devices::bus::SystemBus;

#[derive(Clone)]
pub struct Zapper {
    pub trigger: TriggerState,
    pub is_pulling: bool,
    pub aiming: (u8, u8),
    scanline: u8
}

impl Zapper {
    pub fn new() -> Self {
        Self {
            trigger: Released,
            is_pulling: false,
            aiming: (0, 0),
            scanline: 0,
        }
    }
    pub fn on_frame(&mut self) {
        match &mut self.trigger {
            Released => {
                if self.is_pulling {
                    self.trigger = Half(5)
                }
            }
            Half(timer) => {
                if *timer > 0 {
                    *timer -= 1;
                    if *timer == 0 {
                        self.trigger = Pulled
                    }
                } else if *timer < 0 {
                    *timer += 1;
                    if *timer == 0 {
                        self.trigger = Released
                    }
                }
            }
            Pulled => {
                if !self.is_pulling {
                    self.trigger = Half(-5)
                }
            }
        }
    }

    pub fn scanline(&mut self, scanline: u8) {
        self.scanline = scanline;
    }
}

impl ExpansionDevice for Zapper {
    fn kind(&self) -> ExpansionDeviceKinds {
        ExpansionDeviceKinds::Zapper4017
    }

    fn on_read(&mut self, _: &SystemBus, _: u16) -> Option<u8> {
        let is_detected = self.scanline == self.aiming.1;
        let is_half = matches!(self.trigger, Half(_));
        println!("{} vs {} ({})", self.scanline, self.aiming.1, self.is_pulling);

        let mut data = 0u8;
        if !is_detected {
            data |= 0b00001000;
        } else if self.is_pulling {
            println!("ZAM!")
        };
        if is_half {
            data |= 0b00010000;
        };

        Some(data)
    }

    fn on_write(&mut self, _: &SystemBus, _: u16, _: u8) {}

    fn as_any(&self) -> &dyn Any { self }

    fn as_any_mut(&mut self) -> &mut dyn Any { self }

    fn clone_box(&self) -> Box<dyn ExpansionDevice> {
        Box::new(self.clone())
    }
}

#[derive(Clone)]
pub enum TriggerState {
    Released,
    Half(i8),
    Pulled
}