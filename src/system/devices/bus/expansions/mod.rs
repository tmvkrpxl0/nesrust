use std::any::Any;
use strum_macros::{Display, EnumIter};
use crate::system::devices::bus::expansions::standard_controller::StandardNesControllers;
use crate::system::devices::bus::expansions::zapper::Zapper;

use crate::system::devices::bus::SystemBus;
use crate::system::devices::BusDevice;

pub mod standard_controller;
pub mod zapper;

#[derive(Display, Copy, Clone, EnumIter, Eq, PartialEq)]
pub enum ExpansionDeviceKinds {
    Unspecified,
    StandardNesControllers,
    NesFourScoreControllers,
    Famicom4PAdapter2ControllersSimple,
    Vs1P4016,
    Vs1P4017,
    Reserved,
    VsZapper,
    Zapper4017,
    TwoZappers,
    BandaiLightgun,
    PowerPadA,
    PowerPadB,
    FamilyTrainerA,
    FamilyTrainerB,
    VausNesController,
    VausFamicomeController,
    TwoVausControllersWithFamicomDataRecorder,
    KonamiHyperShotController,
    CoconutsPachinkoController,
    BoxingPunchingBag,
    JissenMahjongController,
    PartyTap,
    KidsTablet,
    BarcodeBattler,
    PianoKeyboard,
    WhackAMoleMatAndMallet,
    InflatableBicycle,
    DoubleFisted,
    Famicom3DSystem,
    DoremikkoKeyboard,
    RobGyroSet,
    FamicomDataRecorder,
    TurboFile,
    IGSStorageBattleBox,
    FamilyBASICKeyboardWithFamicomDataRecorder,
    Dongda586Keyboard,
    BitCorp79Keyboard,
    SuborKeyboard,
    SuborKeyboardWithMouse3x8,
    SuborKeyboardWithMouse24,
    SNESMouse4017,
    Multicart,
    TwoSNESControllers,
    RacerMateBicycle,
    UForce,
    RobStackUp,
    CityPatrolmanLightgun,
    SharpC1CassetteInterface,
    StandardControllerSwappedLayout,
    ExcaliborSudokuPad,
    AblPinball,
    GoldenNuggetCasinoButtons,
}

impl ExpansionDeviceKinds {
    pub fn is_supported(&self) -> bool {
        match self {
            ExpansionDeviceKinds::StandardNesControllers { .. } |
            ExpansionDeviceKinds::Zapper4017 => true,
            _ => false,
        }
    }

    pub fn create(&self, is_1p: bool) -> Box<dyn ExpansionDevice> {
        match self {
            ExpansionDeviceKinds::Unspecified => Box::new(NotConnected {}),
            ExpansionDeviceKinds::StandardNesControllers => Box::new(StandardNesControllers::new()),
            ExpansionDeviceKinds::Zapper4017 if !is_1p => Box::new(Zapper::new()),
            _ => unimplemented!("{} is unsupported!", self)
        }
    }
}

pub trait ExpansionDevice {
    fn kind(&self) -> ExpansionDeviceKinds;

    fn on_read(&mut self, bus: &SystemBus, address: u16) -> Option<u8>;
    fn on_write(&mut self, bus: &SystemBus, address: u16, data: u8);

    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
    fn clone_box(&self) -> Box<dyn ExpansionDevice>;
}

impl<T: ExpansionDevice> BusDevice<SystemBus> for T {
    const MIN_ADDRESS: u16 = 0x4016;
    const MAX_ADDRESS: u16 = 0x4017;

    fn on_read(&mut self, bus: &SystemBus, address: u16) -> Option<u8> {
        self.on_read(bus, address)
    }

    fn on_write(&mut self, bus: &SystemBus, address: u16, data: u8) {
        self.on_write(bus, address, data)
    }
}

#[derive(Clone)]
struct NotConnected {}

impl ExpansionDevice for NotConnected {
    fn kind(&self) -> ExpansionDeviceKinds {
        ExpansionDeviceKinds::Unspecified
    }

    fn on_read(&mut self, _: &SystemBus, _: u16) -> Option<u8> { None }

    fn on_write(&mut self, _: &SystemBus, _: u16, _: u8) {}

    fn as_any(&self) -> &dyn Any { self }

    fn as_any_mut(&mut self) -> &mut dyn Any { self }

    fn clone_box(&self) -> Box<dyn ExpansionDevice> {
        Box::new(self.clone())
    }
}