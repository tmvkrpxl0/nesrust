use crate::system::devices::bus::ppu::PPU;

pub trait DevPPU {
    fn is_drawing(&self) -> bool;
    fn scanline(&self) -> i16;
}

impl DevPPU for PPU {
    fn is_drawing(&self) -> bool {
        self.is_drawing
    }

    fn scanline(&self) -> i16 {
        self.scanline
    }
}
