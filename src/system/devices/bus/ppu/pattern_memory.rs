use crate::system::devices::bus::cartridge_connector::CartridgeConnector;
use crate::system::devices::bus::memory::Memory;
use crate::system::devices::bus::PPUBus;
use crate::system::devices::BusDevice;
use crate::system::Wire;

enum PatternContents {
    Builtin(Memory<0x0000, 0x1FFF>),
    Cartridge(Wire<CartridgeConnector>),
}

pub struct PatternMemory {
    contents: PatternContents,
    pub has_changed: bool,
}

impl PatternMemory {
    pub fn builtin() -> Self {
        Self {
            contents: PatternContents::Builtin(Memory::new("Built-in Pattern Memory", 1)),
            has_changed: true,
        }
    }

    pub fn cartridge(cartridge_port: Wire<CartridgeConnector>) -> Self {
        Self {
            contents: PatternContents::Cartridge(cartridge_port),
            has_changed: true,
        }
    }

    pub fn needs_update(&mut self) -> bool {
        let to_return = self.has_changed;
        self.has_changed = false;
        to_return
    }

    pub fn take_snapshot(&self, cartridge: Wire<CartridgeConnector>) -> Self {
        let contents = match &self.contents {
            PatternContents::Builtin(memory) => PatternContents::Builtin(memory.clone()),
            PatternContents::Cartridge(_) => PatternContents::Cartridge(cartridge)
        };
        Self {
            contents,
            has_changed: true, // maybe?
        }
    }
}

impl ToString for PatternMemory {
    fn to_string(&self) -> String {
        "Pattern Memory".to_string()
    }
}

impl BusDevice<PPUBus> for PatternMemory {
    const MIN_ADDRESS: u16 = 0;
    const MAX_ADDRESS: u16 = 0x1FFF;

    fn on_read(&mut self, bus: &PPUBus, address: u16) -> Option<u8> {
        match &mut self.contents {
            PatternContents::Builtin(memory) => memory.on_read(bus, address),
            PatternContents::Cartridge(port) => port.borrow_mut().on_read(bus, address)
        }
    }

    fn on_write(&mut self, bus: &PPUBus, address: u16, data: u8) {
        self.has_changed = true;
        match &mut self.contents {
            PatternContents::Builtin(memory) => memory.on_write(bus, address, data),
            PatternContents::Cartridge(port) => port.borrow_mut().on_write(bus, address, data)
        }
    }
}
