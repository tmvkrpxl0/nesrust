use std::fs::{File, remove_file};
use std::io::Write;
use std::mem::transmute;
use std::ops::Shl;
use std::rc::Rc;

use bitfield_struct::bitfield;
use bytemuck::{Pod, Zeroable};
use strum_macros::Display;

use crate::system::{Timing, Wire};
use crate::system::devices::bus::{Bus, PPUBus, SystemBus};
use crate::system::devices::BusDevice;
use crate::system::devices::cpu::CPU6502;
use crate::system::devices::game_cartridge::mappers::Mapper;
use crate::utils::{BitShift, Flip, msb};

pub mod debug;
pub mod palette_memory;
pub mod pattern_memory;

pub static PALETTE_2C02: [(u8, u8, u8); 0x40] = unsafe { transmute(*include_bytes!("2C02.pal")) };

static LOG_PPU: bool = false;
static LOG_COLOR: bool = false;

#[bitfield(u8)]
pub struct StatusRegister {
    #[bits(5)]
    unused: u8,
    sprite_overflow: bool,
    sprite_zero_hit: bool,
    vertical_blank: bool,
}

#[bitfield(u8)]
pub struct MaskRegister {
    grayscale: bool,
    render_background_left: bool,
    render_sprites_left: bool,
    render_background: bool,
    render_sprites: bool,
    enhance_red: bool,
    enhance_green: bool,
    enhance_blue: bool,
}

#[bitfield(u8)]
pub struct ControlRegister {
    nametable_x: bool,
    nametable_y: bool,
    increment_mode: bool,
    pattern_slice: bool,
    pattern_background: bool,
    sprite_size: bool,
    slave_mod: bool,
    enable_nmi: bool,
}

#[bitfield(u16)]
pub struct LoopyImpl {
    #[bits(5)]
    coarse_x: u8,
    #[bits(5)]
    coarse_y: u8,
    nametable_x: bool,
    nametable_y: bool,
    #[bits(3)]
    fine_y: u8,
    unused: bool,
}

#[derive(Pod, Copy, Clone, Zeroable)]
#[repr(C)]
pub struct Object {
    y: u8,
    id: u8,
    attribute: u8,
    x: u8
}

impl ToString for Object {
    fn to_string(&self) -> String {
        format!("({:02X},{:02X}) ID: {:02X} AT: {:02X}", self.x, self.y, self.id, self.attribute)
    }
}

pub struct PPU {
    pub ppu_type: PPUType,
    ppu_bus: Rc<PPUBus>,
    cycles: i16,
    scanline: i16,
    odd_frame: bool,
    is_drawing: bool,
    status_register: StatusRegister,
    mask_register: MaskRegister,
    control_register: ControlRegister,
    ppu_data_buffer: u8,
    vloopy: LoopyImpl,
    tloopy: LoopyImpl,
    address_latch: bool, // true = written to high 8 bits before
    fine_x: u8,

    pub age: u32,
    log_file: File,
    colordump: File,

    bg_next: NextBgPixels,
    bg_cache: BgPixelCache,
    cpu: Wire<CPU6502>,
    oam: [Object; 64],
    oam_address: u8,

    to_draw: [Object; 8], // objects to draw this line
    object_count: u8,
    sprite_cache: [SpritePixelCache; 8],

    sprite_zero_possible: bool,
    is_drawing_sprite_zero: bool,

    side_channel: Option<Wire<Box<dyn Mapper>>>
}

#[derive(Copy, Clone)]
struct SpritePixelCache {
    low: u8,
    high: u8
}

#[derive(Copy, Clone)]
struct NextBgPixels {
    id: u8,
    palette: u8,
    color_lsb: u8,
    color_msb: u8,
}

#[derive(Copy, Clone)]
struct BgPixelCache {
    pixel_low: u16,
    pixel_high: u16,
    palette_low: u16,
    palette_high: u16,
}

#[derive(Copy, Clone, Eq, PartialEq, Display)]
pub enum PPUType {
    RP2C02,
    RP2C03B,
    RP2C03G,
    RP2C04_0001,
    RP2C04_0002,
    RP2C04_0003,
    RP2C04_0004,
    RP2C07,
    RC2C03B,
    RC2C03C,
    RC2C05_01,
    RC2C05_02,
    RC2C05_03,
    RC2C05_04,
    RC2C05_05,
    Unknown,
}

impl PPUType {
    pub fn timing(&self) -> Timing {
        match &self {
            PPUType::RP2C02
            | PPUType::RP2C03B
            | PPUType::RP2C03G
            | PPUType::RP2C04_0001
            | PPUType::RP2C04_0002
            | PPUType::RP2C04_0003
            | PPUType::RP2C04_0004 => Timing::NTSC,

            PPUType::RP2C07
            | PPUType::RC2C03B
            | PPUType::RC2C03C
            | PPUType::RC2C05_01
            | PPUType::RC2C05_02
            | PPUType::RC2C05_03
            | PPUType::RC2C05_04
            | PPUType::RC2C05_05 => Timing::PAL,
            PPUType::Unknown => Timing::MultiRegion,
        }
    }
}

pub struct PPUOutput {
    pub screen_pos: (u8, u8),
    pub color: (u8, u8, u8),
}

impl PPU {
    pub fn new(
        ppu_type: PPUType,
        ppu_bus: Rc<PPUBus>,
        cpu: Wire<CPU6502>,
    ) -> PPU {
        let _ = remove_file("ppu.txt");
        Self {
            ppu_type,
            ppu_bus,
            cycles: 0,
            scanline: 0,
            odd_frame: false,
            is_drawing: true,
            status_register: StatusRegister::new(),
            mask_register: MaskRegister::new(),
            control_register: ControlRegister::new(),
            ppu_data_buffer: 0,
            vloopy: LoopyImpl::new(),
            tloopy: LoopyImpl::new(),
            address_latch: false,
            fine_x: 0,
            age: 0,
            log_file: File::options()
            .create(true)
            .read(true)
            .write(true)
            .append(true)
            .open("ppu.txt")
            .unwrap(),
            colordump: File::options()
                .create(true)
                .read(true)
                .write(true)
                .append(true)
                .open("colordump.txt")
                .unwrap(),
            bg_next: NextBgPixels {
                id: 0,
                palette: 0,
                color_lsb: 0,
                color_msb: 0,
            },
            bg_cache: BgPixelCache {
                pixel_low: 0,
                pixel_high: 0,
                palette_low: 0,
                palette_high: 0,
            },
            cpu,
            oam: [Object::zeroed(); 64],
            oam_address: 0,
            to_draw: [Object::zeroed(); 8],
            object_count: 0,
            sprite_cache: [SpritePixelCache { low: 0, high: 0 }; 8],
            sprite_zero_possible: false,
            is_drawing_sprite_zero: false,
            side_channel: None,
        }
    }

    pub fn new_side_channel(&mut self, side_channel: Wire<Box<dyn Mapper>>) {
        self.side_channel.replace(side_channel);
    }

    pub fn oam_bytes(&mut self) -> &mut [u8; 256] {
        unsafe { transmute(&mut self.oam) }
    }

    pub fn oam(&mut self) -> &mut [Object; 64] {
        &mut self.oam
    }

    pub fn reset(&mut self) {
        self.cycles = 0;
        self.scanline = 0;
        self.is_drawing = true;
        self.status_register = StatusRegister::new();
        self.mask_register = MaskRegister::new();
        self.control_register = ControlRegister::new();
        self.ppu_data_buffer = 0;
        self.vloopy = LoopyImpl::new();
        self.tloopy = LoopyImpl::new();
        self.address_latch = false;
        self.fine_x = 0;
        self.bg_next = NextBgPixels {
            id: 0,
            palette: 0,
            color_lsb: 0,
            color_msb: 0,
        };
        self.bg_cache = BgPixelCache {
            pixel_low: 0,
            pixel_high: 0,
            palette_low: 0,
            palette_high: 0,
        };
    }

    fn increment_x(&mut self) {
        if self.mask_register.render_background() || self.mask_register.render_sprites() {
            if self.vloopy.coarse_x() == 31 {
                self.vloopy.set_coarse_x(0);
                self.vloopy.set_nametable_x(!self.vloopy.nametable_x());
            } else {
                self.vloopy.set_coarse_x(self.vloopy.coarse_x() + 1);
            }
        }
    }

    fn increment_y(&mut self) {
        if self.mask_register.render_background() || self.mask_register.render_sprites() {
            if self.vloopy.fine_y() < 7 {
                self.vloopy.set_fine_y(self.vloopy.fine_y() + 1);
            } else {
                self.vloopy.set_fine_y(0);

                if self.vloopy.coarse_y() == 29 {
                    self.vloopy.set_coarse_y(0);
                    self.vloopy.set_nametable_y(!self.vloopy.nametable_y());
                } else if self.vloopy.coarse_y() == 31 {
                    self.vloopy.set_coarse_y(0);
                } else {
                    self.vloopy.set_coarse_y(self.vloopy.coarse_y() + 1);
                }
            }
        }
    }

    fn transfer_x(&mut self) {
        if self.mask_register.render_background() || self.mask_register.render_sprites() {
            self.vloopy.set_nametable_x(self.tloopy.nametable_x());
            self.vloopy.set_coarse_x(self.tloopy.coarse_x());
        }
    }

    fn transfer_y(&mut self) {
        if self.mask_register.render_background() || self.mask_register.render_sprites() {
            self.vloopy.set_fine_y(self.tloopy.fine_y());
            self.vloopy.set_nametable_y(self.tloopy.nametable_y());
            self.vloopy.set_coarse_y(self.tloopy.coarse_y());
        }
    }

    fn load_shifters(&mut self) {
        self.bg_cache.pixel_low =
            (self.bg_cache.pixel_low & 0xFF00) | self.bg_next.color_lsb as u16;
        self.bg_cache.pixel_high =
            (self.bg_cache.pixel_high & 0xFF00) | self.bg_next.color_msb as u16;

        self.bg_cache.palette_low = (self.bg_cache.palette_low & 0xFF00)
            | if (self.bg_next.palette & 0b01) != 0 {
                0xFF
            } else {
                0x00
            };
        self.bg_cache.palette_high = (self.bg_cache.palette_high & 0xFF00)
            | if (self.bg_next.palette & 0b10) != 0 {
                0xFF
            } else {
                0x00
            };
    }

    fn update_shifters(&mut self) {
        if self.mask_register.render_background() {
            self.bg_cache.pixel_low <<= 1;
            self.bg_cache.pixel_high <<= 1;
            self.bg_cache.palette_low <<= 1;
            self.bg_cache.palette_high <<= 1;
        }

        if self.mask_register.render_sprites() && self.cycles >= 1 && self.cycles < 258 {
            for i in 0..(self.object_count as usize) {
                let object = &mut self.to_draw[i];
                if object.x > 0 {
                    object.x -= 1;
                } else {
                    self.sprite_cache[i].low <<= 1;
                    self.sprite_cache[i].high <<= 1;
                }
            }
        }
    }

    pub fn tick(&mut self) -> Result<Option<PPUOutput>, String> {
        if self.scanline >= -1 && self.scanline < 240 {
            if self.scanline == 0 && self.cycles == 0 && self.odd_frame && (self.mask_register.render_background() || self.mask_register.render_sprites()) {
                self.cycles = 1;
            }

            if self.scanline == -1 && self.cycles == 1 {
                self.status_register.set_vertical_blank(false);
                self.status_register.set_sprite_zero_hit(false);
                self.status_register.set_sprite_overflow(false);

                // Clear sprite cache with transparent pixels
                self.sprite_cache.fill(SpritePixelCache {
                    low: 0,
                    high: 0,
                });
            }

            if (self.cycles >= 2 && self.cycles < 258) || (self.cycles >= 321 && self.cycles < 338) {
                self.update_shifters();

                match (self.cycles - 1) % 8 {
                    0 => {
                        self.load_shifters();
                        self.bg_next.id = self.ppu_bus.read(0x2000 | (self.vloopy.0 & 0x0FFF));
                    }
                    2 => {
                        self.bg_next.palette = self.ppu_bus.read(
                            0x23C0u16
                                | self.vloopy.nametable_y().shl_bool(11)
                                | self.vloopy.nametable_x().shl_bool(10)
                                | ((self.vloopy.coarse_y() >> 2) << 3) as u16
                                | (self.vloopy.coarse_x() >> 2) as u16,
                        );

                        if self.vloopy.coarse_y() & 0x02 != 0 {
                            self.bg_next.palette >>= 4;
                        }
                        if self.vloopy.coarse_x() & 0x02 != 0 {
                            self.bg_next.palette >>= 2;
                        }
                        self.bg_next.palette &= 0x03;
                    }
                    4 => {
                        self.bg_next.color_lsb = self.ppu_bus.read(
                            self.control_register.pattern_background().shl_bool(12)
                                + ((self.bg_next.id as u16) << 4)
                                + self.vloopy.fine_y() as u16,
                        );
                    }
                    6 => {
                        self.bg_next.color_msb = self.ppu_bus.read(
                            self.control_register.pattern_background().shl_bool(12)
                                + ((self.bg_next.id as u16) << 4)
                                + self.vloopy.fine_y() as u16
                                + 8,
                        );
                    }
                    7 => {
                        self.increment_x();
                    }
                    _ => {}
                }
            }

            if self.cycles == 256 {
                self.increment_y()
            }

            if self.cycles == 257 {
                self.load_shifters();
                self.transfer_x()
            }

            if self.cycles == 338 || self.cycles == 340 {
                self.bg_next.id = self.ppu_bus.read(0x2000 | (self.vloopy.0 & 0x0FFF));
            }

            if self.scanline == -1 && self.cycles >= 280 && self.cycles < 305 {
                self.transfer_y()
            }

            // Foreground ==========
            if self.cycles == 257 && self.scanline >= 0 {
                self.to_draw.fill(Object {
                    y: 0xFF,
                    id: 0x00,
                    attribute: 0x00,
                    x: 0x00,
                });
                self.object_count = 0;

                self.sprite_zero_possible = false;
                for (index, object) in self.oam.iter().enumerate() {
                    if self.object_count >= 9 {
                        break
                    }

                    let diff = (self.scanline) - (object.y as i16);
                    if (0..(if self.control_register.sprite_size() { 16 } else { 8 })).contains(&diff) {
                        if self.object_count < 8 {
                            if index == 0 {
                                self.sprite_zero_possible = true;
                            }
                            self.to_draw[self.object_count as usize] = *object;
                            self.object_count += 1;
                        }
                    }
                }
                self.status_register.set_sprite_overflow(self.object_count > 8);
            }

            if self.cycles == 340 {
                for i in 0..(self.object_count as usize) {
                    let object = &self.to_draw[i];
                    let id = object.id as u16;
                    let y_diff = (self.scanline - object.y as i16) as u16;
                    let low_address: u16;
                    let vertical_flip = msb(object.attribute);
                    let is_big = self.control_register.sprite_size();

                    if !is_big {
                        let pattern_offset = self.control_register.pattern_slice().shl_bool(12);
                        if !vertical_flip {
                            low_address = pattern_offset
                                | id.shl(4)
                                | y_diff;
                        } else {
                            low_address = pattern_offset
                                | id.shl(4)
                                | (7 - y_diff);
                        }
                    } else {
                        let is_top = y_diff < 8;
                        let pattern_offset = (id & 0x01) << 12;

                        if !vertical_flip {
                            if is_top {
                                low_address = pattern_offset
                                    | (id & 0xFE).shl(4)
                                    | (y_diff & 0x07);
                            } else {
                                low_address = pattern_offset
                                    | ((id & 0xFE) + 1).shl(4)
                                    | (y_diff & 0x07);
                            }
                        } else {
                            if is_top {
                                low_address = pattern_offset
                                    | ((id & 0xFE) + 1).shl(4)
                                    | (7 - (y_diff & 0x07));
                            } else {
                                low_address = pattern_offset
                                    | (id & 0xFE).shl(4)
                                    | (7 - (y_diff & 0x07));
                            }
                        }
                    }

                    let high_address = low_address + 8;

                    let mut bits_low = self.ppu_bus.read(low_address);
                    let mut bits_high = self.ppu_bus.read(high_address);

                    if (object.attribute & 0x40) != 0 { // flip horizontally
                        bits_high = bits_high.reverse_bits();
                        bits_low = bits_low.reverse_bits();
                    }

                    self.sprite_cache[i] = SpritePixelCache {
                        low: bits_low,
                        high: bits_high,
                    };
                }
            }
        }

        if self.scanline == 240 {
            // nothing yet
        }

        if self.scanline == 241 && self.cycles == 1 {
            self.status_register.set_vertical_blank(true);
            if self.control_register.enable_nmi() {
                self.cpu.borrow_mut().nmi();
            }
        }

        let mut bg_pixel = 0;
        let mut bg_palette = 0;
        if self.mask_register.render_background() {
            let mux = 0x8000u16 >> (self.fine_x as u16);

            let low = (self.bg_cache.pixel_low & mux) != 0;
            let high = (self.bg_cache.pixel_high & mux) != 0;

            bg_pixel = (if high { 0b10 } else { 0b00 }) | (low as u8);

            let low = (self.bg_cache.palette_low & mux) != 0;
            let high = (self.bg_cache.palette_high & mux) != 0;
            bg_palette = (if high { 0b10 } else { 0b00 }) | (low as u8);
        }

        let mut sprite_pixel: u8 = 0;
        let mut sprite_palette = 0;
        let mut sprite_priority = false;

        if self.mask_register.render_sprites() {
            self.is_drawing_sprite_zero = false;

            for i in 0..(self.object_count as usize) {
                let object = &self.to_draw[i];

                if object.x == 0 {
                    let low = msb(self.sprite_cache[i].low);
                    let high = msb(self.sprite_cache[i].high);

                    sprite_pixel = (if high { 0b10 } else { 0b00 } | low as u8);

                    sprite_palette =   (object.attribute & 0b00000111) + 0x04;
                    sprite_priority = (object.attribute & 0b00100000) == 0;

                    if sprite_pixel != 0 { // 0th pixel is always transparent.
                        if i == 0 {
                            self.is_drawing_sprite_zero = true;
                        }
                        // Breaks out early upon finding non-transparent sprite.
                        // Objects in front is closer to 0th index in `self.to_draw`
                        // In other words, when we find first non-transparent pixel, that is also "most-front" pixel.
                        break;
                    }
                }
            }
        }

        let final_pixel;
        let final_palette;

        if sprite_pixel == 0 && bg_pixel == 0 {
            final_pixel = 0;
            final_palette = 0;
        } else if sprite_pixel != 0 && bg_pixel == 0 {
            final_pixel = sprite_pixel;
            final_palette = sprite_palette;
        } else if sprite_pixel == 0 && bg_pixel != 0 {
            final_pixel = bg_pixel;
            final_palette = bg_palette;
        } else {
            if sprite_priority {
                final_pixel = sprite_pixel;
                final_palette = sprite_palette;
            } else {
                final_pixel = bg_pixel;
                final_palette = bg_palette;
            }

            if self.sprite_zero_possible && self.is_drawing_sprite_zero {
                if self.mask_register.render_sprites() && self.mask_register.render_background() {
                    let include_left = self.mask_register.render_sprites_left() || self.mask_register.render_background_left();
                    let left_boundary = if include_left { 1 } else { 9 };

                    if (left_boundary..258).contains(&self.cycles) {
                        self.status_register.set_sprite_zero_hit(true);
                    }
                }
            }
        }

        let color = self.generate_color(final_palette, final_pixel)?;

        if LOG_COLOR {
            let contents = format!("[{}]: {} {} {} {} {} {} {} {} {} {}\n", self.age,
               self.bg_cache.palette_low, self.bg_cache.palette_high, self.bg_cache.pixel_low, self.bg_cache.pixel_high,
                self.bg_next.id, self.bg_next.palette, self.bg_next.color_lsb, self.bg_next.color_msb,
                final_palette, final_pixel
            );
            self.colordump.write_all(contents.as_bytes()).unwrap();
        }

        let x = self.cycles - 1;
        let y = self.scanline;
        let output = if (0..256).contains(&x) && (0..240).contains(&y) {
            Some(PPUOutput {
                screen_pos: (x as u8, y as u8),
                color,
            })
        } else {
            None
        };

        if LOG_PPU {
            let contents = format!(
                "{:10} V: {:04X} T: {:04X} MASK: {:02X} CTR: {:02X} STS: {:02X} \n",
                self.age, self.vloopy.0, self.tloopy.0, self.mask_register.0, self.control_register.0, self.status_register.0
            );
            self.log_file.write_all(contents.as_bytes()).unwrap();
        }

        self.cycles += 1;

        if self.mask_register.render_background() || self.mask_register.render_sprites() {
            if self.cycles == 260 && self.scanline < 240 {
                if let Some(channel) = self.side_channel.as_ref() {
                    channel.borrow_mut().on_scanline();
                }
            }
        }
        if self.cycles >= 341 {
            self.cycles = 0;
            self.scanline += 1;
            if self.scanline >= 261 {
                self.scanline = -1;
                self.is_drawing = false;
                self.odd_frame.flip();
            }
        } else {
            self.is_drawing = true;
        }

        self.age += 1;

        Ok(output)
    }

    pub fn colors(ppu_type: PPUType) -> Result<&'static [(u8, u8, u8); 0x40], String> {
        match ppu_type {
            PPUType::RP2C02 | PPUType::RP2C07 => Ok(&PALETTE_2C02),
            _ => Err("Unsupported PPU Type".to_string()),
        }
    }

    fn generate_color(
        &self,
        palette_id: u8,
        pixel_id: u8,
    ) -> Result<(u8, u8, u8), String> {
        let colors = Self::colors(self.ppu_type)?;
        let index = self.ppu_bus.read(0x3F00u16 + ((palette_id << 2) + pixel_id) as u16) & 0x3F;
        Ok(colors[index as usize])
    }

    pub fn take_snapshot(&self, new_bus: Rc<PPUBus>, new_cpu: Wire<CPU6502>) -> Self {
        Self {
            ppu_type: self.ppu_type,
            ppu_bus: new_bus,
            cycles: self.cycles,
            scanline: self.scanline,
            odd_frame: self.odd_frame,
            is_drawing: self.is_drawing,
            status_register: self.status_register,
            mask_register: self.mask_register,
            control_register: self.control_register,
            ppu_data_buffer: self.ppu_data_buffer,
            vloopy: self.vloopy,
            tloopy: self.tloopy,
            address_latch: self.address_latch,
            fine_x: self.fine_x,
            age: self.age,
            log_file: self.log_file.try_clone().unwrap(),
            colordump: self.colordump.try_clone().unwrap(),
            bg_next: self.bg_next,
            bg_cache: self.bg_cache,
            cpu: new_cpu,
            oam: self.oam.clone(),
            oam_address: self.oam_address,
            to_draw: self.to_draw,
            object_count: self.object_count,
            sprite_cache: self.sprite_cache,
            sprite_zero_possible: self.sprite_zero_possible,
            is_drawing_sprite_zero: self.is_drawing_sprite_zero,
            side_channel: None,
        }
    }
}

impl ToString for PPU {
    fn to_string(&self) -> String {
        self.ppu_type.to_string()
    }
}

impl BusDevice<SystemBus> for PPU {
    const MIN_ADDRESS: u16 = 0x2000;
    const MAX_ADDRESS: u16 = 0x2007;

    fn on_read(&mut self, _: &SystemBus, address: u16) -> Option<u8> {
        match address {
            0x2000 => {} // CONTROL
            0x2001 => {} // MASK
            0x2002 => {
                // STATUS
                let to_return = (self.status_register.0 & 0xE0) | (self.ppu_data_buffer & 0x1F);
                self.status_register.set_vertical_blank(false);
                self.address_latch = false;
                return Some(to_return);
            }
            0x2003 => {} // OAM_ADDRESS
            0x2004 => unsafe { // OAM_DATA
                let as_bytes: &[u8; 256] = transmute(&self.oam);
                return Some(as_bytes[self.oam_address as usize])
            }
            0x2005 => {} // SCROLL
            0x2006 => {} // PPU_ADDRESS
            0x2007 => {
                let mut to_return = Some(self.ppu_data_buffer);

                self.ppu_data_buffer = self.ppu_bus.read(self.vloopy.0);
                if self.vloopy.0 >= 0x3F00 {
                    to_return = Some(self.ppu_data_buffer);
                }
                self.vloopy.0 += if self.control_register.increment_mode() {
                    32
                } else {
                    1
                };
                return to_return;
            } // PPU_DATA
            _ => unreachable!(),
        };
        Some(0) // This is probably not impedance
    }

    fn on_write(&mut self, _: &SystemBus, address: u16, data: u8) {
        match address {
            0x2000 => {
                // CONTROL
                self.control_register.0 = data;
                self.tloopy
                    .set_nametable_x(self.control_register.nametable_x());
                self.tloopy
                    .set_nametable_y(self.control_register.nametable_y());
            }
            0x2001 => {
                // MASK
                self.mask_register.0 = data;
            }
            0x2002 => {} // STATUS
            0x2003 => { // OAM_ADDRESS
                self.oam_address = data;
            }
            0x2004 => unsafe { // OAM_DATA
                let as_bytes: &mut [u8; 256] = transmute(&mut self.oam);
                as_bytes[self.oam_address as usize] = data;
            }
            0x2005 => {
                // SCROLL
                if !self.address_latch {
                    self.fine_x = data & 0b00000111;
                    self.tloopy.set_coarse_x(data >> 3);
                } else {
                    self.tloopy.set_fine_y(data & 0b00000111);
                    self.tloopy.set_coarse_y(data >> 3);
                }
                self.address_latch.flip();
            }
            0x2006 => {
                // PPU_ADDRESS
                if !self.address_latch {
                    self.tloopy.0 = (self.tloopy.0 & 0x00FF) | (((data & 0x3F) as u16) << 8);
                } else {
                    self.tloopy.0 = (self.tloopy.0 & 0xFF00) | data as u16;
                    self.vloopy.0 = self.tloopy.0;
                }
                self.address_latch.flip();
            }
            0x2007 => {
                // PPU_DATA
                self.ppu_bus.write(self.vloopy.0, data);
                self.vloopy.0 = self.vloopy.0.wrapping_add_signed(if self.control_register.increment_mode() {
                    32
                } else {
                    1
                });
            }
            _ => unreachable!(),
        };
    }
}
