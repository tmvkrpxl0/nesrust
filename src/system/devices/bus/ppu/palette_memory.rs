use crate::system::devices::bus::memory::Memory;
use crate::system::devices::bus::PPUBus;
use crate::system::devices::BusDevice;

pub struct PaletteMemory {
    internal: Memory<{ PaletteMemory::MIN_ADDRESS }, { PaletteMemory::MAX_ADDRESS }>,
    has_changed: bool,
}

impl Clone for PaletteMemory {
    fn clone(&self) -> Self {
        Self {
            internal: self.internal.clone(),
            has_changed: true,
        }
    }
}

impl PaletteMemory {
    pub fn new() -> Self {
        Self {
            internal: Memory::new("Palette Memory", 1),
            has_changed: true,
        }
    }

    pub fn or(&mut self, value: bool) {
        self.has_changed |= value;
    }

    pub fn needs_update(&mut self) -> bool {
        let to_return = self.has_changed;
        self.has_changed = false;
        to_return
    }
}

impl ToString for PaletteMemory {
    fn to_string(&self) -> String {
        "Palette Memory".to_string()
    }
}

impl BusDevice<PPUBus> for PaletteMemory {
    const MIN_ADDRESS: u16 = 0x3F00;
    const MAX_ADDRESS: u16 = 0x3FFF;

    fn on_read(&mut self, bus: &PPUBus, mut address: u16) -> Option<u8> {
        address &= 0x3F1F;

        address = match address {
            0x3F10 => 0x3F00,
            0x3F14 => 0x3F04,
            0x3F18 => 0x3F08,
            0x3F1C => 0x3F0C,
            _ => address,
        };

        self.internal.on_read(bus, address)
    }

    fn on_write(&mut self, bus: &PPUBus, mut address: u16, data: u8) {
        address &= 0x3F1F;

        address = match address {
            0x3F10 => 0x3F00,
            0x3F14 => 0x3F04,
            0x3F18 => 0x3F08,
            0x3F1C => 0x3F0C,
            _ => address,
        };
        self.has_changed = true;
        self.internal.on_write(bus, address, data)
    }
}
