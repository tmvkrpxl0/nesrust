use std::cell::Cell;

use crate::system::devices::bus::cartridge_connector::CartridgeConnector;
use crate::system::devices::bus::expansions::ExpansionDevice;
use crate::system::devices::bus::memory::Memory;
use crate::system::devices::bus::ppu::palette_memory::PaletteMemory;
use crate::system::devices::bus::ppu::pattern_memory::PatternMemory;
use crate::system::devices::bus::ppu::PPU;
use crate::system::devices::BusDevice;
use crate::system::devices::cpu::apu::APU;
use crate::system::Wire;
use crate::utils::{append, Wrapping};

pub mod cartridge_connector;
pub mod expansions;
pub mod memory;
pub mod ppu;

pub trait Bus {
    fn read(&self, address: u16) -> u8;

    fn write(&self, address: u16, data: u8);
}

pub struct SystemBus {
    system_ram: Wire<Memory<0x0000, 0x1FFF>>,
    cartridge_port: Wire<CartridgeConnector>,
    ppu: Wire<PPU>,
    apu: Wire<APU>,
    peripheral: Wire<(Box<dyn ExpansionDevice>, Box<dyn ExpansionDevice>)>,

    dma: Cell<DmaInfo>,
}

impl SystemBus {
    pub fn new(
        system_ram: Wire<Memory<0x0000, 0x1FFF>>,
        cartridge_port: Wire<CartridgeConnector>,
        ppu: Wire<PPU>,
        apu: Wire<APU>,
        peripheral: Wire<(Box<dyn ExpansionDevice>, Box<dyn ExpansionDevice>)>,
    ) -> SystemBus {
        Self {
            system_ram,
            cartridge_port,
            ppu,
            apu,
            peripheral,

            dma: Cell::new(DmaInfo {
                dma_page: 0,
                dma_address: Wrapping(0),
                dma_data: 0,
                during_dma: false,
                is_dma_synced: false,
            }),
        }
    }

    pub fn dma(&self, oam: &mut [u8; 256], time: u32) -> bool {
        let DmaInfo {
            dma_page, mut dma_address, mut dma_data, mut during_dma, mut is_dma_synced
        } = self.dma.get();

        let mut dma_processed = false;

        if during_dma {
            dma_processed = true;
            if !is_dma_synced {
                if time % 2 == 1 {
                    is_dma_synced = true;
                }
            } else {
                if time % 2 == 0 {
                    dma_data = self.read(append(dma_page, dma_address.0));
                } else {
                    oam[dma_address.0 as usize] = dma_data;
                    dma_address += 1;

                    if dma_address.0 == 0 {
                        during_dma = false;
                        is_dma_synced = false;
                    }
                }
            }
        }

        self.dma.set(DmaInfo {
            dma_page,
            dma_address,
            dma_data,
            during_dma,
            is_dma_synced,
        });
        dma_processed
    }

    pub fn take_snapshot(
        &self,
        system_ram: Wire<Memory<0x0000, 0x1FFF>>,
        cartridge_port: Wire<CartridgeConnector>,
        ppu: Wire<PPU>,
        apu: Wire<APU>,
        peripheral: Wire<(Box<dyn ExpansionDevice>, Box<dyn ExpansionDevice>)>
    ) -> SystemBus {
        Self {
            system_ram,
            cartridge_port,
            ppu,
            apu,
            peripheral,
            dma: self.dma.clone(),
        }
    }

    pub fn reset(&self) {
        self.cartridge_port.borrow().take_irq();
        self.ppu.borrow_mut().reset();
        self.cartridge_port.borrow_mut().reset();
    }

    pub fn has_irq(&self) -> bool {
        let v1 = self.cartridge_port.borrow().take_irq();
        let v2 = self.apu.borrow().has_irq();

        v1 | v2
    }

    pub fn load_trainer(&self, trainer: &Vec<u8>) {
        assert_eq!(512, trainer.len());
        let mut cartridge = self.cartridge_port.borrow_mut();
        trainer.iter().cloned().enumerate().for_each(|(idx, data)| {
            cartridge.on_write(self, 0x7000 + idx as u16, data);
        })
    }
}

pub struct PPUBus {
    pattern_memory: Wire<PatternMemory>,
    palette: Wire<PaletteMemory>,
    cartridge_port: Wire<CartridgeConnector>,
}

impl PPUBus {
    pub fn new(
        pattern_memory: Wire<PatternMemory>,
        palette: Wire<PaletteMemory>,
        cartridge_port: Wire<CartridgeConnector>,
    ) -> PPUBus {
        Self {
            pattern_memory,
            palette,
            cartridge_port,
        }
    }
}

#[derive(Clone, Copy)]
struct DmaInfo {
    dma_page: u8,
    dma_address: Wrapping<u8>,
    dma_data: u8,

    during_dma: bool,
    is_dma_synced: bool,
}

macro_rules! try_read {
    ($bus:expr, $device_type:ty, $device:expr, $address:expr) => {{
        if (<$device_type>::MIN_ADDRESS..=<$device_type>::MAX_ADDRESS).contains(&$address) {
            let binding = $device;
            let mut temp = binding.borrow_mut();
            return temp.on_read($bus, $address).unwrap_or(0);
        }
    }};
    ($bus:expr, $min:expr, $max:expr, $device:expr, $address:expr) => {{
        if ($min..=$max).contains(&$address) {
            let binding = $device;
            let mut temp = binding.borrow_mut();
            return temp.on_read($bus, $address).unwrap_or(0);
        }
    }};
}

macro_rules! try_write {
    ($bus:expr, $device_type:ty, $device:expr, $address:expr, $data:expr) => {{
        if (<$device_type>::MIN_ADDRESS..=<$device_type>::MAX_ADDRESS).contains(&$address) {
            let binding = $device;
            let mut temp = binding.borrow_mut();
            temp.on_write($bus, $address, $data);
            return;
        }
    }};
    ($bus:expr, $min:expr, $max:expr, $device:expr, $address:expr, $data:expr) => {{
        if ($min..=$max).contains(&$address) {
            let binding = $device;
            let mut temp = binding.borrow_mut();
            temp.on_write($bus, $address, $data);
            return;
        }
    }};
}

impl Bus for SystemBus {
    fn read(&self, address: u16) -> u8 {
        try_read!(self, 0x0000, 0x1FFF, self.system_ram.clone(), address);
        try_read!(self, PPU, self.ppu.clone(), address);
        if address == 0x4016 {
            return self.peripheral.borrow_mut().0.on_read(self, address).unwrap_or(0);
        }
        if address == 0x4017 {
            return self.peripheral.borrow_mut().1.on_read(self, address).unwrap_or(0)
        }
        try_read!(self, 0x4020, 0xFFFF, self.cartridge_port.clone(), address);

        0
    }

    fn write(&self, address: u16, data: u8) {
        try_write!(self, 0x0000, 0x07FF, self.system_ram.clone(), address, data);
        try_write!(self, PPU, self.ppu.clone(), address, data);

        if address == 0x4014 { // DMA
            let mut dma = self.dma.get();
            dma.dma_page = data;
            dma.dma_address.0 = 0;
            dma.during_dma = true;
            self.dma.set(dma);
            return;
        }

        if address == 0x4016 {
            let expansion = self.peripheral.clone();
            let mut temp = expansion.borrow_mut();
            temp.0.on_write(&self, address, data);
            return;
        }

        try_write!(self, APU, self.apu.clone(), address, data);
        try_write!(self, 0x4020, 0xFFFF, self.cartridge_port.clone(), address, data);
    }
}

impl Bus for PPUBus {
    fn read(&self, address: u16) -> u8 {
        if address > 0x4000 {
            return 0;
        }

        try_read!(self, PatternMemory, self.pattern_memory.clone(), address);
        try_read!(self, 0x2000, 0x3EFF, self.cartridge_port.clone(), address);
        try_read!(self, PaletteMemory, self.palette.clone(), address);

        0
    }

    fn write(&self, address: u16, data: u8) {
        if address > 0x4000 {
            return;
        }
        try_write!(self, PaletteMemory, self.palette.clone(), address, data);
        try_write!(self, 0, 0x3EFF, self.cartridge_port.clone(), address, data);
        try_write!(self, PatternMemory, self.pattern_memory.clone(), address, data);
    }
}
