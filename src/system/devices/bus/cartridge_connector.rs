use crate::system::devices::bus::{PPUBus, SystemBus};
use crate::system::devices::bus::ppu::PPU;
use crate::system::devices::game_cartridge::Cartridge;
use crate::system::devices::BusDevice;

pub struct CartridgeConnector {
    pub cartridge: Option<Cartridge>,
}

impl ToString for CartridgeConnector {
    fn to_string(&self) -> String {
        String::from("Cartridge Connector")
    }
}

impl CartridgeConnector {
    fn on_read(&mut self, address: u16) -> Option<u8> {
        if let Some(cartrige) = &mut self.cartridge {
            cartrige.on_read(address)
        } else {
            None
        }
    }

    fn on_write(&mut self, address: u16, data: u8) {
        match &mut self.cartridge {
            None => {}
            Some(cartridge) => {
                cartridge.on_write(address, data);
            }
        }
    }

    pub fn reset(&mut self) {
        if let Some(cartridge) = &mut self.cartridge {
            cartridge.reset();
        }
    }

    pub fn take_snapshot(&self) -> Self {
        let cloned = self.cartridge.as_ref().map(|cartridge| cartridge.take_snapshot());
        Self {
            cartridge: cloned,
        }
    }

    pub fn take_irq(&self) -> bool {
        self.cartridge.as_ref().map(|rom| rom.take_irq()).unwrap_or(false)
    }

    pub fn connect_side_channel(&self, ppu: &mut PPU) {
        if let Some(rom) = self.cartridge.as_ref() {
            ppu.new_side_channel(rom.mapper.wire())
        }
    }
}

impl BusDevice<PPUBus> for CartridgeConnector {
    const MIN_ADDRESS: u16 = 0x2000;
    const MAX_ADDRESS: u16 = 0x3EFF;

    fn on_read(&mut self, _: &PPUBus, address: u16) -> Option<u8> {
        self.on_read(address)
    }

    fn on_write(&mut self, _: &PPUBus, address: u16, data: u8) {
        self.on_write(address, data)
    }
}

impl BusDevice<SystemBus> for CartridgeConnector {
    const MIN_ADDRESS: u16 = 0x4020;
    const MAX_ADDRESS: u16 = 0xFFFF;

    fn on_read(&mut self, _: &SystemBus, address: u16) -> Option<u8> {
        self.on_read(address)
    }

    fn on_write(&mut self, _: &SystemBus, address: u16, data: u8) {
        self.on_write(address, data)
    }
}
