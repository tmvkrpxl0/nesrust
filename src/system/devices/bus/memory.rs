use crate::system::devices::bus::Bus;
use crate::system::devices::BusDevice;

#[derive(Clone)]
pub struct Memory<const MIN: u16, const MAX: u16> {
    memory: Vec<u8>,
    name: String,
}

impl<const MIN: u16, const MAX: u16> Memory<MIN, MAX> {
    pub fn new(name: &str, repeat: usize) -> Memory<MIN, MAX> {
        let size = (MAX - MIN + 1) as usize;

        if size % repeat != 0 {
            panic!("Memory capacity must be divisible by repeat count!")
        }
        let memory = vec![0; size / repeat];
        Memory {
            memory,
            name: name.to_string(),
        }
    }
}

impl<const MIN: u16, const MAX: u16> ToString for Memory<MIN, MAX> {
    fn to_string(&self) -> String {
        self.name.clone()
    }
}

impl<T: Bus, const MIN: u16, const MAX: u16> BusDevice<T> for Memory<MIN, MAX> {
    const MIN_ADDRESS: u16 = MIN;
    const MAX_ADDRESS: u16 = MAX;

    fn on_read(&mut self, _: &T, address: u16) -> Option<u8> {
        let index = (address - MIN) as usize % self.memory.len();
        let value = self.memory[index];
        Some(value)
    }

    fn on_write(&mut self, _: &T, address: u16, data: u8) {
        let index = (address - MIN) as usize % self.memory.len();
        self.memory[index] = data;
    }
}
