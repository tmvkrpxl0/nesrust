use crate::system::{NES, Wire};
use crate::system::devices::bus::ppu::palette_memory::PaletteMemory;
use crate::system::devices::bus::ppu::pattern_memory::PatternMemory;
use crate::system::devices::bus::ppu::PPU;
use crate::system::devices::bus::PPUBus;
use crate::system::devices::cpu::CPU6502;

pub trait DevConsole {
    fn ppu(&self) -> Wire<PPU>;
    fn ppu_bus(&self) -> &PPUBus;
    fn palette(&self) -> Wire<PaletteMemory>;
    fn pattern(&self) -> Wire<PatternMemory>;
    fn cpu(&self) -> Wire<CPU6502>;
    fn pause(&mut self);
    fn resume(&mut self);
}

impl DevConsole for NES {
    fn ppu(&self) -> Wire<PPU> {
        self.ppu.wire()
    }

    fn ppu_bus(&self) -> &PPUBus {
        &self.ppu_bus
    }

    fn palette(&self) -> Wire<PaletteMemory> {
        self.palette.wire()
    }

    fn pattern(&self) -> Wire<PatternMemory> {
        self.pattern.wire()
    }

    fn cpu(&self) -> Wire<CPU6502> {
        self.cpu.wire()
    }

    fn pause(&mut self) {
        self.has_paused = true;
    }

    fn resume(&mut self) {
        self.has_paused = false;
    }
}
