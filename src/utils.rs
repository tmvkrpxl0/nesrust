use std::cmp::Ordering;
use std::f64::consts::PI;
use std::fmt::{Formatter, UpperHex};
use std::num::NonZeroU32;
use std::ops::{
    Add, AddAssign, BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign, Shl, Sub,
    SubAssign,
};

use num_traits::{signum, WrappingAdd, WrappingSub};

static PULSE_HARMONICS: u8 = 20;

pub fn msb(value: u8) -> bool {
    value.bitand(0b10000000) != 0
}

pub fn append(high: u8, low: u8) -> u16 {
    let shifted: u16 = (high as u16).shl(8);
    shifted.bitxor(low as u16)
}

pub trait MultiByte {
    fn lower(&self) -> u8;
    fn higher(&self) -> u8;
}

impl MultiByte for u16 {
    fn lower(&self) -> u8 {
        (self & 0x00FF) as u8
    }

    fn higher(&self) -> u8 {
        ((self & 0xFF00) >> 8) as u8
    }
}

pub trait Flip {
    fn flip(&mut self) -> Self;
}

impl Flip for bool {
    fn flip(&mut self) -> Self {
        self.bitxor_assign(true);
        *self
    }
}

pub trait BitShift<T: Copy + Ord> {
    fn shl_bool(self, left: u8) -> T;
    fn shr_bool(self, right: u8) -> T;
}

impl BitShift<u16> for bool {
    fn shl_bool(self, left: u8) -> u16 {
        let value = if self { 1 } else { 0 };
        value << left
    }

    fn shr_bool(self, right: u8) -> u16 {
        let value = if self { 1 } else { 0 };
        value >> right
    }
}

#[derive(Copy, Clone)]
pub struct Wrapping<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd>(pub T);

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd> PartialEq<T>
    for Wrapping<T>
{
    fn eq(&self, other: &T) -> bool {
        self.0.eq(other)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd> PartialOrd<T>
    for Wrapping<T>
{
    fn partial_cmp(&self, other: &T) -> Option<Ordering> {
        self.0.partial_cmp(other)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd> Eq for Wrapping<T> {}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd> PartialEq<Self>
    for Wrapping<T>
{
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd> PartialOrd<Self>
    for Wrapping<T>
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd> Ord for Wrapping<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + UpperHex> UpperHex
    for Wrapping<T>
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + BitAnd<Output = T>>
    BitAnd<T> for Wrapping<T>
{
    type Output = T;

    fn bitand(self, rhs: T) -> Self::Output {
        self.0.bitand(rhs)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + BitXor<Output = T>>
    BitXor<T> for Wrapping<T>
{
    type Output = T;

    fn bitxor(self, rhs: T) -> Self::Output {
        self.0.bitxor(rhs)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + BitAndAssign>
    BitAndAssign<T> for Wrapping<T>
{
    fn bitand_assign(&mut self, rhs: T) {
        self.0.bitand_assign(rhs)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + Add<Output = T>> Add<T>
    for Wrapping<T>
{
    type Output = T;

    fn add(self, rhs: T) -> Self::Output {
        self.0.wrapping_add(&rhs)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + AddAssign> AddAssign<T>
    for Wrapping<T>
{
    fn add_assign(&mut self, rhs: T) {
        let value = self.0.wrapping_add(&rhs);
        self.0 = value;
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + Sub<Output = T>> Sub<T>
    for Wrapping<T>
{
    type Output = T;

    fn sub(self, rhs: T) -> Self::Output {
        self.0.wrapping_sub(&rhs)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + SubAssign> SubAssign<T>
    for Wrapping<T>
{
    fn sub_assign(&mut self, rhs: T) {
        let value = self.0.wrapping_sub(&rhs);
        self.0 = value;
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + BitXorAssign>
    BitXorAssign<T> for Wrapping<T>
{
    fn bitxor_assign(&mut self, rhs: T) {
        self.0.bitxor_assign(rhs)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + BitOr<Output = T>> BitOr<T>
    for Wrapping<T>
{
    type Output = T;

    fn bitor(self, rhs: T) -> Self::Output {
        self.0.bitor(rhs)
    }
}

impl<T: PartialEq + Eq + PartialOrd + Ord + WrappingSub + WrappingAdd + BitOrAssign> BitOrAssign<T>
    for Wrapping<T>
{
    fn bitor_assign(&mut self, rhs: T) {
        self.0.bitor_assign(rhs)
    }
}

impl MultiByte for Wrapping<u16> {
    fn lower(&self) -> u8 {
        self.0.lower()
    }

    fn higher(&self) -> u8 {
        self.0.higher()
    }
}

pub trait CheckedAssign {
    fn checked_add_assign(&mut self, value: u32) -> Option<NonZeroU32>;
    fn checked_sub_assign(&mut self, value: u32) -> Option<NonZeroU32>;
}

impl CheckedAssign for NonZeroU32 {
    fn checked_add_assign(&mut self, value: u32) -> Option<NonZeroU32> {
        let result = self.checked_add(value);
        if let Some(added) = &result {
            *self = *added
        }
        result
    }

    fn checked_sub_assign(&mut self, value: u32) -> Option<NonZeroU32> {
        let new_value = self.get() - value;
        if new_value == 0 {
            None
        } else {
            unsafe { *self = NonZeroU32::new_unchecked(new_value) }
            Some(self.clone())
        }
    }
}

pub fn sin_fast(x: f64) -> f64 {
    let mut j = x * 0.15915;
    j = j - j.floor();
    return 20.785 * j * (j - 0.5) * (j - 1.0);
}

pub fn square_wave(x: f64, frequency: f64, duty_cycle: f64, amplitude: f64) -> f64 {
    ((signum((x * 2.0 * PI * frequency - duty_cycle * 2.0).sin() + 2.0 * duty_cycle - 1.0) + 1.0) / 2.0) * amplitude
}

pub fn triangle_wave(x: f64, frequency: f64) -> f64 {
    let volume = x % (1.0 / (2.0 * frequency)) * frequency * 2.0;
    let should_flip = signum(x % (1.0 / frequency) * frequency - 0.5) > 0.0;

    let to_return = if should_flip {
        -volume + 1.0
    } else {
        volume
    };

    to_return
}

pub fn pcg_hash(input: u32) -> u32 {
    let state = input.wrapping_mul(747796405).wrapping_add(2891336453);
    let word = (state.wrapping_shr(state.wrapping_shr(28).wrapping_add(4)) ^ state).wrapping_mul(277803737);
    (word.wrapping_shr(22)) ^ word
}
