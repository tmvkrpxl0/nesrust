use crate::platform::run;

mod platform;
mod system;
mod utils;
mod val_text;

#[cfg_attr(target_arch = "wasm32", wasm_bindgen(start))]
pub async fn launch_web() {
    run(None).await;
}
